/**
 *ZipLongVal.java
 *
 * COPYRIGHT NOTICE:
 * Copyright (c) 2007 Infosys Technologies Limited.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Infosys Technologies Ltd. ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered
 * into with Infosys.
 */
package com.infosys.custom.ebanking.common.validators;

import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.types.valueobjects.CustomStandardTextDetailsVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.types.IFEBAType;
import com.infosys.feba.framework.types.primitives.FEBAAString;
import com.infosys.feba.framework.valengine.IFEBAStandardValidator;
import com.infosys.fentbase.common.FBAErrorCodes;

/**
 * This validator class is used to validate whether the entered value is a number or not
 * 
 * @author Antony_Arockiasamy
 */
public class CustomNumberValidator implements IFEBAStandardValidator {
    private static CustomNumberValidator instance;
    /**
     * This method validate whether the entered value is a number or not
     * 
     * @see com.infosys.feba.framework.valengine.IFEBAStandardValidator#validate(com.infosys.feba.framework.commontran.context.FEBATransactionContext,
     *      com.infosys.feba.framework.types.IFEBAType)
     */
       /**
     * @param tc
     * @param valCheckFlag
     * @param strToken
     * @return
     * @throws BusinessException
     */
    /**
     * Returns the Singleton instance of this validator.
     * 
     * @return
     */
    public static CustomNumberValidator getInstance() {

        if (instance == null) {
            instance = new CustomNumberValidator();
        }
        return instance;
    }
	@Override
	public void validate(FEBATransactionContext opcontext, IFEBAType displayOrder)
			throws BusinessException, CriticalException, FatalException {
		CustomStandardTextDetailsVO  objToBeValidated=(CustomStandardTextDetailsVO) (displayOrder);
		 try
         {
             Long.parseLong(objToBeValidated.getDisplayOrder().getValue());
             
         }catch(NumberFormatException nfe){
			throw new BusinessException(
					true,
					opcontext,
					CustomEBankingIncidenceCodes.INVALID_DISPLAY_ORDER,
					"Display Order should be a numeric value.",
					null,
					CustomEBankingErrorCodes.INVALID_DISPLAY_ORDER,
					null);
		}
		
	}
}
