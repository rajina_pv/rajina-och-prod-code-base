package com.infosys.custom.ebanking.common;

public class CustomEBConstants {
	private CustomEBConstants() {
		super();
	}

	
	public static final String BRANCH_CODE_SUFFIX = "BRANCH_CODE_SUFFIX";
	public static final String REGISTRATION_SMS = "SMS_REG";
	public static final String LOGIN_SMS = "SMS_LOGIN";
	public static final String APIGEE_TOKEN = "APIGEE_TOKEN";
	public static final String APIGEE_SECRET_KEY = "APIGEE_SECRET_KEY";
	public static final String BRI_SIGNATURE_TOKEN_TAG = "X-BRI-Signature";
	public static final String BRI_SIGNATURE_TIMESTAMP_TAG = "X-BRI-Timestamp";
	public static final String HMAC_ALGO = "HmacSHA256";
	public static final String ASTG = "ASTG";
	public static final String PINANG = "PINANG";
	public static final String SAHABAT = "SAHABAT";
	public static final String INSTALLATION_PRODUCT_ID = "INSTALLATION_PRODUCT_ID";
	public static final String PLACEHOLDER_IDENTIFIER = "$$";
	public static final String AMOUNT1 = "AMOUNT1";
	public static final String AMOUNT2 = "AMOUNT2";
	public static final String AMOUNT3 = "AMOUNT3";
	public static final String DATE1 = "DATE1";
	public static final String DATE2 = "DATE2";
	public static final String DATE3 = "DATE3";
	public static final String OTHER1 = "OTHER1";
	public static final String OTHER2 = "OTHER2";
	public static final String OTHER3 = "OTHER3";
	public static final String APP_NOT_COM = "APP_NOT_COM";
	public static final String LON_ACT_REM = "LON_ACT_REM";
	public static final String NEXT_NOTIFICATION_ID_SEQ = "NXT_NOTIFICATION_ID_SEQ";
	public static final String NEXT_EVENT_ID_SEQ = "NXT_EVENT_ID_SEQ";
	public static final String CR_SCORE_APR = "CR_SCORE_APR";
	public static final String CR_SCORE_SUB = "CR_SCORE_SUB";
	public static final String LOAN_APPLN_DOC_FETCH_PROPERTY = "LOAN_APPLN_DOC";
	public static final String LOAN_APPLN_DOC_CODE_TYPE = "DOC";
	public static final String LOAN_CLO = "LON_CLO";
	public static final String LOAN_CLOSED = "LOAN_CLOSED";
	public static final String LOAN_CREATED = "LOAN_CREATED";
	public static final String CD_TYPE_MARITAL_STATUS = "MST";
	public static final String CD_TYPE_HM_OWN_STATUS = "RES";
	public static final String CD_TYPE_HM_OWN_STATUS_HOST_MAPPING = "RESH";
	public static final String CD_TYPE_EDU_CODE_HOST_MAPPING = "EDUH";
	public static final String CD_TYPE_MARITAL_STATUS_HOST_MAPPING = "MSTH";
	public static final String CODE_TYPE_HELP_TOPIC = "MTP";
	
	public static final String WITH_FACILITY = "WITH_FACILITY";
    public static final String WITHOUT_FACILITY = "WITHOUT_FACILITY";
	      public static final String FORGOT_PSWD_MODE = "FPONUNavigation.xml";
    public static final String FORGOT_PSWD_SMS = "SMS_FORPWD";
    public static final String EMAIL_SUBJECT = "EMAIL_SUB";
    public static final String EMAIL_CONTENT = "EMAIL_MSG";
    public static final String CLGDC = "CLGDC";
    public static final String CLGOTP = "CLGOTP";
	public static final String EKYC_COM = "EKYC_COM";
	public static final String PRIVY_COM = "DIG_SIGN_COM";	
	public static final String DEF_APP_STAUS_LOAN_ORG="LOAN_CREATED";
	public static final String ACT_LINK_CONF = "ACT_LINK_CONF";
	public static final String APP_EML_PEN ="APP_EML_PEN";
	public static final String PART_TRAN_TYPE_DEBIT="D";
	// Added for PINANG - Notification batch alerts
	public static final String EXPDATE = "EXPDATE";
	public static final String NEXT_APP_NOTIFICATION_ID_SEQ = "NEXT_APP_NOTIFICATION_ID_SEQ";
	public static final String CUSTOMER_NAME = "CUSTOMER_NAME";
}