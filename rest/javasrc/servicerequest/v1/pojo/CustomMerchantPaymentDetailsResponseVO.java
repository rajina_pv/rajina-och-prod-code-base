package com.infosys.custom.ebanking.rest.servicerequest.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomMerchantPaymentDetailsResponseVO extends RestHeaderFooterHandler{
	private CustomMerchantPaymentDetailsInOutVO data;
	
	public CustomMerchantPaymentDetailsInOutVO getData() {
		return data;
	}

	public void setData(CustomMerchantPaymentDetailsInOutVO data) {
		this.data = data;
	}

	
	
}
