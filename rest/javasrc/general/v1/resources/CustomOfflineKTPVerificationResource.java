package com.infosys.custom.ebanking.rest.general.v1.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.general.v1.pojo.CustomOfflineKTPVerificationReferencesResponseVO;
import com.infosys.custom.ebanking.rest.general.v1.pojo.CustomOfflineKTPVerificationReferencesVO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomOfflineKTPVerificationEnquiryVO;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.Header;
import com.infosys.feba.framework.interceptor.rest.pojo.RestCommonSuccessMessageHandler;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "common")
@Api(value = "Offline KTP Verification")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path(CustomResourcePaths.OFFLINE_KTP_VERIFICATION_URL)
@ApiResponses(value = { @ApiResponse(code = 400, message = ResourcePaths.HTTP_ERROR_400, response = Header.class),
		@ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 403, message = ResourcePaths.HTTP_ERROR_403, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 422, message = ResourcePaths.HTTP_ERROR_422, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class) })

public class CustomOfflineKTPVerificationResource extends ResourceAuthenticator {

	@Context
	HttpServletRequest request1;
	@Context
	HttpServletResponse httpResponse;

	final IClientStub serviceCStub = ServiceUtil
			.getService(new FEBAUnboundString("CustomOfflineKTPVerificationService"));

	@POST
	@ApiOperation(value = "Offline KTP Verification", response = CustomOfflineKTPVerificationReferencesResponseVO.class)
	@MethodInfo(uri = CustomResourcePaths.OFFLINE_KTP_VERIFICATION_URL)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Offline KTP Verification", response = CustomOfflineKTPVerificationReferencesResponseVO.class),
			@ApiResponse(code = 500, message = "Table update failed") })
	public Response hitCallback(CustomOfflineKTPVerificationReferencesVO customOfflineKTPVerificationReferencesVO,
			@ApiParam(value = "bank id", required = true) @PathParam("bankId") String bankId,
			@ApiParam(value = "app id", required = true) @PathParam("appId") String appId) {
		CustomOfflineKTPVerificationReferencesResponseVO responseVO = new CustomOfflineKTPVerificationReferencesResponseVO();
		List<CustomOfflineKTPVerificationReferencesVO> refListVO = new ArrayList<CustomOfflineKTPVerificationReferencesVO>();
		responseVO.setData(refListVO);
		IOpContext context = (IOpContext) request1.getAttribute("OP_CONTEXT");
		try {
			
			
			CustomOfflineKTPVerificationEnquiryVO enquiryVO = (CustomOfflineKTPVerificationEnquiryVO) FEBAAVOFactory
					.createInstance(CustomTypesCatalogueConstants.CustomOfflineKTPVerificationEnquiryVO);
			enquiryVO.getCriteria().setIsKTPVerified(customOfflineKTPVerificationReferencesVO.getIsKTPVerified());
			enquiryVO.getCriteria().setApplicationId(new ApplicationNumber(appId));

			CustomOfflineKTPVerificationReferencesVO refVO = updateKTPStatus(enquiryVO,context);
			responseVO.getData().add(refVO);
			
			RestCommonSuccessMessageHandler outputHeader = new RestCommonSuccessMessageHandler();
			
			RestResourceManager.updateHeaderFooterResponseData(outputHeader, request1, 0);
		} catch (Exception e) {
			
			LogManager.logError(context, e);
			
			RestResourceManager.handleFatalErrorOutput(request1, response, e, restResponseErrorCodeMapping());
		}
		return Response.ok().entity(responseVO).build();

	}
	@SuppressWarnings("rawtypes")
	private List restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<>();
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(100239, "BE", 401));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211052, "BE", 400));
		return restResponceErrorCode;
	}

	private CustomOfflineKTPVerificationReferencesVO updateKTPStatus(CustomOfflineKTPVerificationEnquiryVO enqVO,
			IOpContext context) throws FEBAException{
		CustomOfflineKTPVerificationReferencesVO refVO = new CustomOfflineKTPVerificationReferencesVO();
		enqVO = (CustomOfflineKTPVerificationEnquiryVO) serviceCStub.callService(context, enqVO,
				new FEBAUnboundString("update"));
		if(enqVO.getDetails().getResponseCode().toString().equalsIgnoreCase("200")){
			refVO.setResponseCode(enqVO.getDetails().getResponseCode().toString());
			refVO.setResponseStatus(enqVO.getDetails().getResponseStatus().toString());
		}
		return refVO;
	}


}
