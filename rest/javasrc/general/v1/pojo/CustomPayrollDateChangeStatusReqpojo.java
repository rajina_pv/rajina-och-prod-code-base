package com.infosys.custom.ebanking.rest.general.v1.pojo;

import io.swagger.annotations.ApiModelProperty;

public class CustomPayrollDateChangeStatusReqpojo {

	@ApiModelProperty(value = "From Date")
	private String fromDate;
	@ApiModelProperty(value = "To Date")
	private String toDate;
	@ApiModelProperty(value = "User Mobile No")
	private String mobileNo;

	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}


}
