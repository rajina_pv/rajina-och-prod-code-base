package com.infosys.custom.ebanking.rest.general.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomCreditScoringCallbackReferencesResponseVO extends RestHeaderFooterHandler {
	private List<CustomCreditScoringCallbackReferencesVO> data;

	public List<CustomCreditScoringCallbackReferencesVO> getData() {
		return data;
	}

	public void setData(List<CustomCreditScoringCallbackReferencesVO> data) {
		this.data = data;
	}
}
