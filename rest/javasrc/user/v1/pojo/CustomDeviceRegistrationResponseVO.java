package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

import io.swagger.annotations.ApiModel;

/**
 * 
 * Response VO of MPin Registration.
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value = "Device registration response details")
public class CustomDeviceRegistrationResponseVO extends RestHeaderFooterHandler {
	
	private CustomDeviceRegistrationOutputVO data;

	public CustomDeviceRegistrationOutputVO getData() {
		return data;
	}

	public void setData(CustomDeviceRegistrationOutputVO data) {
		this.data = data;
	}

	
}
