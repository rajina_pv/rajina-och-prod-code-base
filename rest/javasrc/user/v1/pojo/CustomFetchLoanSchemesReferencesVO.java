package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Loan Scheme Codes List")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomFetchLoanSchemesReferencesVO {

	
	@ApiModelProperty(value = "Scheme Code")
	private String schemeCode;
	
	@ApiModelProperty(value = "Scheme Description")
	private String schemeDesc;		

	@ApiModelProperty(value = "Minimum loan amount")
	private CustomAmountVO minLoanAmount;
	
	@ApiModelProperty(value = "Max loan amount")
	private CustomAmountVO maxLoanAmount;
	
	@ApiModelProperty(value = "Minimum Repay Months")
	private int minRepayMonths;
	
	@ApiModelProperty(value = "Minimum Repay Days")
	private int minRepayDays;
	
	@ApiModelProperty(value = "Max Repay Months")
	private int maxRepayMonths;
	
	@ApiModelProperty(value = "Max Repay Days")
	private int maxRepayDays;
	
	@ApiModelProperty(value = "Normal Interest")
	private double normalInterest;

	@ApiModelProperty(value = "Penal Interest")
	private double penalInterest;
	
	private List<CustomFeesReferencesVO> feeDetailsList;
	
	@ApiModelProperty(value = "Fees Count")
	private int feesCount;

	public String getSchemeCode() {
		return schemeCode;
	}

	public void setSchemeCode(String schemeCode) {
		this.schemeCode = schemeCode;
	}

	public String getSchemeDesc() {
		return schemeDesc;
	}

	public void setSchemeDesc(String schemeDesc) {
		this.schemeDesc = schemeDesc;
	}

	public CustomAmountVO getMinLoanAmount() {
		return minLoanAmount;
	}

	public void setMinLoanAmount(CustomAmountVO minLoanAmount) {
		this.minLoanAmount = minLoanAmount;
	}

	public CustomAmountVO getMaxLoanAmount() {
		return maxLoanAmount;
	}

	public void setMaxLoanAmount(CustomAmountVO maxLoanAmount) {
		this.maxLoanAmount = maxLoanAmount;
	}

	public int getMinRepayMonths() {
		return minRepayMonths;
	}

	public void setMinRepayMonths(int minRepayMonths) {
		this.minRepayMonths = minRepayMonths;
	}

	public int getMinRepayDays() {
		return minRepayDays;
	}

	public void setMinRepayDays(int minRepayDays) {
		this.minRepayDays = minRepayDays;
	}

	public int getMaxRepayMonths() {
		return maxRepayMonths;
	}

	public void setMaxRepayMonths(int maxRepayMonths) {
		this.maxRepayMonths = maxRepayMonths;
	}

	public int getMaxRepayDays() {
		return maxRepayDays;
	}

	public void setMaxRepayDays(int maxRepayDays) {
		this.maxRepayDays = maxRepayDays;
	}

	public double getNormalInterest() {
		return normalInterest;
	}

	public void setNormalInterest(double normalInterest) {
		this.normalInterest = normalInterest;
	}

	public double getPenalInterest() {
		return penalInterest;
	}

	public void setPenalInterest(double penalInterest) {
		this.penalInterest = penalInterest;
	}

	public List<CustomFeesReferencesVO> getFeeDetailsList() {
		return feeDetailsList;
	}

	public void setFeeDetailsList(List<CustomFeesReferencesVO> feeDetailsList) {
		this.feeDetailsList = feeDetailsList;
	}

	public int getFeesCount() {
		return feesCount;
	}

	public void setFeesCount(int feesCount) {
		this.feesCount = feesCount;
	}


}
