package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@XmlRootElement
@ApiModel(value = "Liveness")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomVidaLivenessInputVO {

		
	@ApiModelProperty(value = "Liveness Image")
	private String base64Image;

	/**
	 * @return the base64Image
	 */
	public String getBase64Image() {
		return base64Image;
	}

	/**
	 * @param base64Image the base64Image to set
	 */
	public void setBase64Image(String base64Image) {
		this.base64Image = base64Image;
	}
	

}
