package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.infosys.ebanking.rest.common.v1.pojo.BaseFileDetailsVO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Photo Validation")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomPhotoValidationReferenceVO {

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}		
			
	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@ApiModelProperty(value = "applicationId")
	private String applicationId;
	
	@ApiModelProperty(value = "token")
	private String token;
	
	@ApiModelProperty(value = "liveImage1")
	private BaseFileDetailsVO liveImage1;
	
	public BaseFileDetailsVO getLiveImage1() {
		return liveImage1;
	}

	public void setLiveImage1(BaseFileDetailsVO liveImage1) {
		this.liveImage1 = liveImage1;
	}

	public BaseFileDetailsVO getLiveImage2() {
		return liveImage2;
	}

	public void setLiveImage2(BaseFileDetailsVO liveImage2) {
		this.liveImage2 = liveImage2;
	}


	@ApiModelProperty(value = "liveImage2")
	private BaseFileDetailsVO liveImage2;

	@ApiModelProperty(value = "statusCode")
	private String statusCode;
	
	@ApiModelProperty(value = "status")
	private String status;

	public String getMerchantKey() {
		return merchantKey;
	}

	public void setMerchantKey(String merchantKey) {
		this.merchantKey = merchantKey;
	}

	@ApiModelProperty(value = "merchantKey")
	private String merchantKey;
	
	@ApiModelProperty(value = "userid")
	private String userid;
	
	@ApiModelProperty(value = "statusAfterCIFCreation")
	private String statusAfterCIFCreation;
	
	public String getStatusAfterCIFCreation() {
		return statusAfterCIFCreation;
	}

	public void setStatusAfterCIFCreation(String statusAfterCIFCreation) {
		this.statusAfterCIFCreation = statusAfterCIFCreation;
	}

	public String getStatusAfterDocCreation() {
		return statusAfterDocCreation;
	}

	public void setStatusAfterDocCreation(String statusAfterDocCreation) {
		this.statusAfterDocCreation = statusAfterDocCreation;
	}

	public String getStatusAfterPhotoCompare() {
		return statusAfterPhotoCompare;
	}

	public void setStatusAfterPhotoCompare(String statusAfterPhotoCompare) {
		this.statusAfterPhotoCompare = statusAfterPhotoCompare;
	}

	@ApiModelProperty(value = "statusAfterDocCreation")
	private String statusAfterDocCreation;
	
	@ApiModelProperty(value = "statusAfterPhotoCompare")
	private String statusAfterPhotoCompare;

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}
			
}
