package com.infosys.custom.ebanking.rest.user.v1.pojo;



import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * VO for state code references
 * @author Indupuri.Sravan
 *
 */
@ApiModel(value = "OTP Generation References Details")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomGenerateOtpReferencesVO {

	@ApiModelProperty(value = "Code")
	private String code;

	@ApiModelProperty(value = "Status")
	private String status;
	
	@ApiModelProperty(value = "Message")
	private String message;
	
	@ApiModelProperty(value = "Genereated Token")
	private String token;
	
	@ApiModelProperty(value = "Request Id")
	private String requestId;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	

}
