package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.infosys.ebanking.rest.common.v1.pojo.CodeReferencesVO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value= "Loan Product Maintenance Details")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomLoanProductReferencesVO {

	@ApiModelProperty(value = "Product Code")
	private String productCode;
	
	@ApiModelProperty(value = "Product Category")
	private String productCategory;
		
	@ApiModelProperty(value = "Product Sub Category")
	private String productSubCategory;
	
	@ApiModelProperty(value = "Config Type")
	private String configType;
	
	@ApiModelProperty(value = "Min amount")
	private Double minAmount;
	
	@ApiModelProperty(value = "Max Amount")
	private Double maxAmount;
	
	
	@ApiModelProperty(value = "Tenure Details List")
	private List<CustomTenureDetailsVO> tenureDetailsList;
	
	@ApiModelProperty(value = "Tenure Details List")
	private List<CodeReferencesVO> loanPurposeList;

	
	
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}
	public String getProductSubCategory() {
		return productSubCategory;
	}
	public void setProductSubCategory(String productSubCategory) {
		this.productSubCategory = productSubCategory;
	}
	public String getConfigType() {
		return configType;
	}
	public void setConfigType(String configType) {
		this.configType = configType;
	}
	public Double getMinAmount() {
		return minAmount;
	}
	public void setMinAmount(Double minAmount) {
		this.minAmount = minAmount;
	}
	public Double getMaxAmount() {
		return maxAmount;
	}
	public void setMaxAmount(Double maxAmount) {
		this.maxAmount = maxAmount;
	}

	public List<CustomTenureDetailsVO> getTenureDetails() {
		return tenureDetailsList;
	}
	public void setTenureDetails(List<CustomTenureDetailsVO> tenureDetails) {
		this.tenureDetailsList = tenureDetails;
	}
	public List<CodeReferencesVO> getLoanPurpose() {
		return loanPurposeList;
	}
	public void setLoanPurpose(List<CodeReferencesVO> loanPurpose) {
		this.loanPurposeList = loanPurpose;
	}

	public CustomLoanProductReferencesVO(String productCode, String productCategory, String productSubCategory,
			String configType, Double minAmount, Double maxAmount, List<CustomTenureDetailsVO> tenureDetails,
			List<CodeReferencesVO> loanPurpose) {
		super();
		this.productCode = productCode;
		this.productCategory = productCategory;
		this.productSubCategory = productSubCategory;
		this.configType = configType;
		this.minAmount = minAmount;
		this.maxAmount = maxAmount;
		this.tenureDetailsList = tenureDetails;
		this.loanPurposeList = loanPurpose;
	}


	public CustomLoanProductReferencesVO(){
		super();
	}
	
}
