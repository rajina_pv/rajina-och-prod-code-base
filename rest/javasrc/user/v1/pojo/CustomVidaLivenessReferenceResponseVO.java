package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomVidaLivenessReferenceResponseVO extends RestHeaderFooterHandler {
	private CustomVidaLivenessReferenceVO data;

	public CustomVidaLivenessReferenceVO getData() {
		return data;
	}

	public void setData(CustomVidaLivenessReferenceVO data) {
		this.data = data;
	}
}
