package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Fee List")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomFeesReferencesVO {
	
	
	@ApiModelProperty(value = "Minimum  amount")
	private CustomAmountVO minAmount;
	
	@ApiModelProperty(value = "Max  amount")
	private CustomAmountVO maxAmount;
	
	@ApiModelProperty(value = "Fee amount")
	private CustomAmountVO feeAmount;

	public CustomAmountVO getMinAmount() {
		return minAmount;
	}

	public void setMinAmount(CustomAmountVO minAmount) {
		this.minAmount = minAmount;
	}

	public CustomAmountVO getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(CustomAmountVO maxAmount) {
		this.maxAmount = maxAmount;
	}

	public CustomAmountVO getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(CustomAmountVO feeAmount) {
		this.feeAmount = feeAmount;
	}
	
}
