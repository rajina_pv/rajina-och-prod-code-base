package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomLoanSimulationReferencesResponseVO extends RestHeaderFooterHandler{
	private List<CustomLoanSimulationReferencesVO> data;
	
	public List<CustomLoanSimulationReferencesVO> getData() {
		return data;
	}

	public void setData(List<CustomLoanSimulationReferencesVO> data) {
		this.data = data;
	}

	
	
}
