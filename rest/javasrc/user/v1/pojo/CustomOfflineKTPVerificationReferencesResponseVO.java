package com.infosys.custom.ebanking.rest.general.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomOfflineKTPVerificationReferencesResponseVO extends RestHeaderFooterHandler {
	private List<CustomOfflineKTPVerificationReferencesVO> data;

	public List<CustomOfflineKTPVerificationReferencesVO> getData() {
		return data;
	}

	public void setData(List<CustomOfflineKTPVerificationReferencesVO> data) {
		this.data = data;
	}
}
