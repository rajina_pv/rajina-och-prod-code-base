package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomStandardTextResponseVO extends RestHeaderFooterHandler {

	private List<CustomHelpTopicReferenceVO> data;

	public List<CustomHelpTopicReferenceVO> getData() {
		return data;
	}

	public void setData(List<CustomHelpTopicReferenceVO> data) {
		this.data = data;
	}
}
