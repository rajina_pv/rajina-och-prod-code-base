package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.infosys.ebanking.rest.common.v1.pojo.PasswordDetailsVO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Anju_AL
 * 
 * This is the VO which holds the fields for the Forgot Password Online request for retail user input.
 *
 */
@ApiModel(value="The input details for resetting the password online in case the user forgets the password.")
@JsonInclude(JsonInclude.Include.NON_EMPTY)

public class CustomForgotPasswordOnlineInputDetailsVO {

	@ApiModelProperty(value="The user ID for which the password should be reset.", required=true)
	@Size(min=0,max=20)
	private String loginUserId;

	
	
	@ApiModelProperty(value="The authentication details of the user.", required=true)
	public CustomAuthenticationDetailsVO authenticationDetails;
	
	@ApiModelProperty(value="The password details of the user.", required=true)
	public PasswordDetailsVO passwordDetails;
	
	public String getLoginUserId() {
		return loginUserId;
	}

	public void setLoginUserId(String loginUserId) {
		this.loginUserId = loginUserId;
	}


	public CustomAuthenticationDetailsVO getAuthenticationDetails() {
		return authenticationDetails;
	}

	public void setAuthenticationDetails(CustomAuthenticationDetailsVO authenticationDetails) {
		this.authenticationDetails = authenticationDetails;
	}

	public PasswordDetailsVO getPasswordDetails() {
		return passwordDetails;
	}

	public void setPasswordDetails(PasswordDetailsVO passwordDetails) {
		this.passwordDetails = passwordDetails;
	}

	
}
