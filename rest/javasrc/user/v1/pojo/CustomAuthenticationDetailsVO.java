package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Anju_AL
 * 
 * This is the VO which holds the fields for authentication details.
 *
 */
@ApiModel(value="The authentication details of the user.")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomAuthenticationDetailsVO {
	
	@ApiModelProperty(value="The one time password or OTP that should be entered for authentication.", required=false)
	@Size(min=0,max=100)
	private String oneTimePassword;
	
	@ApiModelProperty(value="The debit card details of the user required for authentication.", required=false)
	private CustomDebitCardDetailsVO debitCardDetails;
	
	@ApiModelProperty(value="The channel needed for authentication.", required=false)
	private String channel;

	public String getOneTimePassword() {
		return oneTimePassword;
	}

	public void setOneTimePassword(String oneTimePassword) {
		this.oneTimePassword = oneTimePassword;
	}
	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public CustomDebitCardDetailsVO getDebitCardDetails() {
		return debitCardDetails;
	}

	public void setDebitCardDetails(CustomDebitCardDetailsVO debitCardDetails) {
		this.debitCardDetails = debitCardDetails;
	}

	
	
}
