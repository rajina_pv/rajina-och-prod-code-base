/**
 * CustomLoanApplicationDocumentListReferenceVO.java
 * @since Oct 4, 2018 - 11:22:58 AM
 *
 * COPYRIGHT NOTICE:
 * Copyright (c) 2018 Infosys Technologies Limited, Electronic City,
 * Hosur Road, Bangalore - 560 100, India.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Infosys Technologies Ltd. ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered
 * into with Infosys.
 */
package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author Tarun_Kumar18
 *
 */
public class CustomLoanApplicationDocumentListReferenceVO {
	
	@ApiModelProperty(value = "List of Document Type Code Reference") 
	private List<CustomLoanApplicationDocumentVO> applicationDocuments;

	public List<CustomLoanApplicationDocumentVO> getApplicationDocuments() {
		return applicationDocuments;
	}

	public void setApplicationDocuments(List<CustomLoanApplicationDocumentVO> applicationDocuments) {
		this.applicationDocuments = applicationDocuments;
	}


}
