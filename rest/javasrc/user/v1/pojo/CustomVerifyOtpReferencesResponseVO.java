package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomVerifyOtpReferencesResponseVO extends RestHeaderFooterHandler {

	private List<CustomVerifyOtpReferencesVO> data;

	public List<CustomVerifyOtpReferencesVO> getData() {
		return data;
	}

	public void setData(List<CustomVerifyOtpReferencesVO> data) {
		this.data = data;
	}

	

	
}
