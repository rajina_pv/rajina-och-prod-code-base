package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class CustomLoanApplicationCreationOutputVO {
	@ApiModelProperty(value = "Application id")
	private Long applicationId;
	
	@ApiModelProperty(value = "last action date")
	private Date lastActionDate;
	
	@ApiModelProperty(value = "Application status")
	private CustomApplicationStatusVO applicationStatus;
	
	@ApiModelProperty(value = "Application status")
	private long timeLeftForScoreCompletion;

	public Long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	public Date getLastActionDate() {
		return lastActionDate;
	}

	public void setLastActionDate(Date lastActionDate) {
		this.lastActionDate = lastActionDate;
	}

	public CustomApplicationStatusVO getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(CustomApplicationStatusVO applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public long getTimeLeftForScoreCompletion() {
		return timeLeftForScoreCompletion;
	}

	public void setTimeLeftForScoreCompletion(long timeLeftForScoreCompletion) {
		this.timeLeftForScoreCompletion = timeLeftForScoreCompletion;
	}
}
