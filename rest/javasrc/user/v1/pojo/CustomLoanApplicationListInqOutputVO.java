package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class CustomLoanApplicationListInqOutputVO {
	
	@ApiModelProperty(value = "Application id")
	private Long applicationId;
	
	@ApiModelProperty(value = "Application status")
	private CustomApplicationStatusVO applicationStatus;	
	
	@ApiModelProperty(value = "last action date")
	private Date lastActionDate;
	
	@ApiModelProperty(value = "last action date")
	private long timeLeftForScoreCompletion;
	
	@ApiModelProperty(value = "is email verified")
	private char isEmailVerified;
	
	@ApiModelProperty(value = "rejection details")
	private CustomRejectionDetailsVO rejectionDetails;
	
	@ApiModelProperty(value = "approval details")
	private CustomApprovalDetailsVO approvalDetails;
	
	@ApiModelProperty(value = "incomplete application details")
	private CustomIncompleteApplicationDetailsVO incompleteApplicationDetails;
	
	@ApiModelProperty(value = "loan limit details")
	private CustomLoanLimitDetailsVO loanLimitDetails;
	
	@ApiModelProperty(value = "payroll rejection details")
	private CustomPayrollRejDetailsVO payrollrejDetails;
	
	@ApiModelProperty(value = "Loan paid application Flag")
	private char isSecondApply;
	
	@ApiModelProperty(value = "Previous Loan Paid Application id")
	private Long previousLoanPaidAppId;
	
	@ApiModelProperty(value = "Previous Loan Paid Application status")
	private String previousLoanAppStatus;
	
// Added KTP in Applciation inq Resposne--bug fix (for apk cache) -->START
	@ApiModelProperty(value = "Ktp Number")
	private String ktpNum;
	
	public String getKtpNum() {
		return ktpNum;
	}

	public void setKtpNum(String ktpNum) {
		this.ktpNum = ktpNum;
	}

	// Added KTP in Applciation inq Resposne--bug fix (for apk cache) -->END
	public Long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	public CustomApplicationStatusVO getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(CustomApplicationStatusVO applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public Date getLastActionDate() {
		return lastActionDate;
	}

	public void setLastActionDate(Date lastActionDate) {
		this.lastActionDate = lastActionDate;
	}

	public long getTimeLeftForScoreCompletion() {
		return timeLeftForScoreCompletion;
	}

	public void setTimeLeftForScoreCompletion(long timeLeftForScoreCompletion) {
		this.timeLeftForScoreCompletion = timeLeftForScoreCompletion;
	}

	public char getIsEmailVerified() {
		return isEmailVerified;
	}

	public void setIsEmailVerified(char isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
	}

	public CustomRejectionDetailsVO getRejectionDetails() {
		return rejectionDetails;
	}

	public void setRejectionDetails(CustomRejectionDetailsVO rejectionDetails) {
		this.rejectionDetails = rejectionDetails;
	}

	public CustomApprovalDetailsVO getApprovalDetails() {
		return approvalDetails;
	}

	public void setApprovalDetails(CustomApprovalDetailsVO approvalDetails) {
		this.approvalDetails = approvalDetails;
	}

	public CustomIncompleteApplicationDetailsVO getIncompleteApplicationDetails() {
		return incompleteApplicationDetails;
	}

	public void setIncompleteApplicationDetails(CustomIncompleteApplicationDetailsVO incompleteApplicationDetails) {
		this.incompleteApplicationDetails = incompleteApplicationDetails;
	}

	public CustomLoanLimitDetailsVO getLoanLimitDetails() {
		return loanLimitDetails;
	}

	public void setLoanLimitDetails(CustomLoanLimitDetailsVO loanLimitDetails) {
		this.loanLimitDetails = loanLimitDetails;
	}

	public CustomPayrollRejDetailsVO getPayrollrejdetails() {
		return payrollrejDetails;
	}

	public void setPayrollrejdetails(CustomPayrollRejDetailsVO payrollrejDetails) {
		this.payrollrejDetails = payrollrejDetails;
	}

	public char getIsSecondApply() {
		return isSecondApply;
	}

	public void setIsSecondApply(char isSecondApply) {
		this.isSecondApply = isSecondApply;
	}

	public Long getPreviousLoanPaidAppId() {
		return previousLoanPaidAppId;
	}

	public void setPreviousLoanPaidAppId(Long previousLoanPaidAppId) {
		this.previousLoanPaidAppId = previousLoanPaidAppId;
	}

	public String getPreviousLoanAppStatus() {
		return previousLoanAppStatus;
	}

	public void setPreviousLoanAppStatus(String previousLoanAppStatus) {
		this.previousLoanAppStatus = previousLoanAppStatus;
	}
	
	
}
