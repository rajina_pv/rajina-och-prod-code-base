package com.infosys.custom.ebanking.rest.user.v1.pojo;


import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomDocTextResponseReferencesResponsePOJO extends RestHeaderFooterHandler{
	private List<CustomDocTextResponsePOJO> data;
	
	public List<CustomDocTextResponsePOJO> getData() {
		return data;
	}

	public void setData(List<CustomDocTextResponsePOJO> data) {
		this.data = data;
	}
	
	// Added for PINANG Pay later start
	private String schemeCode;

	public String getSchemeCode() {
		return schemeCode;
	}

	public void setSchemeCode(String schemeCode) {
		this.schemeCode = schemeCode;
	}
	// Added for PINANG Pay later end
}
