package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomLoanApplicationListInqResponseVO extends RestHeaderFooterHandler {
	private List<CustomLoanApplicationListInqOutputVO> data = null;
	
	public void setData(List<CustomLoanApplicationListInqOutputVO> data) {
		this.data = data;
	}

	public List<CustomLoanApplicationListInqOutputVO> getData() {
		return data;
	}
}
