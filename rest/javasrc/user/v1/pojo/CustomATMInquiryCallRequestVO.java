package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@XmlRootElement
@ApiModel(value = "ATM inquiry input details")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomATMInquiryCallRequestVO {
	@ApiModelProperty(value = "Application id", required = true)
	private String ktp;
	
	@ApiModelProperty(value = "card number", required = true)
	private String cardNumber;
	
	@ApiModelProperty(value = "expiry date", required = true)
	private String expiryDate;
	
	@ApiModelProperty(value = "Bank Code", required  = true)
	private String bankCode;
	
	@ApiModelProperty(value = "product Code", required  = true)
	private String productCode;
	

	public String getKtp() {
		return ktp;
	}

	public void setKtp(String ktp) {
		this.ktp = ktp;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

}
