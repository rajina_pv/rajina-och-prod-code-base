package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Bank List Inquiry")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomBankListInquiryReferencesVO {
		
	@ApiModelProperty(value = "Bank Details List")
	private List<CustomBankDetailsVO> bankDetailsList;

	public List<CustomBankDetailsVO> getBankDetails() {
		return bankDetailsList;
	}

	public void setBankDetails(List<CustomBankDetailsVO> bankDetails) {
		this.bankDetailsList = bankDetails;
	}
	
	

}
