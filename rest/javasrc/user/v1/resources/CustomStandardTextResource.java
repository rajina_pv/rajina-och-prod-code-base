package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomHelpSubTopicVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomHelpTopicReferenceVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomStandardTextResponseVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomDocTextResultVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.hif.camel.beans.Header;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.DALException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.lists.FEBAHashList;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.CommonCodeVO;
import com.infosys.fentbase.common.FBAErrorCodes;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Fetch Standard Text for Help Topics")
@Path(CustomResourcePaths.HELP_TOPIC_INQ)
@ApiResponses(value = { @ApiResponse(code = 400, message = ResourcePaths.HTTP_ERROR_400, response = Header.class),
		@ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 403, message = ResourcePaths.HTTP_ERROR_403, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 422, message = ResourcePaths.HTTP_ERROR_422, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class) })

public class CustomStandardTextResource extends ResourceAuthenticator {

	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse httpResponse;

	@Context
	UriInfo uriInfo;

	/**
	 * Authenticates if the userid and bankid who is trying to access the
	 * resource is same as the userid and bankid which is associated with the
	 * context.
	 *
	 * @return boolean if the userId and bankId is authenticated it returns
	 *         false
	 * @throws Exception
	 */
	@Override
	protected boolean isUnAuthenticatedRequest() throws Exception {

		IOpContext opContext = (OpContext) request.getAttribute("OP_CONTEXT");
		MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();

		String bankid = pathParams.getFirst("bankid");
		String userid = pathParams.getFirst("userid");

		// Fetch the user details from the context.
		BankId bankId = (BankId) opContext.getFromContextData(EBankingConstants.BANK_ID);
		UserId userId = (UserId) opContext.getFromContextData(EBankingConstants.USER_ID);

		boolean isInValid;
		if (bankId.getValue().equals(bankid) && userId.getValue().equals(userid)) {
			isInValid = false;
		} else {
			isInValid = true;
		}

		if (isInValid) {
			throw new BusinessException(opContext, EBIncidenceCodes.USER_NOT_AUTHORIZED_TO_CONFIRM,
					"The user is not authorized to perform the action.", FBAErrorCodes.UNATHORIZED_USER);
		}

		return isInValid;
	}

	/**
	 * @param userid
	 * @param bankId
	 * @return JSON response for Help Topic API
	 */
	@GET
	@MethodInfo(uri = CustomResourcePaths.HELP_TOPIC_INQ)
	@ApiOperation(value = "Fetch Help Topics", response = CustomStandardTextResponseVO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Help topic fetched successfully", response = CustomStandardTextResponseVO.class),
			@ApiResponse(code = 404, message = "Help topic do not exist"),
			@ApiResponse(code = 500, message = "Internal Server Error"), })

	public Response fetchHelpTopics(
			@ApiParam(value = "User Id input", required = true) @PathParam("userid") String userid,
			@ApiParam(value = "Bank Id input", required = true) @PathParam("bankid") String bankId) {

		CustomStandardTextResponseVO responseVO = new CustomStandardTextResponseVO();
		FEBATransactionContext txnContext = null;
		try {
			isUnAuthenticatedRequest();
			IOpContext opContext = (IOpContext) request.getAttribute("OP_CONTEXT");
			txnContext = RestCommonUtils.getTransactionContext(opContext);
			HashMap<String, String> textList = fetchStandardText(txnContext);
			List<CustomHelpTopicReferenceVO> ouputVO = fetchHelpTopic(opContext, textList);
			responseVO.setData(ouputVO);
			RestResourceManager.updateHeaderFooterResponseData(responseVO, request, 0);

		} catch (Exception e) {
			RestResourceManager.handleFatalErrorOutput(request, e, restResponseErrorCodeMapping());
		} finally {
			if (txnContext != null) {
				try {
					txnContext.cleanup();
				} catch (CriticalException e) {
					LogManager.logError(null, e);
				}
			}
		}

		return Response.ok().entity(responseVO).build();
	}

	/**
	 * @param txnContext
	 * @return Hashmap for Standard Text
	 */
	@SuppressWarnings("unchecked")
	private HashMap<String, String> fetchStandardText(FEBATransactionContext txnContext) {

		QueryOperator queryOperator = QueryOperator.openHandle(txnContext,
				CustomEBQueryIdentifiers.CUSTOM_DOCUMENT_TEXT_MAINTENANCE);
		queryOperator.associate("bankId", txnContext.getBankId());
		queryOperator.associate("textType", new FEBAUnboundString(CustomEBConstants.CODE_TYPE_HELP_TOPIC));
		FEBAArrayList<CustomDocTextResultVO> list = new FEBAArrayList<>();
		try {
			list = queryOperator.fetchList(txnContext);
		} catch (DALException e) {
			LogManager.logError(txnContext, e);
		} finally {
			queryOperator.closeHandle(txnContext);
		}
		HashMap<String, String> textMap = new HashMap<>();
		list.forEach(docTextVO -> textMap.put(docTextVO.getTextName().toString(), docTextVO.getText().toString()));
		return textMap;
	}

	/**
	 * @param opContext
	 * @param textList
	 * @return formatted help topics
	 */
	@SuppressWarnings("unchecked")
	private List<CustomHelpTopicReferenceVO> fetchHelpTopic(IOpContext opContext, HashMap<String, String> textList) {

		List<CustomHelpTopicReferenceVO> outputVO = new ArrayList<>();
		FEBAHashList<CommonCodeVO> helpTopicList = RestCommonUtils.getList(opContext,
				CustomEBConstants.CODE_TYPE_HELP_TOPIC);
		helpTopicList.forEach(helpTopic -> {
			FEBAHashList<CommonCodeVO> helpSubTopicList = RestCommonUtils.getList(opContext,
					helpTopic.getCommonCode().toString());
			List<CustomHelpSubTopicVO> subTopics = new ArrayList<>();
			helpSubTopicList.forEach(subTopic -> {
				CustomHelpSubTopicVO subTopicVO = new CustomHelpSubTopicVO();
				subTopicVO.setSubTopic(subTopic.getCodeDescription().toString());
				subTopicVO.setDetails(textList.getOrDefault(subTopic.getCommonCode().toString(), ""));
				subTopics.add(subTopicVO);
			});
			CustomHelpTopicReferenceVO helpTopicVO = new CustomHelpTopicReferenceVO();
			helpTopicVO.setHelpTopic(helpTopic.getCodeDescription().toString());
			helpTopicVO.setSubTopics(subTopics);
			outputVO.add(helpTopicVO);
		});
		return outputVO;
	}

	/**
	 * @return error code mapping to HTTP status code
	 */
	private List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {
		return new ArrayList<>();
	}

}
