package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomAccountListInquiryReferencesResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomAccountListInquiryReferencesVO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomAccountListDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomAccountListEnquiryVO;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "USER")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Custom Account List Balance Details")
public class CustomAccountListInquiryResource extends ResourceAuthenticator {


	@Context  
	HttpServletRequest request;
	
	@Context
	HttpServletResponse response;

    @GET
    @ApiOperation(value = "Fetch Account List", response = CustomAccountListInquiryReferencesResponseVO.class)
    @MethodInfo(uri = CustomResourcePaths.CUSTOM_ACCOUNT_LIST_INQUIRY)
    @ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Retrieval of Account List", response = CustomAccountListInquiryReferencesResponseVO.class),
			@ApiResponse(code = 404, message = "Notification List do not exist"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
    
    
    public Response fetchAccountList (
    	@ApiParam(value = "Bank Id input", required = true) @PathParam("bankid") String bankId,
		@ApiParam(value = "User Id input", required = true) @PathParam("userid") String userId,
		@QueryParam("accountOpeningDateFrom") String accountOpeningDateFrom,@QueryParam("accountOpeningDateTo") String accountOpeningDateTo,
		@QueryParam("accountStatus") String accountStatus){
    	IOpContext opcontext = (IOpContext) request.getAttribute("OP_CONTEXT");
    	CustomAccountListInquiryReferencesResponseVO responseVO= new CustomAccountListInquiryReferencesResponseVO();
    	
    	
    	try {
    		isUnAuthenticatedRequest();	
    		List <CustomAccountListInquiryReferencesVO> outputVO = fetchAccountList (userId,accountOpeningDateFrom,accountOpeningDateTo,accountStatus,opcontext);
    		responseVO.setList(outputVO); 		
    	}
    	
    	catch (Exception e) {
    		e.printStackTrace();
			RestResourceManager.handleFatalErrorOutput(request, response, e, restResponseErrorCodeMapping());
    		
    	}
    	
    	return Response.ok().entity(responseVO).build();
    }
    
    public List <CustomAccountListInquiryReferencesVO> fetchAccountList (String userid,String accountOpeningDateFrom, String accountOpeningDateTo ,String accountStatus,IOpContext context) throws FEBAException {
    	
    	List <CustomAccountListInquiryReferencesVO> outputVO = new ArrayList<CustomAccountListInquiryReferencesVO>();
    	
    	CustomAccountListEnquiryVO enquiryVO = (CustomAccountListEnquiryVO) FEBAAVOFactory.createInstance(CustomTypesCatalogueConstants.CustomAccountListEnquiryVO);
    	
    	enquiryVO.setUserId(userid);
    	
    	final IClientStub serviceStub = ServiceUtil.getService(new FEBAUnboundString("CustomAccountListInquiryService"));
		enquiryVO = (CustomAccountListEnquiryVO) serviceStub.callService(context, enquiryVO, new FEBAUnboundString("fetch"));
		
		FEBAArrayList<CustomAccountListDetailsVO> detailsListVO = enquiryVO.getResultList();
		
		for (int i = 0; i < detailsListVO.size(); i++) {
			CustomAccountListDetailsVO detailsVO = detailsListVO.get(i);
			
			CustomAccountListInquiryReferencesVO outVO = new CustomAccountListInquiryReferencesVO();
			
			outVO.setAccountOpenDate(detailsVO.getPurchaseDate().toString());
			outVO.setPurchaseAmount(detailsVO.getPurchaseAmount().toString());
			outVO.setInstallmentAmount(detailsVO.getInstallmentAmount().toString());
			outVO.setTotalOutstandingBalance(detailsVO.getTotalOutstandingBalance().toString());
			outVO.setNumberOfInstallments(detailsVO.getNumberOfInstallments().toString());
			outVO.setMerchantId(detailsVO.getMerchantId().toString());
			outVO.setPurchaseRemarks(detailsVO.getPurchaseRemarks().toString());
			outputVO.add(outVO);
		}
    
    	return outputVO;
    }
    
    protected List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<RestResponseErrorCodeMapping>();
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211041, "BE", 400));
		return restResponceErrorCode;
	}
    
    
}
