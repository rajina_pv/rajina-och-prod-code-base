/**
 * CustomLoanApplicationDocumentListResource.java
 * @since Oct 4, 2018 - 11:46:43 AM
 *
 * COPYRIGHT NOTICE:
 * Copyright (c) 2018 Infosys Technologies Limited, Electronic City,
 * Hosur Road, Bangalore - 560 100, India.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Infosys Technologies Ltd. ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered
 * into with Infosys.
 */
package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanApplicationDocumentListReferenceVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanApplicationDocumentResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanApplicationDocumentVO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationDocumentCriteriaVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationDocumentEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsDetailsVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.Header;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.fentbase.common.FBAErrorCodes;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Tarun_Kumar18
 * 
 *         REST API to fetch all Loan Application document of a user
 */

@ServiceInfo(moduleName = "users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Application document List Inquiry")
@Path(CustomResourcePaths.CUSTOM_APPLICATION_DOCUMENT_LIST_RESOURCE)
@ApiResponses(value = { @ApiResponse(code = 400, message = ResourcePaths.HTTP_ERROR_400, response = Header.class),
		@ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 403, message = ResourcePaths.HTTP_ERROR_403, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 422, message = ResourcePaths.HTTP_ERROR_422, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class), })

public class CustomLoanApplicationDocumentListResource extends ResourceAuthenticator {

	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse httpResponse;

	@Context
	UriInfo uriInfo;

	/**
	 * Authenticates if the userid and bankid who is trying to access the
	 * resource is same as the userid and bankid which is associated with the
	 * context.
	 *
	 * @return boolean if the userId and bankId is authenticated it returns
	 *         false
	 * @throws Exception
	 */
	@Override
	protected boolean isUnAuthenticatedRequest() throws Exception {

		IOpContext opContext = (OpContext) request.getAttribute("OP_CONTEXT");
		MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();

		String bankid = pathParams.getFirst("bankid");
		String userid = pathParams.getFirst("userid");

		// Fetch the user details from the context.
		BankId bankId = (BankId) opContext.getFromContextData(EBankingConstants.BANK_ID);
		UserId userId = (UserId) opContext.getFromContextData(EBankingConstants.USER_ID);

		boolean isInValid;
		if (bankId.getValue().equals(bankid) && userId.getValue().equals(userid)) {
			isInValid = false;
		} else {
			isInValid = true;
		}

		if (isInValid) {
			throw new BusinessException(opContext, EBIncidenceCodes.USER_NOT_AUTHORIZED_TO_CONFIRM,
					"The user is not authorized to perform the action.", FBAErrorCodes.UNATHORIZED_USER);
		}

		return isInValid;
	}

	/**
	 * @param bankId
	 * @param userId
	 * @return JSON Object
	 */
	@GET
	@MethodInfo(uri = CustomResourcePaths.CUSTOM_APPLICATION_DOCUMENT_LIST_RESOURCE, auditFields = {
			"fileSeqNum : File Sequence Number of document" })
	@ApiOperation(value = "Fetch list of document for loan applications of user", response = CustomLoanApplicationDocumentResponseVO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Loan application document fetched successfully", response = CustomLoanApplicationDocumentResponseVO.class),
			@ApiResponse(code = 404, message = "Loan application document does not exist"),
			@ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 500, message = "Internal Server Error") })

	public Response fetchDocuments(
			@ApiParam(value = "Bank Id input", required = true) @PathParam("bankid") String bankId,
			@ApiParam(value = "User Id input", required = true) @PathParam("userid") String userId) {

		CustomLoanApplicationDocumentResponseVO responseVO = new CustomLoanApplicationDocumentResponseVO();
		try {
			isUnAuthenticatedRequest();
			MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();
			String userid = pathParams.getFirst("userid");
			String bankid = pathParams.getFirst("bankid");
			IOpContext opContext = (OpContext) request.getAttribute("OP_CONTEXT");
			List<CustomLoanApplicationDocumentListReferenceVO> outputVO = fetchLoanApplicationDocuments(opContext,
					bankid, userid);
			responseVO.setData(outputVO);
			RestResourceManager.updateHeaderFooterResponseData(responseVO, request, 0);
		} catch (Exception e) {
			RestResourceManager.handleFatalErrorOutput(request, response, e, restResponseErrorCodeMapping());
		}
		return Response.ok().entity(responseVO).build();
	}

	/**
	 * @param opContext
	 * @param bankid
	 * @param userid
	 * @return List of application Documents
	 * @throws BusinessConfirmation
	 * @throws BusinessException
	 * @throws CriticalException
	 */
	private List<CustomLoanApplicationDocumentListReferenceVO> fetchLoanApplicationDocuments(IOpContext opContext,
			String bankid, String userid) throws CriticalException, BusinessException, BusinessConfirmation {

		CustomLoanApplicationDocumentCriteriaVO criteriaVO = (CustomLoanApplicationDocumentCriteriaVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomLoanApplicationDocumentCriteriaVO);
		CustomLoanApplicationDocumentEnquiryVO enquiryVO = (CustomLoanApplicationDocumentEnquiryVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomLoanApplicationDocumentEnquiryVO);

		criteriaVO.setBankId(bankid);
		criteriaVO.setUserId(userid);
		enquiryVO.setCriteria(criteriaVO);

		final IClientStub serviceStub = ServiceUtil
				.getService(new FEBAUnboundString("CustomLoanApplicationDocumentService"));
		enquiryVO = (CustomLoanApplicationDocumentEnquiryVO) serviceStub.callService(opContext, enquiryVO,
				new FEBAUnboundString("fetch"));
		return formatOutputVO(opContext, enquiryVO);
	}

	/**
	 * @param opContext
	 * @param enquiryVO
	 * @return Formatted outputVO
	 */
	@SuppressWarnings("unchecked")
	private List<CustomLoanApplicationDocumentListReferenceVO> formatOutputVO(IOpContext opContext,
			CustomLoanApplicationDocumentEnquiryVO enquiryVO) {

		List<CustomLoanApplicationDocumentListReferenceVO> outputVO = new ArrayList<>();
		FEBAArrayList<CustomLoanApplnDocumentsDetailsVO> result = enquiryVO.getResultList();
		String prevApplnId = "";
		List<CustomLoanApplicationDocumentVO> applicationDocuments = new ArrayList<>();
		for (CustomLoanApplnDocumentsDetailsVO detailsVO : result) {
			if (!detailsVO.getApplicationId().toString().equals(prevApplnId)) {
				applicationDocuments = new ArrayList<>();
			}

			CustomLoanApplicationDocumentVO docVO = new CustomLoanApplicationDocumentVO();
			docVO.setFileSeqNum(detailsVO.getFileSeqNo().toString());
			docVO.setDocumentType(RestCommonUtils.getCodeReferences(opContext,
					CustomEBConstants.LOAN_APPLN_DOC_CODE_TYPE, detailsVO.getDocType().toString()));
			applicationDocuments.add(docVO);

			if (!detailsVO.getApplicationId().toString().equals(prevApplnId)) {
				CustomLoanApplicationDocumentListReferenceVO refVO = new CustomLoanApplicationDocumentListReferenceVO();
				refVO.setApplicationDocuments(applicationDocuments);
				outputVO.add(refVO);
			}
			prevApplnId = detailsVO.getApplicationId().toString();
		}
		return outputVO;
	}

	/**
	 * @return error code mapping to HTTP status code
	 */
	private List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {

		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<>();
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(200605, "CE", 500));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(200606, "BE", 400));
		return restResponceErrorCode;
	}
}
