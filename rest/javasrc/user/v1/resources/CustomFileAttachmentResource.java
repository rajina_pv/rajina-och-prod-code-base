package  com.infosys.custom.ebanking.rest.user.v1.resources;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.ebanking.types.TypesCatalogueConstants;
import com.infosys.ebanking.types.valueobjects.FDTTDetailsVO;
import com.infosys.ebanking.types.valueobjects.FileDownloadEnquiryVO;

import com.infosys.feba.framework.common.ErrorCodes;
import com.infosys.feba.framework.common.FEBAConstants;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessExceptionVO;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.Header;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.primitives.FEBAOperatingMode;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.FileSequenceNumber;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.utils.insulate.ArrayList;
import com.infosys.feba.utils.insulate.HashMap;
import com.infosys.fentbase.common.FBAErrorCodes;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@ServiceInfo(moduleName = "Fileupload")
@Api(value="Custom File Download resource")
@Path("/v1/banks/{bankid}/users/{userid}/custom/fileattachment/{fileseqno}")
@ApiResponses(value = {
		@ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 404, message = ResourcePaths.HTTP_ERROR_404, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class)
})
public class CustomFileAttachmentResource extends ResourceAuthenticator{

	@Context
	private HttpServletRequest request;

	protected HashMap files = new HashMap();


	String tempFileName;

	@Context  UriInfo uriInfo;


	/**
	 * This method downloads the file as an attachment.
	 * 
	 * @param fileSeqNo
	 * @return Response
	 */
	@GET
	@Produces({MediaType.APPLICATION_OCTET_STREAM,MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@ApiOperation(value="Download file of a service request",response=String.class)
	@MethodInfo(uri ="/v1/banks/{bankid}/users/{userid}/custom/fileattachment/{fileseqno}", auditFields = { "bankid:The ID of the bank" })
//	@Path(ResourcePaths.FILE_SEQ_NO)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "File download successful.", response = String.class) })
	public Response downloadFile(@ApiParam(value = "The ID of the bank.")  @PathParam("bankid") String bankId,@ApiParam(value = "The ID of the retail user.")  @PathParam("userid") String userId,@ApiParam(value = "The Sequence Number of the file to be downloaded.")  @PathParam("fileseqno") String fileSeqNo) {



		ResponseBuilder responseBuilder = null;

		try {
			IOpContext opcontext = (OpContext) request.getAttribute("OP_CONTEXT");

			isUnAuthenticatedRequest();

			FileDownloadEnquiryVO enqVO= RestCommonUtils.getFileDownloadEnquiry(opcontext, fileSeqNo);

			com.infosys.feba.utils.insulate.ArrayList<BusinessExceptionVO> bVOListException = new com.infosys.feba.utils.insulate.ArrayList<BusinessExceptionVO>();

			long size= 0;

			if(enqVO!=null && enqVO.getDetails()!=null && enqVO.getDetails().getDownloadedFile()!=null) {
				size = enqVO.getDetails().getDownloadedFile().length();

				final FileSequenceNumber fileSequenceNumber = new FileSequenceNumber();
				fileSequenceNumber.setValue(fileSeqNo);
				FDTTDetailsVO info = (FDTTDetailsVO)FEBAAVOFactory.createInstance(TypesCatalogueConstants.FDTTDetailsVO);;
				info.setFileSequenceNumber(fileSequenceNumber);
				
				opcontext.setOperatingMode(new FEBAOperatingMode());
	        	 IClientStub fetchService = ServiceUtil.getService(new FEBAUnboundString("FDTTDetailsService"));
	        
	        	 fetchService.callService(opcontext, info, new FEBAUnboundString("fetch"));
				String  saveDirectory = FEBAConstants.FEBA_SYSTEM_ROOT_PATH +FEBAConstants.SLASH+PropertyUtil.getProperty(FEBAConstants.DOWNLOADED_REPORT_FOLDER,null)+"\\";

				File file = new File(saveDirectory+info.getFileName());

			try(
				OutputStream out = new FileOutputStream(file);
					){
				
				out.write(enqVO.getDetails().getDownloadedFile().getValue());
				out.flush();
			}

				// Method that generates the response based on the file download PRPM
				return downloadFileResponse(
						opcontext,info.getFileName().getValue(),size,enqVO.getDetails().getDownloadedFile().getValue());
			}

		} catch (Throwable e) {
			RestResourceManager.handleFatalErrorOutput(request, response, e, restResponseErrorCodeMapping());
		}

		return responseBuilder.build();

	}
	public static Response downloadFileResponse(IOpContext opcontext,String reportName, long contentLengthSize, byte[] fileBytes){


			if(contentLengthSize > 0){
				return Response
						.ok(fileBytes,MediaType.APPLICATION_OCTET_STREAM)
						.header("Content-Disposition",
								"attachment; fileName=\"" + reportName + "\"")
						.header("Content-Length", contentLengthSize)
						.build();
			}
			else{
				return Response
						.ok(fileBytes,MediaType.APPLICATION_OCTET_STREAM)
						.header("Content-Disposition",
								"attachment; fileName=\"" + reportName + "\"")
						.build();
			}
	}

	
	private List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponseErrorCode = new ArrayList<RestResponseErrorCodeMapping>();
		restResponseErrorCode.add(new RestResponseErrorCodeMapping(FBAErrorCodes.UNATHORIZED_USER, "BE", 401));
		restResponseErrorCode.add(new RestResponseErrorCodeMapping(FBAErrorCodes.CRP_CUSER_KEY_INVALID_FIELD, "BE", 401));
		restResponseErrorCode.add(new RestResponseErrorCodeMapping(8504, "BE", 400));
		restResponseErrorCode.add(new RestResponseErrorCodeMapping(105939, "BE", 400));
		restResponseErrorCode.add(new RestResponseErrorCodeMapping(91008, "BE", 400));
		restResponseErrorCode.add(new RestResponseErrorCodeMapping(106774, "BE", 400));
		restResponseErrorCode.add(new RestResponseErrorCodeMapping(FBAErrorCodes.CRP_CUSER_KEY_INVALID_FIELD, "BE", 401));
		restResponseErrorCode.add(new RestResponseErrorCodeMapping(ErrorCodes.REQUEST_OBJECT_NULL, "CE", 400));
		restResponseErrorCode.add(new RestResponseErrorCodeMapping(FBAErrorCodes.INVALID_FILE_SEQN_NUM, "CE", 400));
		restResponseErrorCode.add(new RestResponseErrorCodeMapping(102206, "CE", 400));
		return restResponseErrorCode;
	}

}
