package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomVidaLivenessInputVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomVidaLivenessReferenceResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomVidaLivenessReferenceVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomVidaLivenessDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomVidaLivenessEnquiryVO;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.Header;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "user")
@Api(value = "Liveness")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("/v1/banks/{bankid}/users/{userid}/custom/face/liveness")
@ApiResponses(value = { @ApiResponse(code = 400, message = ResourcePaths.HTTP_ERROR_400, response = Header.class),
		@ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 403, message = ResourcePaths.HTTP_ERROR_403, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 422, message = ResourcePaths.HTTP_ERROR_422, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class)
})
public class CustomVidaLivenessResource extends ResourceAuthenticator {
	
	@Context
	HttpServletRequest request;
	@Context
	HttpServletResponse httpResponse;
	
	@POST
	@ApiOperation(value = "Vida Liveness", response = CustomVidaLivenessReferenceResponseVO.class)
	@MethodInfo(uri = CustomResourcePaths.VIDA_LIVENESS_URL)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Validate face succcessfully", response = CustomVidaLivenessReferenceResponseVO.class),
			@ApiResponse(code = 404, message = "Face validation failed"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	public Response hitCallback(CustomVidaLivenessInputVO livenessVO,	
			@ApiParam(value = "bankid", required = true) @PathParam("bankid") String bankid,
			@ApiParam(value = "userid", required = true) @PathParam("userid") String userid){
		
		IOpContext opcontext = (OpContext) request.getAttribute("OP_CONTEXT");
		CustomVidaLivenessReferenceResponseVO responseVO = new CustomVidaLivenessReferenceResponseVO();
		try {
			isUnAuthenticatedRequest();
			CustomVidaLivenessEnquiryVO enqVO = (CustomVidaLivenessEnquiryVO) FEBAAVOFactory
					.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomVidaLivenessEnquiryVO");
			
			enqVO.getCriteria().setUserId(userid);

			CustomVidaLivenessReferenceVO outputVO = faceValidate(enqVO, livenessVO, opcontext);
			responseVO.setData(outputVO);
			
		} catch (Exception e) {
			e.printStackTrace();
			RestResourceManager.handleFatalErrorOutput(request, response, e, restResponseErrorCodeMapping());
		}finally{
			if(opcontext != null){
				opcontext.cleanup();
			}
		}
		return Response.ok().entity(responseVO).build();
	}
	
	private CustomVidaLivenessReferenceVO faceValidate(CustomVidaLivenessEnquiryVO enqVO, CustomVidaLivenessInputVO livenessVO,
			IOpContext context) throws CriticalException, BusinessException, BusinessConfirmation {

		CustomVidaLivenessReferenceVO pojoVO = new CustomVidaLivenessReferenceVO();
		int score = 0;
		
		try {
			
			// Service IMPL to generate the access token for liveness start
			//final IClientStub serviceCStub1 = ServiceUtil.getService(new FEBAUnboundString("CustomVidaTokenService"));
			//serviceCStub1.callService(context, enqVO, new FEBAUnboundString("generate"));
			
			// Response object from token API
			//CustomVidaLivenessDetailsVO detailsVO = enqVO.getDetails();
			// Service IMPL to generate the access token for liveness end
	
			
			// Service IMPL to validate the base64 image for liveness start
			CustomVidaLivenessEnquiryVO enqVO1 = (CustomVidaLivenessEnquiryVO) FEBAAVOFactory
					.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomVidaLivenessEnquiryVO");
			
			if(RestCommonUtils.isNotNull(livenessVO.getBase64Image())){
				enqVO1.getCriteria().setImage(livenessVO.getBase64Image());
			}
			
			/*if(FEBATypesUtility.isNotNullOrBlank(detailsVO.getAccessToken())){
				enqVO1.getCriteria().setAccessToken(detailsVO.getAccessToken());
			}*/
			

			final IClientStub serviceCStub2 = ServiceUtil.getService(new FEBAUnboundString("CustomVidaFaceLivenessService"));
			serviceCStub2.callService(context, enqVO1, new FEBAUnboundString("validate"));
			
			// Response object from liveness API.
			CustomVidaLivenessDetailsVO detailsVO1 = enqVO1.getDetails();
			float respScoreVal=Float.parseFloat(detailsVO1.getScore().getValue());
			score = (int) (respScoreVal * 100);
			pojoVO.setMessage(detailsVO1.getMessage().getValue());
			pojoVO.setCode(detailsVO1.getCode().getValue());
			pojoVO.setScore(score);
			// Service IMPL to validate the base64 image for liveness end
			
			}catch (Exception e) {
					e.printStackTrace();
					LogManager.logError(null, e);
					throw e;
			} 
		return pojoVO;
	}
	
	private List restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<>();
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211153, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211154, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211155, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(103557, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211094, "BE", 400));

		return restResponceErrorCode;
	}

		

}
