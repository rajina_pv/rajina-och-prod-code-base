Rem    This file will create CUSTOM_DEVICE_DETAILS_TABLE
Rem    with the following characteristics .

drop table CUSTOM_DEVICE_DETAILS_TABLE
/

drop synonym DVDT
/

create table CUSTOM_DEVICE_DETAILS_TABLE
(
	DB_TS number(5,0),
	BANK_ID nvarchar2(11),
	USER_ID nvarchar2(32) NOT NULL,
	DEVICE_TOKEN nvarchar2(400),
	DEVICE_NAME nvarchar2(20),
	R_MOD_ID nvarchar2(65),
	R_MOD_TIME date,
	R_CRE_ID nvarchar2(65),
	R_CRE_TIME date
) 
TABLESPACE MASTER
/

create synonym DVDT for CUSTOM_DEVICE_DETAILS_TABLE
/

create unique index IDX_DVDT
on CUSTOM_DEVICE_DETAILS_TABLE( BANK_ID,USER_ID )
TABLESPACE IDX_MASTER
/
