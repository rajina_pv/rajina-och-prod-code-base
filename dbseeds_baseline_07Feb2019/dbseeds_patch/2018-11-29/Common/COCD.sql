update COCD set cd_desc='Berjalan' where code_type='LTP' and cm_code='A';
update COCD set cd_desc='Lunas' where code_type='LTP' and cm_code='P';
commit;

DELETE FROM COCD WHERE CODE_TYPE='STXT' AND BANK_ID='01';
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") 
values (1,'01','STXT','TNC','001','Terms and Conditions','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','STXT','MTP','001','Help Topics','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") 
values (1,'01','STXT','DTS','001','Privy Terms and Conditions','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") 
values (1,'01','STXT','TXM','001','Document Text','N','setup',sysdate,'setup',sysdate);
commit;
