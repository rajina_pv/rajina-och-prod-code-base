
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id",
"r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','DAYS_TO_DUE_DATE_INST_ONE','0',
'Days to due days for installment number one','N','setup',
sysdate,'setup',sysdate);

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id",
"r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','DAYS_TO_DUE_DATE_INST_TWO_OR_MORE','2',
'Days to due days for installment number two or more','N','setup',
sysdate,'setup',sysdate);