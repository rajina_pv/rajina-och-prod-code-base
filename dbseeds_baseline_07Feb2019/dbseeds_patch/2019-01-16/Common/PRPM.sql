DELETE FROM PRPM WHERE PROPERTY_NAME IN ('CUSTOM_NOTIFICATION_CORE_PROCESSEDPATH','CUSTOM_NOTIFICATION_CORE_INPUTPATH','CUSTOM_NOTIFICATION_CORE_OUTPUTPATH');
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") 
values (1,'01','BWY','CUSTOM_NOTIFICATION_CORE_PROCESSEDPATH','/opt/OCH/FebaBatch/output/ProcessedNotification/','Notification Core Batch Output file path','N','setup',sysdate,'setup',sysdate);
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','CUSTOM_NOTIFICATION_CORE_INPUTPATH','/share/pinang/notifications/','Notification Core Batch Input file path','N','setup',sysdate,'setup',sysdate);
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','CUSTOM_NOTIFICATION_CORE_OUTPUTPATH','/opt/OCH/FebaBatch/output/FailureNotification/','Notification Core Batch Output file path','N','setup',sysdate,'setup',sysdate);
