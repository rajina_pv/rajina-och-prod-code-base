package com.infosys.custom.ebanking.contentmanagement.util;

import com.infosys.custom.ebanking.tao.CACMConstants;
import com.infosys.custom.ebanking.tao.CMGTConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomContentMgmtDetailsVO;
import com.infosys.ebanking.applicationmaintenance.common.TxnTypeConstants;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.transaction.workflow.IPopulateWorkflowVOUtility;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;

/**
 * This class is used to populate the workflow VO
 * 
 */
public class CustomContentMgmtPopulateWfUtil {
	/**
	 *
	 * <b>Description</b>:This method is used to populate the workflow VO
	 * 
	 * @param objContext
	 * @param wfVOPopulator
	 * @param objInputOutput
	 * @param objTxnWM
	 * @param operation
	 * <br>	

	 */
	
	public static void populateWorkflowVO(FEBATransactionContext objContext,
			IPopulateWorkflowVOUtility wfVOPopulator,
			IFEBAValueObject objInputOutput, 
			char operation) {
		//Cast the Transaction context into EBanking transaction context
		EBTransactionContext ebcontext = (EBTransactionContext) objContext;
		CustomContentMgmtDetailsVO detailsVO = (CustomContentMgmtDetailsVO) objInputOutput;
		// Using the setter methods in the utility set the input parameters
		wfVOPopulator.setCorpId(ebcontext.getBankId().toString());
		wfVOPopulator.setEntityType("CACM");
		wfVOPopulator.setWfOperation(String.valueOf(operation));
		wfVOPopulator.setModuleID(EBankingConstants.BANK_USER_ADMIN);
		wfVOPopulator.setTransactionType("ACM");		
		wfVOPopulator.setKeyField(TxnTypeConstants.BANK_ID, ebcontext
				.getBankId().toString());
		wfVOPopulator.setKeyField(CACMConstants.PHOTO_KEY,detailsVO.getObjectId().toString().toUpperCase());
		wfVOPopulator.setKeyField(CMGTConstants.OBJECT_KEY,detailsVO.getObjectId().toString().toUpperCase());		
		}

	/**
	 *
	 * <b>Description</b>:This method is used to populate the workflow VO
	 * 
	 * @param objContext
	 * @param wfVOPopulator
	 * @param objInputOutput
	 * @param objTxnWM
	 * @param operation
	 * @param functionCode
	 * <br>	
	 */
	public void populateWorkflowVO(FEBATransactionContext objContext,
			IPopulateWorkflowVOUtility wfVOPopulator,
			IFEBAValueObject objInputOutput,
			char operation, char functionCode) {
		//calling the overloaded method
		populateWorkflowVO(objContext, wfVOPopulator, objInputOutput,
				operation);
		wfVOPopulator.setFuncCode(String.valueOf(functionCode));
	}
}
