/*
 * COPYRIGHT NOTICE:
 * Copyright (c) 2007 Infosys Technologies Limited, Electronic City,
 * Hosur Road, Bangalore - 560 100, India.
 * All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Infosys Technologies Ltd. ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered
 * into with Infosys.
 */
package com.infosys.custom.ebanking.contentmanagement.service;

import com.infosys.custom.ebanking.tao.CACMTAO;
import com.infosys.custom.ebanking.tao.CMGTTAO;
import com.infosys.custom.ebanking.tao.info.CACMInfo;
import com.infosys.custom.ebanking.tao.info.CMGTInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomContentMgmtDetailsVO;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalInquiryTran;
import com.infosys.feba.framework.types.primitives.CorporateId;
import com.infosys.feba.framework.types.primitives.ModuleName;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.types.primitives.EntityKey;
import com.infosys.fentbase.types.primitives.FreeText2;

/**
 * This class will fetch the user details to be edited
 * 
 * @author
 * @version 1.0
 * @since FEBA 2.0
 */

public class CustomContentMgmtServiceFetchImpl extends
		AbstractLocalInquiryTran {

	@Override
	public FEBAValItem[] prepareValidationsList(
			FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException,
			BusinessConfirmation, CriticalException {		
		return new FEBAValItem[] {};
	}

	/**
	 * This method is used to fetch the details of the User for modification.
	 * 
	 * @param objContext
	 * @param objInputOutput
	 * @param objTransactionWM
	 * @throws BusinessException
	 * @throws BusinessConfirmation
	 * @throws CriticalException
	 * @author
	 * @see com.infosys.feba.framework.transaction.pattern.AbstractLocalInquiryTran#process(com.infosys.feba.framework.commontran.context.FEBATransactionContext,
	 *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject,
	 *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject)
	 * @since
	 */
	@Override
	protected void process(FEBATransactionContext pObjContext,
			IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
			throws BusinessException, BusinessConfirmation, CriticalException {
		CustomContentMgmtDetailsVO detVO = (CustomContentMgmtDetailsVO) objInputOutput;
		
		try{
		CACMInfo acomInfo= CACMTAO.select(pObjContext, detVO.getObjectId());
		
		CMGTInfo cmgtInfo =CMGTTAO.select(pObjContext, new CorporateId("01"), new UserId("ACOM"), new FreeText2(detVO.getObjectId().toString()), 
				new EntityKey("IUSR"), new ModuleName("ACOM"), pObjContext.getBankId());

		updateVOWithValues(detVO, acomInfo,cmgtInfo);
		}
		catch(Exception e)
		{
		//do nothing
		}
	}

	
	private void updateVOWithValues(
			CustomContentMgmtDetailsVO detVO, CACMInfo acomInfo,CMGTInfo cmgtInfo) {
		    detVO.setStartDate(acomInfo.getStartDate());
		    detVO.setEndDate(acomInfo.getEndDate());
		    detVO.setContType(acomInfo.getContentType());
		    detVO.setHeader(acomInfo.getHeader());		   	      
	        detVO.setDescription(acomInfo.getDescription());
	        detVO.setTextType(acomInfo.getTextType());	      
	        detVO.setMerchantId(acomInfo.getMerchantId());
	        detVO.setAndAppURL(acomInfo.getAndAppUrl());
	        detVO.setIosAppURL(acomInfo.getIosAppUrl());
	        detVO.setIosStoreURL(acomInfo.getIosStoreUrl());
	        detVO.setPlayStoreURL(acomInfo.getPlayStoreUrl());
	        detVO.setPriority(acomInfo.getPriority());
	        detVO.setObjectId(acomInfo.getPhotoKey());
	        detVO.setFileName(acomInfo.getFilename());
	        
	        detVO.setProfilePhoto(cmgtInfo.getFileData());
	        detVO.setContentType(cmgtInfo.getFileMime());
	        detVO.setFileSize(cmgtInfo.getDataLength());	        
	}		

}
