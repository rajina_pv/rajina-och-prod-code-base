
package com.infosys.custom.ebanking.contentmanagement.service;
import com.infosys.custom.ebanking.contentmanagement.util.CustomContentMgmtPopulateWfUtil;
import com.infosys.custom.ebanking.contentmanagement.util.CustomContentMgmtUtil;
import com.infosys.custom.ebanking.contentmanagement.validators.CustomContentTypeFieldsValidator;
import com.infosys.custom.ebanking.contentmanagement.validators.CustomContentUploadDateValidator;
import com.infosys.custom.ebanking.types.valueobjects.CustomContentMgmtDetailsVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.tmo.FEBATMOConstants;
import com.infosys.feba.framework.tmo.FEBATMOException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.transaction.workflow.IPopulateWorkflowVOUtility;
import com.infosys.feba.framework.types.FEBATypesConstants;
import com.infosys.feba.framework.types.valueobjects.FEBATMOCriteria;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValEngineConstants;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.common.util.MaintenanceReqdUtility;
import com.infosys.fentbase.framework.common.CAFrameworkErrorMappingHelper;
import com.infosys.fentbase.framework.common.OperationModeConstants;


public class CustomContentMgmtServiceRejectImpl extends AbstractLocalUpdateTran {
       

    @Override
    public void process(FEBATransactionContext pObjContext,
            IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
            throws BusinessException, BusinessConfirmation, CriticalException {
    	
    	EBTransactionContext ebContext = (EBTransactionContext) pObjContext;		
        FBATransactionContext objContext = (FBATransactionContext) pObjContext;
        CustomContentMgmtDetailsVO detVO = (CustomContentMgmtDetailsVO) objInputOutput;
        FEBATMOCriteria criteria = new FEBATMOCriteria();
        criteria.setMaintenanceRequired(MaintenanceReqdUtility
                .getMaintenanceReqd(pObjContext));
        criteria.setDependencyFlag(FBAConstants.NO);
     
        try {          	        				
            CustomContentMgmtUtil.processCACOM(pObjContext,
                    detVO,
                    FEBATMOConstants.TMO_OPERATION_REJECT);
            criteria.setDependencyFlag(FBAConstants.YES);                                   	         		               
           
            CustomContentMgmtUtil.processCMGT(pObjContext,
                    detVO,
                    FEBATMOConstants.TMO_OPERATION_REJECT);
            criteria.setDependencyFlag(FBAConstants.YES);                                   	         		               
        	
            
        } catch (FEBATMOException tmoExp) {
            int errorCode = CAFrameworkErrorMappingHelper
                    .getMappedErrorCode(OperationModeConstants.CA_REJECT
                            + tmoExp.getErrorCode());
            throw new BusinessException(objContext,
                    FBAIncidenceCodes.USER_REJECT_TMO_EXCEPTION, errorCode,
                    tmoExp);
        } catch (FEBATableOperatorException tblOperExp) {
            throw new CriticalException(objContext,
                    FBAIncidenceCodes.TAO_EXP_USER_REJECT, tblOperExp
                            .getMessage(), tblOperExp.getErrorCode());
        }
        objContext.addBusinessInfo(CustomContentMgmtUtil.getSuccessMessage(ebContext,
				EBankingErrorCodes.REQUEST_SUCESSFULLY_REJECTED));
    }
    /**
     * Method populateWorkflowVO() Description Populates workflow value object
     * from context and InputOutputVO Input objContext, workflowVO,
     * objInputOutput, objTxnWM
     *
     * @param objContext
     * @param populateWorkflowVOUtility
     * @param objInputOutput
     * @param objTxnWM
     */
    @Override
	protected void populateWorkflowVO(FEBATransactionContext objContext,
			IPopulateWorkflowVOUtility wfVOPopulator,
			IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM) {

    	CustomContentMgmtPopulateWfUtil.populateWorkflowVO(objContext, wfVOPopulator, objInputOutput,
				 EBankingConstants.REJECT);
	}
                     
  
    
    @Override
    public FEBAValItem[] prepareValidationsList(
            FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
            IFEBAValueObject objTxnWM) throws BusinessException,
            BusinessConfirmation, CriticalException {

    	CustomContentMgmtDetailsVO photoVO = (CustomContentMgmtDetailsVO) objInputOutput;
		return new FEBAValItem[] {
				new FEBAValItem(FEBATypesConstants.NO_TAG, photoVO,
						new CustomContentTypeFieldsValidator(),
						FEBAValEngineConstants.INDEPENDENT),
				  
				new FEBAValItem(FEBATypesConstants.NO_TAG, photoVO,
						new CustomContentUploadDateValidator(),
						FEBAValEngineConstants.INDEPENDENT),
				};
		
    }
}
