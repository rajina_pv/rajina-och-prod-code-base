/**
 * COPYRIGHT NOTICE:
 * Copyright (c) 2007 Infosys Technologies Limited.
 * All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Infosys Technologies Ltd. ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered
 * into with Infosys.
 */

package com.infosys.custom.ebanking.contentmanagement.validators;



import com.infosys.custom.ebanking.types.valueobjects.CustomContentMgmtDetailsVO;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.types.IFEBAType;
import com.infosys.feba.framework.valengine.IFEBAExtendedValidator;
import com.infosys.feba.framework.valengine.IFEBAStandardValidator;
import com.infosys.fentbase.common.FBATransactionContext;


public class CustomContentTypeFieldsValidator implements IFEBAStandardValidator {

	@Override
	public void validate(FEBATransactionContext arg0, IFEBAType arg1)
			throws BusinessException, CriticalException {
		
		CustomContentMgmtDetailsVO vo = (CustomContentMgmtDetailsVO) arg1;
		FBATransactionContext ctx = (FBATransactionContext) arg0;
		
		if(vo.getContType().getValue()=="null" || vo.getContType().getValue().isEmpty() )
		{
			throw new BusinessException(ctx,"CMGT003",
					"Content Type Blank",
					105895);
			
		}
	}

	
	

		
}

