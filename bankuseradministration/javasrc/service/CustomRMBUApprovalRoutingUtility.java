package com.infosys.custom.ebanking.bankuseradministration.service;

import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.fentbase.bankuseradministration.service.RMBUApprovalRoutingUtility;

/**
 * This class is used to make Custom entries for workflow
 *
 * @author Vidhi_Kapoor01
 * @version 1.0
 * @since FEBA 2.0
 */
public class CustomRMBUApprovalRoutingUtility extends RMBUApprovalRoutingUtility  {

	@Override
	//@Deprecated		
	public FEBAArrayList<FEBAUnboundString> loadAllTransaction() {

		FEBAArrayList<FEBAUnboundString> fgDetailsList =super.loadAllTransaction();
		//The order of the FG details should be in the following order:
		//TransactionType|FormsGroup|ModuleId|FlowType|FMID
		//If the flow type or FMID is null, then make sure to give a space in the string.
		
		FEBAArrayList txntypeArraylist = new FEBAArrayList();
		txntypeArraylist.add(new FEBAUnboundString("ACM|CustomContentMgmtCRUDFG|contentmanagement| | "));
		txntypeArraylist.add(new FEBAUnboundString("LPM|CustomLoanProdMaintenanceFG|applicationmaintenance| | "));
		txntypeArraylist.add(new FEBAUnboundString("LID|CustomLoanInterestDetailsFG|applicationmaintenance| | "));
		for(int i=0; i< txntypeArraylist.size(); i++)
		{
			FEBAUnboundString customtxntype = (FEBAUnboundString)txntypeArraylist.get(i);
			String[] customFg =customtxntype.toString().split("\\|");
			String customTxnType = customFg[0];				
			for(int j=0; j<fgDetailsList.size();j++)
			{
				FEBAUnboundString txntypedetails =fgDetailsList.get(j);
				String[] details = txntypedetails.toString().split("\\|");
				String txnType = details[0];
				if(txnType.equalsIgnoreCase(customTxnType))
				{
					fgDetailsList.remove(j);
					fgDetailsList.add(customtxntype);
					break;
				}
			}
		}
		
		fgDetailsList.add(new FEBAUnboundString("ACM|CustomContentMgmtCRUDFG|contentmanagement| | "));		
		fgDetailsList.add(new FEBAUnboundString("LPM|CustomLoanProdMaintenanceFG|applicationmaintenance| | "));	
		fgDetailsList.add(new FEBAUnboundString("LID|CustomLoanInterestDetailsFG|applicationmaintenance| | "));	
		return fgDetailsList;

	}
}
