<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="feba2" uri="/feba_taglib.tld" %>
<%@ page import="com.infosys.feba.framework.ui.util.FebaTagUtil"%>
<%@ page import="com.infosys.feba.framework.common.util.resource.PropertyUtil"%>
<%@include file="/jsp/topbar.jsp"%>
<feba2:Set value="True" var="DispForm.SubSection2.visible"></feba2:Set>
<feba2:Set value="True" var="DispForm.SubSection4.visible"></feba2:Set>
<feba2:SetAlternateView alternateView="${feba2:select('isConfOrIsApprovals#BankUserPreviewConf@@isRet#RetailUserMaintenance@@isCorp#CorporateUserMaintenance@@isBnkUsr#BankUserMaintenance',pageContext)}"></feba2:SetAlternateView>
<feba2:title pageTitle="true" text = "History"/>
<feba2:Page action="Finacle" name="CustomLoanOriginationFG" forcontrolIDs="Download Details As=CustomLoanOriginationFG.OUTFORMAT@@">
<% if(!isGroupletView){%>

	<%@include file="/jsp/header.jsp"%>
<%if (pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
<%@include file="/jsp/sidebar.jsp"%>
	<%@include file="/jsp/parentTable.jsp"%>
<%} else {%>
	<%@include file="/jsp/parentTable.jsp"%>
	<%@include file="/jsp/sidebar.jsp"%>
<%}%>
<%}%>
  <feba2:Section id="BrdCrumbNImg" identifier="BreadCrumb" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:moduleTopBar id="moduleTopBar16082044" printScreen="btn-printscreen.gif" breadcrumb="true" helpImage="btn-help.gif" printImage="btn-print.gif" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="PgHeading" identifier="PageHeader" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1">
            <feba2:Col identifier="C1" style="width10">
              <feba2:field alt="arrow" id="Image9349517" name="Image9349517" src="arrow-pageheading.gif" isMenu="false" tagHelper="ImageTagHelper" visible="true" altLC="LC1788" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="pageheadingcaps">
              <feba2:title id="title26742489" text="History Details" headinglevel="h1" upperCase="false" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:ErrorDisplay id="ErrorDisplay21624724" style="" /> 
  <feba2:Set value="true" var="ListTableWithCtrls.visible" />
  <feba2:Section id="ListTableWithCtrls" identifier="ListingTable_With_Navigation_Controls_black_border" style="section_fourlinbrd">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="false" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="false" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="false" style="width100percent">
          <feba2:row identifier="Ra1" style="listingrow">
            <feba2:Col identifier="C1" style="">
              <feba2:Table caption="History Details" captionstyle="" collapsableFlag="" collapsableId="" collapsible="" footerStyle="" headerStyle="listtopbigbg" headinglevel="h3" hideEmptyTable="true" id="CustomLoanOriginationHistoryListing" lastupdatedate="" listDisplay="Displaying$range$of$total$results$" listing="CustomLoanOriginationHistoryListing" pagination="true" paginationButtonCaption="Go" paginationFooter="Page$current$of$total" paginationFooterText="Go to Page" rowOneStyle="listgreyrow" rowTwoStyle="listwhiterow" selectAllCheck="${allChk}" summary="" tableDisplayStyle="" tableStyle="width100percent" visible="true" width="100" captionLC="LC20005" listDisplay1LC="LC22" listDisplay2LC="LC17" listDisplay3LC="LC23" paginationButtonCaptionLC="LC24" paginationFooter1LC="LC25" paginationFooter2LC="LC17" paginationFooterTextLC="LC26">
               <feba2:Header>
               	 <feba2:Heading id="column7145547" sort="true" sortOrder="ASC" style="slno" visible="true" text="Select" textLC="LC38" />
                 <feba2:Heading id="column13549765" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Application Id" textLC="LC14250" />
                 <feba2:Heading id="column13549765" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Creation Time" textLC="LC14250" />
             	 <feba2:Heading id="column28061596" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Remarks" textLC="LC20001" />
                 <feba2:Heading id="column6744085" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Application Status" textLC="LC20002" />
               </feba2:Header>
                 
                <feba2:field id="CustomLoanOriginationFG.SELECTED_INDEX" name="CustomLoanOriginationFG.SELECTED_INDEX" tableColStyle="listgreyrowtxtwithoutline" title="Select Loan Parameter" visible="true" style="absmiddle" tagHelper="TableRadioTagHelper" usage="I" value="true" titleLC="LC319" />
              	<feba2:field id="CustomLoanOriginationFG.APPLICATION_ID_ARRAY" name="CustomLoanOriginationFG.APPLICATION_ID_ARRAY"   tableColStyle="listgreyrowtxtleftline"  disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" />               
                <feba2:field fldFormatter="DateTimeFormatter" id="CustomLoanOriginationFG.HIS_CREATION_TIME_ARRAY" name="CustomLoanOriginationFG.HIS_CREATION_TIME_ARRAY" tableColStyle="listgreyrowtxtleftline" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true"  />
           	 	<feba2:field fldFormatter="DateFormatter" id="CustomLoanOriginationFG.HIS_REMARKS_ARRAY" name="CustomLoanOriginationFG.HIS_REMARKS_ARRAY" tableColStyle="listgreyrowtxtleftline" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true"  />
            	<feba2:field id="CustomLoanOriginationFG.APPLICATION_STATUS_ARRAY" key="CODE_TYPE=APS" name="CustomLoanOriginationFG.APPLICATION_STATUS_ARRAY" tableColStyle="listgreyrowtxtleftline" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper"  />
	             <feba2:Footer pageNo="REQUESTED_PAGE_NUMBER" isPinnable="False" goName="CustomLoanOriginationHistoryListing.GOTO_PAGE__" prevPage="CustomLoanOriginationHistoryListing.GOTO_PREV__" nextPage="CustomLoanOriginationHistoryListing.GOTO_NEXT__" />
              </feba2:Table>
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
        </feba2:SubSection>
        </feba2:SubSectionSet>
        </feba2:Section>
 
  <feba2:Section id="NavPanel" displaymode="N" identifier="NavigationPanel" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="right">
          <feba2:row identifier="Ra1" style="">          
            <feba2:Col identifier="C1" style="">
              <feba2:field caption="Back" id="BACK" name="BACK" style="formbtn_last" visible="true" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC4" />
            </feba2:Col>          
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
 <feba2:hidden name="FORMSGROUP_ID__" id="FORMSGROUP_ID__" value="CustomLoanOriginationFG" />
 <feba2:hidden name="CustomLoanOriginationFG.REPORTTITLE" id="CustomLoanOriginationFG.REPORTTITLE" value="CustomLoanOriginationHistory" />
<%if(!isGroupletView){%>
	<%@include file="/jsp/footer.jsp"%>
<%}%>
</feba2:Page>

