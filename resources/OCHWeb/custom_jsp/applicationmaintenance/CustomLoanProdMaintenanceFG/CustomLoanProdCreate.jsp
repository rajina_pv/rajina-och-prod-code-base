<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="feba2" uri="/feba_taglib.tld" %>
<%@ page import="com.infosys.feba.framework.ui.util.FebaTagUtil"%>
<%@include file="/jsp/topbar.jsp"%>

<feba2:Set value="${feba2:getValue('CustomLoanProdMaintenanceFG.VIEW_MODE','formfield', pageContext)}" var="opModeIndicator"></feba2:Set>
*** ${opModeIndicator}
<feba2:Set value="${feba2:equals(opModeIndicator,'NEW')}" var="opMode"></feba2:Set>
<feba2:Set value="${feba2:select('opMode#Add New||LC459@@!opMode#Update||LC460',pageContext)}" var="displayMode"></feba2:Set>
<feba2:Set value="${feba2:select('opMode#I@@!opMode#O',pageContext)}" var="Usage"></feba2:Set>
<feba2:Set value="${feba2:select('opMode#true@@!opMode#false',pageContext)}" var="req"></feba2:Set>
<feba2:SetAlternateView alternateView=""></feba2:SetAlternateView>
<feba2:title pageTitle="true" text = "${displayMode}"/>
<feba2:Page action="Finacle" name="CustomLoanProdMaintenanceFG" forcontrolIDs="Product Code:=CustomLoanProdMaintenanceFG.PRODUCT_COODE@@Product Category:=CustomLoanProdMaintenanceFG.PROD_CATEGORY@@Product SubCategory:=CustomLoanProdMaintenanceFG.PROD_SUBCATEGORY@@Configuration Type:=CustomLoanProdMaintenanceFG.CONFIG_TYPE@@Min Amount:=CustomLoanProdMaintenanceFG.MIN_AMOUNT@@Max Amount:=CustomLoanProdMaintenanceFG.MAX_AMOUNT@@">
<% if(!isGroupletView){%>

	<%@include file="/jsp/header.jsp"%>
<%if (pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
<%@include file="/jsp/sidebar.jsp"%>
	<%@include file="/jsp/parentTable.jsp"%>
<%} else {%>
	<%@include file="/jsp/parentTable.jsp"%>
	<%@include file="/jsp/sidebar.jsp"%>
<%}%>
<%}%>
  <feba2:Section id="BrdCrumbNImg" identifier="BreadCrumb" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:moduleTopBar id="mtb" breadcrumb="true" helpImage="btn-help.gif" printImage="btn-print.gif" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="PgHeading" identifier="PageHeader" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1">
            <feba2:Col identifier="C1" style="width10">
              <feba2:field alt="Arrow Image" id="ArrowImage" name="arrow" src="arrow-pageheading.gif" isMenu="false" tagHelper="ImageTagHelper" visible="true" altLC="LC27" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="pageheadingcaps">
              <feba2:title id="PageHeader" text="${displayMode}" headinglevel="h1" upperCase="false" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:ErrorDisplay id="ErrorDisplay22424518" style="alerttable" />
  <feba2:Section id="InputForm" displaymode="N" identifier="InputForm" style="section_blackborder">
    <feba2:SectHeader identifier="Header">
      <feba2:caption id="header" style="whtboldtxt" text="${displayMode}" isDownloadLink="false" isMenu="false" visible="true" />
    </feba2:SectHeader>
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="width100percent">
        <feba2:row identifier="Ra7" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomLoanProdMaintenanceFG.MERCHANT_ID" id="merchantId" name="merchantId" required="true" text="Merchant ID:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC44" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field combomode="complex" id="CustomLoanProdMaintenanceFG.MERCHANT_ID" key="CODE_TYPE=MID" name="CustomLoanProdMaintenanceFG.MERCHANT_ID" select="true" title="Merchant ID" visible="true" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC309" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomLoanProdMaintenanceFG.PRODUCT_CODE" id="productCode" name="productCode" required="true" text="Product Code:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC44" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field combomode="complex" id="CustomLoanProdMaintenanceFG.PRODUCT_CODE" key="CODE_TYPE=PCO" name="CustomLoanProdMaintenanceFG.PRODUCT_CODE" select="true" title="Product Code" visible="true" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC309" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomLoanProdMaintenanceFG.PROD_CATEGORY" id="productCategory" name="productCategory" required="true" text="Product Category:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC461" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field combomode="complex" id="CustomLoanProdMaintenanceFG.PROD_CATEGORY" key="CODE_TYPE=PCA" name="CustomLoanProdMaintenanceFG.PROD_CATEGORY" select="true" title="Product Code" visible="true" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC309" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomLoanProdMaintenanceFG.PROD_SUBCATEGORY" id="prodSubcategory" name="prodSubCategory" required="true" text="Product SubCategory:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC462" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanProdMaintenanceFG.PROD_SUBCATEGORY"  name="CustomLoanProdMaintenanceFG.PROD_SUBCATEGORY" style="querytextboxmedium" title="Product SubCategory" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC463" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomLoanProdMaintenanceFG.CONFIG_TYPE" id="configType" name="configType" required="true" text="Configuration Type:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC464" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field combomode="complex" id="CustomLoanProdMaintenanceFG.CONFIG_TYPE" key="CODE_TYPE=CFT" name="CustomLoanProdMaintenanceFG.CONFIG_TYPE" select="true" title="Configuration Type" visible="true" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC309" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra5" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomLoanProdMaintenanceFG.MIN_AMOUNT" id="minAmt" name="minAmt" required="true" text="Minimum Amount:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC466" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanProdMaintenanceFG.MIN_AMOUNT" name="CustomLoanProdMaintenanceFG.MIN_AMOUNT" style="querytextboxmedium" title="Minimum Amount" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC467" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra6" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomLoanProdMaintenanceFG.MAX_AMOUNT" id="maxAmt" name="maxAmt" required="true" text="Maximum Amount:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC468" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanProdMaintenanceFG.MAX_AMOUNT"  name="CustomLoanProdMaintenanceFG.MAX_AMOUNT" style="querytextboxmedium" title="Maximum Amount" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC469" />
            </feba2:Col>
            </feba2:row>
            <feba2:row identifier="Ra8" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomLoanProdMaintenanceFG.LN_PURPOSE" id="lnPurpose" name="lnPurpose" required="true" text="Loan Purpose:" displayColonAfterDateFormat="true" style="simpletext" visible="false" textLC="LC462" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanProdMaintenanceFG.LN_PURPOSE"  name="CustomLoanProdMaintenanceFG.LN_PURPOSE" style="querytextboxmedium" title="Loan Purpose" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="false" titleLC="LC463" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra8" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomLoanProdMaintenanceFG.CURRENCY" id="currency" name="currency" required="false" text="Currency:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC462" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanProdMaintenanceFG.CURRENCY"  name="CustomLoanProdMaintenanceFG.CURRENCY" style="querytextboxmedium" title="Currency" disable="false" readonly="false" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC463" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="NavPanel" displaymode="N" identifier="NavigationPanel" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="right">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:field caption="Continue" id="CONTINUE" name="CONTINUE" title="Continue" disable="false" isDownloadAction="false" style="formbtn" tagHelper="ButtonTagHelper" visible="true" captionLC="LC62" titleLC="LC62" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="">
              <feba2:field caption="Back" id="BACK" name="BACK_TO_LIST" title="Back" disable="false" isDownloadAction="false" style="formbtn" tagHelper="ButtonTagHelper" visible="true" captionLC="LC4" titleLC="LC4" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:hidden id="CustomLoanProdMaintenanceFG.VIEW_MODE" name="CustomLoanProdMaintenanceFG.VIEW_MODE" value="${opModeIndicator}" />
 <feba2:hidden name="FORMSGROUP_ID__" id="FORMSGROUP_ID__" value="CustomLoanProdMaintenanceFG" />
 <feba2:hidden name="CustomLoanProdMaintenanceFG.REPORTTITLE" id="CustomLoanProdMaintenanceFG.REPORTTITLE" value="CustomLoanProdCreate" />
<%if(!isGroupletView){%>
	<%@include file="/jsp/footer.jsp"%>
<%}%>
</feba2:Page>

