<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="feba2" uri="/feba_taglib.tld" %>
<%@ page import="com.infosys.feba.framework.ui.util.FebaTagUtil"%>
<%@include file="/jsp/topbar.jsp"%>

<feba2:Set value="${feba2:getValue('CustomContentMgmtFG.RESULT_LIST_SIZE','formfield', pageContext)}" var="listSize"></feba2:Set>
<feba2:Set value="${feba2:getValue('CustomContentMgmtFG.VIEW_MODE','formfield', pageContext)}" var="viewMode"></feba2:Set>
<feba2:Set value="${feba2:equalsIgnoreCase(viewMode,'DELETE')}" var="isDelete"></feba2:Set>


<feba2:Set value="${feba2:equals(listSize,'0')}" var="isVisible"></feba2:Set>

<feba2:title pageTitle="true" text = "Content Management" textLC = "LC1785" />
<feba2:Page action="Finacle" name="CustomContentMgmtFG" forcontrolIDs="">
<% if(!isGroupletView){%>

	<%@include file="/jsp/header.jsp"%>
<%if (pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
<%@include file="/jsp/sidebar.jsp"%>
	<%@include file="/jsp/parentTable.jsp"%>
<%} else {%>
	<%@include file="/jsp/parentTable.jsp"%>
	<%@include file="/jsp/sidebar.jsp"%>
<%}%>
<%}%>
  <feba2:Section id="BrdCrumbNImg" identifier="BreadCrumb" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:moduleTopBar id="mtb" printScreen="btn-printscreen.gif" breadcrumb="true" helpImage="btn-help.gif" printImage="btn-print.gif" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="PgHeading" identifier="PageHeader" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1">
            <feba2:Col identifier="C1" style="width10">
              <feba2:field alt="Arrow Image" id="ArrowImage" name="arrow" src="arrow-pageheading.gif" isMenu="false" tagHelper="ImageTagHelper" visible="true" altLC="LC27" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="pageheadingcaps">
              <feba2:title id="PgHeading" text="Content Management" headinglevel="h1" upperCase="false" visible="true" textLC="LC1785" />
            </feba2:Col>
             <feba2:Col displaymode="N" identifier="C3" style="right">
              <feba2:field caption="Create New" id="CREATE_NEW" method="FG.New" name="CREATE_NEW" style="formbtn_top" visible="true" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC73" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:ErrorDisplay style="alerttable" />
  <feba2:Section id="SearchPanel" collapsibleDisplay="allCollapse" displaymode="N" identifier="SearchPanel" style="section_blackborder">
    <feba2:SectHeader identifier="Header">
      <feba2:caption id="TableHeader" style="whtboldtxt" text="Search Criteria" isDownloadLink="false" isMenu="false" visible="true" textLC="LC74" />
    </feba2:SectHeader>
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="width100percent">
          <feba2:row identifier="Ra1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol displayDateFormat="true" forControlId="CustomContentMgmtFG.START_DATE" id="fromDate" name="FromDt" text="Start Date:" visible="true" displayColonAfterDateFormat="true" style="simpletext" textLC="LC2275" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:compositefield id="toDateComponent" align="horizontal" name="ToDateComponent" visible="true">
                <feba2:field disable="false" fldFormatter="DateFormatter" id="CustomContentMgmtFG.START_DATE" name="CustomContentMgmtFG.START_DATE" title="Date From" readonly="false" style="datetextbox" tagHelper="DateTagHelper" visible="true" titleLC="LC1617" maxlength="" />
                <feba2:labelforcontrol displayDateFormat="true" forControlId="CustomContentMgmtFG.END_DATE" id="toCaptionName" name="toCaptionName" style="simpletextrightDate" text="End Date:" displayColonAfterDateFormat="true" visible="true" textLC="LC2277" />
                <feba2:blankfield id="blankfield2" count="1" visible="true" />
                <feba2:field disable="false" fldFormatter="DateFormatter" id="CustomContentMgmtFG.END_DATE" name="CustomContentMgmtFG.END_DATE" title="Date To" readonly="false" style="datetextbox" tagHelper="DateTagHelper" visible="true" titleLC="LC1618" maxlength="" />
              </feba2:compositefield>
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomContentMgmtFG.CONT_TYPE" id="ContType" name="ContType" text="Content Type:" visible="true" displayColonAfterDateFormat="true" style="simpletext" textLC="LC12610" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field empty="false" id="CustomContentMgmtFG.CONT_TYPE" key="CODE_TYPE=CTYP" name="CustomContentMgmtFG.CONT_TYPE" select="true" visible="true" all="false" disable="false" displayMode="N" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" maxlength="16" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="NavPanel" displaymode="N" identifier="NavigationPanel" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="right">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:field caption="Search" id="RETRIEVE_LIST" name="RETRIEVE_LIST" style="formbtn_top" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" visible="true" captionLC="LC75" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="">
              <feba2:field caption="Clear" id="RESET" name="RESET" style="formbtn_last" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" visible="true" captionLC="LC76" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Set value="${!isVisible}" var="ListTableWithCtrls.visible" />
  <feba2:Section id="ListTableWithCtrls" identifier="ListingTable_With_Navigation_Controls_black_border" style="section_fourlinbrd">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="false" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="false" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="false" style="width100percent">
          <feba2:row identifier="Ra1" style="listingrow">
            <feba2:Col identifier="C1" style="">
              <feba2:Table caption="Content List" captionstyle="" collapsableFlag="" collapsableId="" collapsible="" footerStyle="" headerStyle="gradientbgonelinewithouttxt" headinglevel="" hideEmptyTable="true" id="contentListing" lastupdatedate="" listDisplay="Displaying$range$of$total$results$" listing="contentListing" pagination="true" paginationButtonCaption="Go" paginationFooter="Page$current$of$total" paginationFooterText="Go to Page" rowOneStyle="listgreyrow" rowTwoStyle="listwhiterow" selectAllCheck="false" summary="" tableDisplayStyle="" tableStyle="width100percent" visible="true" width="100" captionLC="LC1787" listDisplay1LC="LC22" listDisplay2LC="LC17" listDisplay3LC="LC23" paginationButtonCaptionLC="LC24" paginationFooter1LC="LC25" paginationFooter2LC="LC17" paginationFooterTextLC="LC26">
                <feba2:Header>
                  <feba2:Heading id="column8548382" sortOrder="ASC" style="slno" visible="true" text="Select" textLC="LC38" />
                  <feba2:Heading id="column26872956" sort="true" sortOrder="ASC" width="12" style="listboldtxtwithline" text="Start Date" textLC="LC784" />
                  <feba2:Heading id="column31454114" sort="true" sortOrder="ASC" width="12" style="listboldtxtwithline" text="End Date" textLC="LC1786" />
                   <feba2:Heading id="column26872956" sort="true" sortOrder="ASC" width="16" style="listboldtxtwithline" text="Content Type" textLC="LC784" />
                  <feba2:Heading id="column31454114" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Header" textLC="LC1786" />
                 <feba2:Heading id="column31454114" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Description" textLC="LC1786" />                
                </feba2:Header>
                <feba2:field id="CustomContentMgmtFG.SELECTED_INDEX" name="CustomContentMgmtFG.SELECTED_INDEX" tableColStyle="listgreyrowtxtwithoutline" title="Select Corporate" style="absmiddle" tagHelper="TableRadioTagHelper" usage="I" value="true" visible="true" titleLC="LC1484" />
                <feba2:field disable="true" fldFormatter="DateFormatter" id="CustomContentMgmtFG.START_DATE_ARRAY" name="CustomContentMgmtFG.START_DATE_ARRAY" tableColStyle="listgreyrowtxtleftline" title="Requested Date" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC4415" maxlength="" />
                <feba2:field disable="true" fldFormatter="DateFormatter" id="CustomContentMgmtFG.END_DATE_ARRAY" name="CustomContentMgmtFG.END_DATE_ARRAY" tableColStyle="listgreyrowtxtleftline" title="Requested Date" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC4415" maxlength="" />                
                 <feba2:field disable="true" id="CustomContentMgmtFG.CONT_TYPE_ARRAY" key="CODE_TYPE=CTYP" name="CustomContentMgmtFG.CONT_TYPE_ARRAY" link="FORMSGROUP_ID__= CustomContentMgmtFG#__EVENT_ID__= FETCH_DETAILS#CustomContentMgmtFG.SELECTED_INDEX={TABLEROW_SERIAL_NO,page}" linkstyle="bluelinkLb" tableColStyle="listgreyrowtxtleftline" title="View Details" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC12615" visible="true" maxlength="256" />               
                 <feba2:field disable="true" id="CustomContentMgmtFG.HEADER_ARRAY" name="CustomContentMgmtFG.HEADER_ARRAY" tableColStyle="listgreyrowtxtleftline" title="Enterer Name" visible="true" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC12616" maxlength="65" />
                <feba2:field disable="true" id="CustomContentMgmtFG.DESCRIPTION_ARRAY" name="CustomContentMgmtFG.DESCRIPTION_ARRAY" tableColStyle="listgreyrowtxtleftline" title="Enterer Name" visible="true" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC12616" maxlength="65" />            
                 <feba2:Footer pageNo="REQUESTED_PAGE_NUMBER" isPinnable="False" goName="contentListing.GOTO_PAGE__" prevPage="contentListing.GOTO_PREV__" nextPage="contentListing.GOTO_NEXT__" />
              </feba2:Table>
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
        <feba2:rowset displaymode="N" identifier="Rowset2" logical="true" style="width100percent">
          <feba2:row identifier="Rb1" style="button_withoutmargin">
             <feba2:Col identifier="C1" style="">
              <feba2:field caption="Update" id="UPDATE_CONTENT" name="UPDATE_CONTENT" visible="true" disable="false" isDownloadAction="false" style="formbtn" tagHelper="ButtonTagHelper" captionLC="LC78" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="">
              <feba2:field caption="Delete" id="DELETE_CONTENT" name="DELETE_CONTENT" visible="true" disable="false" isDownloadAction="false" style="formbtn" tagHelper="ButtonTagHelper" captionLC="LC13" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="NavPanel1" displaymode="N" identifier="NavigationPanel" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="right">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:compositefield align="horizontal" name="DownloadComponent" visible="false">
                <feba2:labelforcontrol forControlId="CustomContentMgmtFG.OUTFORMAT" id="Label" name="label" visible="${!isVisible}" displayColonAfterDateFormat="true" style="searchsimpletext" text="Download Details As" textLC="LC39" />
                <feba2:field combomode="Simple" id="CustomContentMgmtFG.OUTFORMAT" name="CustomContentMgmtFG.OUTFORMAT" visible="${!isVisible}" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" key="CODE_TYPE=FLFT" preSelectValue="5" preselect="true" select="false" style="dropdownexpandalbe_download" tagHelper="ComboBoxTagHelper" title="Select Download Format" titleLC="LC40" />
                <feba2:field id="GENERATE_REPORT" visible="${!isVisible}" caption="OK" disable="false" function="onclick=&quot;sendAlert();formResourceAction();&quot;" isDownloadAction="false" name="GENERATE_REPORT" style="formbtn_drpdwn" tagHelper="ButtonTagHelper" captionLC="LC33" />
              </feba2:compositefield>
            </feba2:Col>
            <feba2:Col identifier="C2" style="">
              <feba2:field caption="Back" id="LOOKUP_BACK" name="LOOKUP_BACK" style="formbtn_last" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" visible="true" captionLC="LC4" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:hidden name="FG_BUTTONS__" value="LOOK_UP_SELECT,RETRIEVE_LIST,contentListing.GOTO_PAGE__,contentListing.GOTO_PREV__,contentListing.GOTO_NEXT__,ADDITIONAL_SELECT_EVENT,ADDITIONAL_BUTTON_EVENT,GENERATE_REPORT,ADDITIONAL_DELETE_EVENT,MAIN_BACK" />
 <feba2:hidden name="FORMSGROUP_ID__" id="FORMSGROUP_ID__" value="CustomContentMgmtFG" />
 <feba2:hidden name="CustomContentMgmtFG.REPORTTITLE" id="CustomContentMgmtFG.REPORTTITLE" value="CustomContentMgmtList" />
<%if(!isGroupletView){%>
	<%@include file="/jsp/footer.jsp"%>
<%}%>
</feba2:Page>

