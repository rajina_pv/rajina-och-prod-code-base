<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="feba2" uri="/feba_taglib.tld" %>
<%@ page import="com.infosys.feba.framework.ui.util.FebaTagUtil"%>
<%@include file="/jsp/topbar.jsp"%>

<feba2:Set value="${feba2:getValue('CorpAdminFWFG.OPERATION_MODE', 'formfield', pageContext)}" var="sMode"></feba2:Set>
<feba2:Set value="${feba2:getValue('CorpAdminFWFG.OPERATION_MODE','formfield', pageContext)}" var="opModeIndicator"></feba2:Set>
<feba2:Set value="${feba2:equals(opModeIndicator,'VIEW')}" var="isSubmitRequired"></feba2:Set>
<feba2:Set value="${feba2:select('isSubmitRequired#Standard Text Details@@!isSubmitRequired#Preview Confirmation Details',pageContext)}" var="displayMode"></feba2:Set>
<feba2:Set value="${feba2:select('isSubmitRequired#View Standard Text Details@@!isSubmitRequired#Preview Confirmation Details',pageContext)}" var="displayMode1"></feba2:Set>
<feba2:Set value="${feba2:select('isSubmitRequired#true@@!isSubmitRequired#false',pageContext)}" var="visible"></feba2:Set>
<feba2:Set value="${feba2:select('isSubmitRequired#btn-printscreen.gif',pageContext)}" var="prntScreen"></feba2:Set>
<feba2:Set value="${feba2:equalsIgnoreCase(sMode,'CANCEL') or feba2:equalsIgnoreCase(sMode,'REJECT') or feba2:equalsIgnoreCase(sMode,'APPROVE') or feba2:equalsIgnoreCase(sMode,'VIEW')}" var="sBackDispMode"></feba2:Set>
<feba2:Set value="${feba2:equals(opModeIndicator,'INSERT')}" var="opMode"></feba2:Set>
<feba2:SetAlternateView alternateView="${feba2:select('isSubmitRequired#StdTextMnt@@!isSubmitRequired#BankUserPreviewConf',pageContext)}"></feba2:SetAlternateView>
<feba2:title pageTitle="true" text = "${feba2:select('isSubmitRequired#View Standard Text Details||LC10121@@!isSubmitRequired#Preview Confirmation Details||LC64',pageContext)}"/>
<feba2:Page action="Finacle" name="CorpAdminFWFG" forcontrolIDs="Bank ID:=CorpAdminFWFG.BANK_ID@@Text Type:=CorpAdminFWFG.TEXT_TYPE@@Text Name:=CorpAdminFWFG.TEXT_NAME@@Standard Text:=CorpAdminFWFG.STANDARD_TEXT@@Display Order:=CorpAdminFWFG.DISPLAY_ORDER@@">
<% if(!isGroupletView){%>

	<%@include file="/jsp/header.jsp"%>
<%if (!pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
	<%@include file="/jsp/parentTable.jsp"%>
<%}%>
<%@include file="/jsp/sidebar.jsp"%>
<%if (pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
	<%@include file="/jsp/parentTable.jsp"%>
<%} %>
<%}%>
  <feba2:Section id="BrdCrumbNImg" identifier="BreadCrumb" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:moduleTopBar id="mtb1" printScreen="${prntScreen}" breadcrumb="true" helpImage="btn-help.gif" printImage="btn-print.gif" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="PgHeading" identifier="PageHeader" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1">
            <feba2:Col identifier="C1" style="width10">
              <feba2:field alt="Arrow Image" id="ArrowImage" name="a" src="arrow-pageheading.gif" isMenu="false" tagHelper="ImageTagHelper" visible="true" altLC="LC27" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="pageheadingcaps">
              <feba2:title id="PageHeader" text="${feba2:select('isSubmitRequired#View Standard Text Details||LC10121@@!isSubmitRequired#Preview Confirmation Details||LC64',pageContext)}" headinglevel="h1" upperCase="false" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:ErrorDisplay id="ErrorDisplay8912033" style="alerttable" />
  <feba2:Section id="NavMenu" displaymode="N" identifier="NavigationMenuControl" style="greybottomleft">
    <feba2:SubSectionSet identifier="SubSectionSet1" style="greybottomright">
      <feba2:SubSection identifier="SubSection1" style="greytopleft">
        <feba2:rowset identifier="Rowset1" style="greytopright">
          <feba2:row identifier="Ra1" style="greybg_margin">
            <feba2:Col identifier="C1" style="">
              <feba2:field caption="Option" id="FinancialTransactionFG.OPTION_ID" name="FinancialTransactionFG.OPTION_ID" title="Admin Navigator" buttonCaption="OK" tagHelper="AccessControlComboBoxTagHelper" visible="true" captionLC="LC31" titleLC="LC43" buttonCaptionLC="LC33" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="DispForm" identifier="DisplayForm" style="section_blackborder">
    <feba2:SectHeader identifier="Header">
      <feba2:caption id="NetworkProductDetails" style="whtboldtxt" text="Standard Text Details" isDownloadLink="false" isMenu="false" visible="true" textLC="LC7247" />
    </feba2:SectHeader>
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="width100percent">
          <feba2:row identifier="Ra1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CorpAdminFWFG.BANK_ID" id="BankID" name="b" text="Bank ID:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC44" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CorpAdminFWFG.BANK_ID" name="CorpAdminFWFG.BANK_ID" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CorpAdminFWFG.TEXT_TYPE" id="TextType" name="b" text="Text Type:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC7248" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CorpAdminFWFG.TEXT_TYPE" key="CODE_TYPE=STXT" name="CorpAdminFWFG.TEXT_TYPE" visible="true" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CorpAdminFWFG.TEXT_NAME" id="TEXT_NAME" name="b" text="Text Name:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC7250" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CorpAdminFWFG.TEXT_NAME" name="CorpAdminFWFG.TEXT_NAME" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CorpAdminFWFG.STANDARD_TEXT" id="STANDARD_TEXT" name="b" text="Standard Text:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC7252" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CorpAdminFWFG.STANDARD_TEXT" name="CorpAdminFWFG.STANDARD_TEXT" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra5" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CorpAdminFWFG.DISPLAY_ORDER" id="DISPLAY_ORDER" name="b" text="Display Order:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC72510" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CorpAdminFWFG.DISPLAY_ORDER" name="CorpAdminFWFG.DISPLAY_ORDER" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Set value="${!isSubmitRequired}" var="Authorization.visible" />
  <feba2:Set value="${!isSubmitRequired}" var="Authorization.visible" />
  <feba2:Section id="Authorization" displaymode="N" identifier="InputForm_Authorization" style="section_grayborder">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection2" style="width100percent">
        <feba2:rowset identifier="Rowset5" style="width100percent">
          <feba2:row identifier="Re1" style="listingrow">
            <feba2:Col identifier="C1" style="">
              <feba2:field id="ApproverDetails19228432" hideAdditionalDetails="false" isRemarksRequired="true" tagHelper="ApproversDetailsTagHelper" visible="true">
                <feba2:map>
                  <feba2:param name="isMandatory" value="false" />
                  <feba2:param name="isConfidential" value="N" />
                </feba2:map>
              </feba2:field>
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re2" style="listingrow">
            <feba2:Col identifier="C1" style="">
              <feba2:field id="Authorization2958608" name="a" tagHelper="AuthenticationTagHelper" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="NavPanel" displaymode="N" identifier="NavigationPanel" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="right">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:field caption="Submit" id="SUBMIT" name="SUBMIT" style="formbtn_top" title="Submit" visible="${!isSubmitRequired}" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC21" titleLC="LC21" />
            </feba2:Col>
            <feba2:Col identifier="C3" style="">
              <feba2:field caption="Back" id="CONF_BACK" name="CONF_BACK" style="formbtn_last" title="Back" visible="${!sBackDispMode}" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC4" titleLC="LC4" />
            </feba2:Col>
            <feba2:Col identifier="C4" style="">
              <feba2:field caption="Back" id="MAIN_BACK" name="MAIN_BACK" style="formbtn_last" title="Back" visible="${sBackDispMode}" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC4" titleLC="LC4" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
 <feba2:hidden name="FORMSGROUP_ID__" id="FORMSGROUP_ID__" value="CorpAdminFWFG" />
 <feba2:hidden name="CorpAdminFWFG.REPORTTITLE" id="CorpAdminFWFG.REPORTTITLE" value="StdTextMntDetails" />
<%if(!isGroupletView){%>
	<%@include file="/jsp/footer.jsp"%>
<%}%>
</feba2:Page>

