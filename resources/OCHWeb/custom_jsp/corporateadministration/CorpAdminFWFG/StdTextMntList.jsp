<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="feba2" uri="/feba_taglib.tld" %>
<%@ page import="com.infosys.feba.framework.ui.util.FebaTagUtil"%>
<%@include file="/jsp/topbar.jsp"%>

<feba2:Set value="${feba2:getValue('InquiryFWFG.DEL_MULTIPLE','formfield', pageContext)}" var="deleteMode"></feba2:Set>
<feba2:Set value="${feba2:select('deleteMode#true@@!deleteMode#false',pageContext)}" var="selectAllCheckVar"></feba2:Set>
<feba2:SetAlternateView alternateView="${feba2:select('deleteMode#DeleteStandardText@@!deleteMode#StandardTextMaintenanceList',pageContext)}"></feba2:SetAlternateView>
<feba2:title pageTitle="true" text = "${feba2:select('deleteMode#Delete Standard Text||LC10122@@!deleteMode#Standard Text Maintenance||LC10123',pageContext)}"/>
<feba2:Page action="Finacle" name="InquiryFWFG" forcontrolIDs="Bank ID:=InquiryFWFG.BANK_ID@@Text Type:=InquiryFWFG.TEXT_TYPE@@Text Name:=InquiryFWFG.TEXT_NAME@@">
<% if(!isGroupletView){%>

	<%@include file="/jsp/header.jsp"%>
<%if (!pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
	<%@include file="/jsp/parentTable.jsp"%>
<%}%>
<%@include file="/jsp/sidebar.jsp"%>
<%if (pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
	<%@include file="/jsp/parentTable.jsp"%>
<%} %>
<%}%>
  <feba2:Set value="${feba2:getValue('InquiryFWFG.RESULT_LIST_SIZE','formfield', pageContext)}" var="listSize" />
  <feba2:Set value="${feba2:equals(listSize,'0')}" var="isVisible" />
  <feba2:Section id="BrdCrumbNImg" identifier="BreadCrumb" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:moduleTopBar id="mtb" printScreen="btn-printscreen.gif" breadcrumb="true" helpImage="btn-help.gif" printImage="btn-print.gif" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Set value="${feba2:getValue('InquiryFWFG.RESULT_LIST_SIZE','formfield', pageContext)}" var="listSize" />
  <feba2:Set value="${feba2:equals(listSize,'0')}" var="isVisible" />
  <feba2:Section id="PgHeading" identifier="PageHeader" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1">
            <feba2:Col identifier="C1" style="width10">
              <feba2:field alt="Arrow Image" id="ArrowImage" name="a" src="arrow-pageheading.gif" isMenu="false" tagHelper="ImageTagHelper" visible="true" altLC="LC27" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="pageheadingcaps">
              <feba2:title id="PageHeading" text="${feba2:select('deleteMode#Delete Standard Text||LC10122@@!deleteMode#Standard Text Maintenance||LC10123',pageContext)}" headinglevel="h1" upperCase="false" visible="true" />
            </feba2:Col>
            <feba2:Col displaymode="N" identifier="C3" style="right">
              <feba2:field caption="Create New" id="STMINQ.ADDL_SLT_EVNT.CREATE_NEW" name="ADDL_SLT_EVNT--InquiryFWFG.ADDL_INFO::CREATE_NEW--InquiryFWFG.INQID::STMINQ" style="formbtn_top" title="Create New" visible="${!deleteMode}" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC73" titleLC="LC73" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:ErrorDisplay id="ErrorDisplay19198573" style="alerttable" />
  <feba2:Section id="NavMenu" displaymode="N" identifier="NavigationMenuControl" style="greybottomleft">
    <feba2:SubSectionSet identifier="SubSectionSet1" style="greybottomright">
      <feba2:SubSection identifier="SubSection1" style="greytopleft">
        <feba2:rowset identifier="Rowset1" style="greytopright">
          <feba2:row identifier="Ra1" style="greybg_margin">
            <feba2:Col identifier="C1" style="">
              <feba2:field caption="Option" id="TradeFG.OPTION_ID" name="TradeFG.OPTION_ID" title="Admin Navigator" buttonCaption="OK" tagHelper="AccessControlComboBoxTagHelper" visible="true" captionLC="LC31" titleLC="LC43" buttonCaptionLC="LC33" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Set value="${!deleteMode}" var="SearchPanel.visible" />
  <feba2:Set value="${!deleteMode}" var="NavPanel.visible" />
  <feba2:Section id="SearchPanel" collapsibleDisplay="allCollapse" displaymode="N" identifier="SearchPanel" style="section_blackborder">
    <feba2:SectHeader identifier="Header">
      <feba2:caption id="TableHeader" style="whtboldtxt" text="Search Criteria" visible="${!deleteMode}" isDownloadLink="false" isMenu="false" textLC="LC74" />
    </feba2:SectHeader>
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="width100percent">
          <feba2:row identifier="Ra1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="InquiryFWFG.BANK_ID" id="BankID" name="b" text="Bank ID:" visible="${!deleteMode}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC44" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="InquiryFWFG.BANK_ID" name="InquiryFWFG.BANK_ID" visible="${!deleteMode}" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="InquiryFWFG.TEXT_TYPE" id="TEXT_TYPE" name="b" text="Text Type:" visible="${!deleteMode}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC7248" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field all="blank" allDescription="All" id="InquiryFWFG.TEXT_TYPE" key="CODE_TYPE=STXT" name="InquiryFWFG.TEXT_TYPE" title="Text Type" visible="${!deleteMode}" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" select="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" allDescriptionLC="LC116" titleLC="LC7255" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="InquiryFWFG.TEXT_NAME" id="TEXT_NAME" name="b" text="Text Name:" visible="${!deleteMode}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC7250" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="InquiryFWFG.TEXT_NAME" name="InquiryFWFG.TEXT_NAME" style="querytextboxmedium" title="Text name" visible="${!deleteMode}" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" titleLC="LC10124" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="NavPanel" displaymode="N" identifier="NavigationPanel" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="right">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:field caption="Search" id="RETRIEVE_LIST" name="RETRIEVE_LIST" title="Search" visible="${!deleteMode}" disable="false" isDownloadAction="false" style="formbtn" tagHelper="ButtonTagHelper" captionLC="LC75" titleLC="LC75" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="">
              <feba2:field caption="Clear" id="RESET" name="RESET" style="formbtn_last" title="Clear" visible="${!deleteMode}" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC76" titleLC="LC76" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Set value="${!isVisible}" var="ListTableWithCtrls.visible" />
  <feba2:Section id="ListTableWithCtrls" identifier="ListingTable_With_Navigation_Controls_black_border" style="section_fourlinbrd">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="false" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="false" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="false" style="width100percent">
          <feba2:row identifier="Ra1" style="listingrow">
            <feba2:Col identifier="C1" style="">
              <feba2:Table caption="Standard Text List" captionstyle="" collapsableFlag="" collapsableId="" collapsible="" footerStyle="" headerStyle="${feba2:select('!deleteMode#listtopbigbg@@deleteMode#gradientbgonelinewithouttxt',pageContext)}" headinglevel="${feba2:select('!deleteMode#h3',pageContext)}" hideEmptyTable="true" id="StdTextMntInqListing" lastupdatedate="" listDisplay="Displaying$range$of$total$results$" listing="StdTextMntInqListing" pagination="true" paginationButtonCaption="Go" paginationFooter="Page$current$of$total" paginationFooterText="Go to Page" rowOneStyle="listgreyrow" rowTwoStyle="listwhiterow" selectAllCheck="${selectAllCheckVar}" summary="" tableDisplayStyle="" tableStyle="width100percent" visible="true" width="100" captionLC="LC7254" listDisplay1LC="LC22" listDisplay2LC="LC17" listDisplay3LC="LC23" paginationButtonCaptionLC="LC24" paginationFooter1LC="LC25" paginationFooter2LC="LC17" paginationFooterTextLC="LC26">
                <feba2:Header>
                  <feba2:Heading id="column16671929" sortOrder="ASC" style="slno" visible="${!deleteMode}" text="Select" textLC="LC38" />
                  <feba2:Heading id="column11107083" sortOrder="ASC" style="slno" visible="${deleteMode}" text="" />
                  <feba2:Heading id="column1267757" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Bank ID" textLC="LC69" />
                  <feba2:Heading id="column5678233" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Text Name" textLC="LC7251" />
                  <feba2:Heading id="column31038029" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Text Type" textLC="LC7255" />
                </feba2:Header>
                <feba2:Set value="${feba2:getValue('InquiryFWFG.SHADOW_ARRAY','formfield', pageContext)}" var="flagIndicator" />
                <feba2:Set value="${feba2:equals(flagIndicator,'Y')}" var="shadowIndicator" />
                <feba2:Set value="${feba2:select('shadowIndicator#listgreyrowredtxtleftline@@!shadowIndicator#simpletext',pageContext)}" var="cssStyle" />
                <feba2:Set value="${feba2:select('shadowIndicator#redlink@@!shadowIndicator#bluelink',pageContext)}" var="hyperLinkStyle" />
                <feba2:field id="InquiryFWFG.SELECTED_INDEX" name="InquiryFWFG.SELECTED_INDEX" tableColStyle="listgreyrowtxtwithoutline" title="Select Standard Text" visible="${!deleteMode}" style="absmiddle" tagHelper="TableRadioTagHelper" usage="I" value="true" titleLC="LC10125" />
                <feba2:field id="InquiryFWFG.SELECTED_INDEX_ARRAY" name="InquiryFWFG.SELECTED_INDEX_ARRAY" tableColStyle="listgreyrowtxtwithoutline" title="Select Standard Text" visible="${deleteMode}" disable="false" selectAllCheck="false" tagHelper="TableCheckBoxTagHelper" titleLC="LC10125" />
                <feba2:field id="InquiryFWFG.BANK_ID_ARRAY" name="InquiryFWFG.BANK_ID_ARRAY" style="${cssStyle}" tableColStyle="listgreyrowtxtleftline" disable="false" isDownloadLink="false" isMenu="false" readonly="false" tagHelper="OutputTextTagHelper" visible="true" />
                <feba2:field id="InquiryFWFG.TEXT_NAME_ARRAY" link="FORMSGROUP_ID__= InquiryFWFG#__EVENT_ID__= ADDL_SLT_EVNT#InquiryFWFG.ADDL_INFO=VIEW#InquiryFWFG.INQID=POMINQ#InquiryFWFG.SELECTED_INDEX={INDEX,page}" linkstyle="${hyperLinkStyle}" name="InquiryFWFG.TEXT_NAME_ARRAY" style="${cssStyle}" tableColStyle="listgreyrowtxtleftline" title="View Details" disable="false" isDownloadLink="false" isMenu="false" readonly="false" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC255" />
                <feba2:field id="InquiryFWFG.TEXT_TYPE_ARRAY" key="CODE_TYPE=STXT" name="InquiryFWFG.TEXT_TYPE_ARRAY" style="${cssStyle}" tableColStyle="listgreyrowtxtleftline" displayEvenIfAbsent="false" resourceName="CommonCode" tagHelper="GenericDescriptionTagHelper" />
                <feba2:Footer pageNo="REQUESTED_PAGE_NUMBER" isPinnable="False" goName="StdTextMntInqListing.GOTO_PAGE__" prevPage="StdTextMntInqListing.GOTO_PREV__" nextPage="StdTextMntInqListing.GOTO_NEXT__" />
              </feba2:Table>
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
        <feba2:rowset displaymode="N" identifier="Rowset2" logical="true" style="width100percent">
          <feba2:row identifier="Rb1" style="button_withoutmargin">
            <feba2:Col identifier="C1" style="">
              <feba2:field caption="Update" id="STDINQ.ADDL_SLT_EVNT.UPDATE" name="ADDL_SLT_EVNT--InquiryFWFG.ADDL_INFO::UPDATE--InquiryFWFG.INQID::STDINQ" style="formbtn_top" title="Update" visible="${!deleteMode}" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC78" titleLC="LC78" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="">
              <feba2:field caption="Delete" id="STDINQ.DLT_MULT_EVNT.DELETE" name="DLT_MULT_EVNT--InquiryFWFG.ADDL_INFO::DELETE--InquiryFWFG.INQID::STDINQ" style="formbtn_last" title="Delete" visible="${!deleteMode}" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC13" titleLC="LC13" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="NavPanel1" displaymode="N" identifier="NavigationPanel" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="right">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C2" style="">
              <feba2:field caption="Delete" id="ADDITIONAL_DELETE_EVENT" name="ADDITIONAL_DELETE_EVENT" style="formbtn_last" title="Delete" visible="${deleteMode}" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC13" titleLC="LC13" />
            </feba2:Col>
            <feba2:Col identifier="C3" style="">
              <feba2:field caption="Back" id="MAIN_BACK" name="MAIN_BACK" title="Back" visible="${deleteMode}" disable="false" isDownloadAction="false" style="formbtn" tagHelper="ButtonTagHelper" captionLC="LC4" titleLC="LC4" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
 <feba2:hidden name="FORMSGROUP_ID__" id="FORMSGROUP_ID__" value="InquiryFWFG" />
 <feba2:hidden name="InquiryFWFG.REPORTTITLE" id="InquiryFWFG.REPORTTITLE" value="StdTextMntList" />
<%if(!isGroupletView){%>
	<%@include file="/jsp/footer.jsp"%>
<%}%>
</feba2:Page>

