SET DEFINE OFF;
UPDATE PRPM SET PROPERTY_VAL='6' WHERE PROPERTY_NAME='SMS_OTP_LENGTH' AND BANK_ID='01';
COMMIT;

INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','PINANG_DIVISION_NAME','SIT Pinang','Division name of SMS Owner','N','setup',SYSDATE,'setup',SYSDATE);
INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','PINANG_PRODUCT_NAME','Sms Dev Pinang','Product Name of the SMS','N','setup',SYSDATE,'setup',SYSDATE);
INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','SAHABAT_DIVISION_NAME','SIT Sahabat','Division name of SMS Owner','N','setup',SYSDATE,'setup',SYSDATE);
INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','SAHABAT_PRODUCT_NAME','Sms Dev Sahabat','Product Name of the SMS','N','setup',SYSDATE,'setup',SYSDATE);

--Added for Credit Score Calculation Start--
Insert into PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','BWY','PERSONAL_NUMBER','123456','Employee number for Credit Score Calculation','N','setup',sysdate,'setup',sysdate);
--Added for Credit Score Calculation End--
COMMIT;

Insert into PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','BWY','BRANCH_CODE_SUFFIX','891','This is branch code suffix','N','setup',sysdate,'setup',sysdate);


--Vidhi 19th Sept Start--
Insert into PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','BWY','CUST_MAX_TOKEN_TRIES','3','Max retries for token generation','N','setup',sysdate,'setup',sysdate);
commit;
--Vidhi 19th Sept End--

--Protik 20th Sept Start--
update PRPM set property_val='IDR' where property_name='HOME_CUR_CODE' and bank_id='01';
--Protik 20th Sept End--

--Added for loan application list inquiry by Ambili on 20-09-2018 : start
INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','CREDIT_SCR_CAL_TIME_INTRVL','15','Time interval in minutes for credit score to calculate and  update credit score','N','setup',SYSDATE,'setup',SYSDATE);
INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','COOLDOWN_PERIOD_FOR_CREDIT_REJECTION','30','Cooldown period in days for a system rejected application , post will user will be allowed to apply again','N','setup',SYSDATE,'setup',SYSDATE);
INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','BAN_PERIOD_FOR_USER_REJECTION','7','Cooldown period in days for a user rejected application , post will user will be allowed to apply again','N','setup',SYSDATE,'setup',SYSDATE);
INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','ALLOWED_CONSECUTIVE_USER_REJ','3','Allowed number of user rejection post which a temporary ban will be put on user for application','N','setup',SYSDATE,'setup',SYSDATE);
INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','ALLOWED_PERIOD_FOR_INCOMPLETE_APP','10','Allowed period in days to complete the incomplete credit approval process','N','setup',SYSDATE,'setup',SYSDATE);
INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','CREDIT_OFFER_VALIDITY_PERIOD','21','Offer Validity Period in days','N','setup',SYSDATE,'setup',SYSDATE);
commit;
--Added for loan application list inquiry by Ambili on 20-09-2018 : start

--Added by pratik for HMAC Header  20-09-2018 | START
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time",
"r_cre_id","r_cre_time") 
values (1,'01','BWY','APIGEE_TOKEN','f3pHl90RwFpKnl0vH9S9XLQC6ZZfbfs9',
'Apigee Token for HMAC header generation','N',
'setup',sysdate,'setup',sysdate);
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time",
"r_cre_id","r_cre_time") 
values (1,'01','BWY','APIGEE_SECRET_KEY','Ceew5mvuleA7qkKH',
'Apigee Secret Key for HMAC header generation','N',
'setup',sysdate,'setup',sysdate);
COMMIT;
--Added by pratik for HMAC Header  20-09-2018 | END

--Added for Password Policy set up for online registration--
update prpm set property_val ='' where property_name ='SPWD_SET_PWD_NUM_CHARACTERS_EXCLUDE' and bank_id ='01' and app_id ='BWY';
update prpm set property_val ='' where property_name ='SPWD_PWD_NUM_CHARACTERS_EXCLUDE' and bank_id ='01' and app_id ='BWY';
update prpm set property_val ='N' where property_name ='SPWD_SET_PWD_SPECIAL_CHAR_MANDATORY' and bank_id ='01' and app_id ='BWY';
update prpm set property_val ='N' where property_name ='SPWD_PWD_SPECIAL_CHAR_MANDATORY' and bank_id ='01' and app_id ='BWY';
update prpm set property_val ='N' where property_name ='SPWD_PWDALPHAMAND' and bank_id ='01' and app_id ='BWY';
update prpm set property_val ='N' where property_name ='SPWD_PWDFOURCHARMAND' and bank_id ='01' and app_id ='BWY';
update prpm set property_val ='N' where property_name ='SPWD_PWDDIGITMAND' and bank_id ='01' and app_id ='BWY';
INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','ONLINE_REG_CHANNEL_LIST','I','Internet channel','N','setup',SYSDATE,'setup',SYSDATE);
--Added for Password Policy set up for online registration--


--Added by Ambili
update prpm set property_val='80' where property_name='MAX_FILENAME_LENGTH';
commit;
--Added by Ambili
  
--Added for Notification core batch --
INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','CUSTOM_NOTIFICATION_CORE_INPUTPATH','D:\\My_Work\\BRI\\batch\\','Notification Core Batch Input file path','N','setup',SYSDATE,'setup',SYSDATE);
INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','CUSTOM_NOTIFICATION_CORE_OUTPUTPATH','D:\\My_Work\\BRI\\batch\\output','Notification Core Batch Input file path','N','setup',SYSDATE,'setup',SYSDATE);
--Added for Notification core batch --
commit;
delete from PRPM where PROPERTY_NAME='CUSTOM_NOTIFICATION_CORE_INPUTPATH' and bank_id='01';
delete from PRPM where PROPERTY_NAME='CUSTOM_NOTIFICATION_CORE_OUTPUTPATH' and bank_id='01';
--Added for Notification core batch --
INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','CUSTOM_NOTIFICATION_CORE_INPUTPATH','/BRIEDB_FE/OCH/FebaBatch/input/CoreBatchNotificationEventInput','Notification Core Batch Input file path','N','setup',SYSDATE,'setup',SYSDATE);
INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','CUSTOM_NOTIFICATION_CORE_OUTPUTPATH','/BRIEDB_FE/OCH/FebaBatch/input/CustomBatchNotificationEventOutput','Notification Core Batch Output file path','N','setup',SYSDATE,'setup',SYSDATE);
--Added for Notification core batch --
commit;
INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','CREDIT_LIMIT_SCHEME_CODE','CERIA','Scheme Code','N','setup',SYSDATE,'setup',SYSDATE);
INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','CREDIT_LIMIT_EXPIRY_DATE_IN_YEARS','5','Expiry Date in Years','N','setup',SYSDATE,'setup',SYSDATE);
commit;

--PRPM update
INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','CREDIT_SCORE_SUCCESS','success','Success Status for Credit Scoring Approval','N','setup',SYSDATE,'setup',SYSDATE);

INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','CREDIT_SCORE_FAILED','failure','Failure Status for Credit Scoring Approval','N','setup',SYSDATE,'setup',SYSDATE);

INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','PRIVY_MERCHANT_KEY','hSmmyXqkgKGCw2TM7TUW','Merchant Key for Privy registration','N','setup',SYSDATE,'setup',SYSDATE);

commit;

update PRPM set property_val='N' where property_name in('CHECK_FILE_NAME_DUPLICITY','CHECK_FILE_CONTENT_DUPLICITY') and bank_id='01';
COMMIT;

INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','COOLDOWN_PERIOD_FOR_API_CALL','10','Cooldown period for next try after exceeding the max tries','N','setup',SYSDATE,'setup',SYSDATE);


INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','ALLOWED_TRIES_FOR_API_CALL','3','Max tries for API invocation','N','setup',SYSDATE,'setup',SYSDATE);
COMMIT;

--Added for credit limit
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','HOME_BRANCH_CODE','001','Home Branch Code','N','setup',sysdate,'setup',sysdate);
--Added for credit limit end

--Callback URL update
UPDATE PRPM SET PROPERTY_VAL='http://172.18.133.198:9001' WHERE PROPERTY_NAME='CREDT_SCORE_ROOT_URL' AND BANK_ID='01';

--Document list service
UPDATE PRPM SET PROPERTY_VAL='DAOF|DBTL' WHERE PROPERTY_NAME='LOAN_APPLN_DOC' AND BANK_ID='01';

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','OCR_CHECK_REQUIRED','Y','Flag for OCR check (Y/N)','N','setup',sysdate,'setup',sysdate);
COMMIT;
--Forgot password--sahana-start-25/10/2018
update prpm set property_val ='' where property_name ='TPWD_SET_PWD_NUM_CHARACTERS_EXCLUDE' and bank_id ='01' and app_id ='BWY';
update prpm set property_val ='' where property_name ='TPWD_PWD_NUM_CHARACTERS_EXCLUDE' and bank_id ='01' and app_id ='BWY';
update prpm set property_val ='N' where property_name ='TPWD_SET_PWD_SPECIAL_CHAR_MANDATORY' and bank_id ='01' and app_id ='BWY';
update prpm set property_val ='N' where property_name ='TPWD_PWD_SPECIAL_CHAR_MANDATORY' and bank_id ='01' and app_id ='BWY'; 
COMMIT;
--Forgot password--sahana-end-25/10/2018

--Updated Callback service incoming status in failed scenario
UPDATE PRPM SET PROPERTY_VAL='failed' WHERE PROPERTY_NAME='CREDIT_SCORE_FAILED';