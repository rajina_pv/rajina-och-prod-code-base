Insert into TKDT ("bank_id","db_ts","task_name","task_group","description","mnu_id","task_handler_name","task_handler_type","task_data","subtasks","del_flg","r_mod_id",
"r_mod_time","r_cre_id","r_cre_time") values
('01',1,'CUSTNOTGEN','B','Custom Notification Event generation batch','GN','com.infosys.feba.framework.batch.scheduler.BatchTaskInvoker',
'J',null,'CUSTNOTGEN','N',
'setup',sysdate,'setup',sysdate);

Insert into TKDT ("bank_id","db_ts","task_name","task_group","description","mnu_id","task_handler_name","task_handler_type","task_data","subtasks","del_flg","r_mod_id",
"r_mod_time","r_cre_id","r_cre_time") values
('01',1,'CUSTOMNOTCORE','B','Custom Notification Event core batch','GN','com.infosys.feba.framework.batch.scheduler.BatchTaskInvoker',
'J',null,'CUSTOMNOTCORE','N',
'setup',sysdate,'setup',sysdate);

commit;

#Added by Prasad for Loan Application Status Update Batch
Insert into TKDT ("bank_id","db_ts","task_name","task_group","description","mnu_id","task_handler_name","task_handler_type","task_data","subtasks","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values ('01',1,'APPSTATUSUPDATE','B','Custom Application Status Upadte batch','GN','com.infosys.feba.framework.batch.scheduler.BatchTaskInvoker','J',null,'APPSTATUSUPDATE','N','setup',to_timestamp('2018-10-22 11:52:36.0','null'),'setup',to_timestamp('2018-10-22 11:52:36.0','null'));
COMMIT;

Insert into TKDT ("bank_id","db_ts","task_name","task_group","description","mnu_id","task_handler_name","task_handler_type","task_data","subtasks","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") 
values ('01',1,'LNDTLDWLD','B','Loan Detail Download Batch','GN','com.infosys.feba.framework.batch.scheduler.BatchTaskInvoker','J','OUTPUT_FILE_NAME=LoanDetail.dat;OUTPUT_FOLDER_PATH=D:\\reports\\etc\\','LNDTLDWLD','N','setup',sysdate,'setup',sysdate);