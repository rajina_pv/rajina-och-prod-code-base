SET DEFINE OFF;
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','LNPP','PR3','001','Principal 3','N','setup',
sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','LNPP','PR2','001','Principal 2','N','setup',
sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','LNPP','PR1','001','Principal 1','N','setup',
sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','LNCT','PRIN','001','PRINCIPAL','N','setup',
sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','LNCT','INST','001','INTEREST','N','setup',
sysdate,'setup',sysdate);
COMMIT;
--Added for Credit Score Calculation Start--
REM INSERTING into COCD
SET DEFINE OFF;
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','LNTP','Sahabat','001','901','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','LNTP','Pinang','001','902','N','setup',sysdate,'setup',sysdate);
COMMIT;
--Added for Credit Score Calculation End--

Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','CABL','452708','001','Y','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','CABL','387911','001','Y','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','CABL','601811','001','N','N','setup',sysdate,'setup',sysdate);


Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','CTYP','MRC','001','Merchants','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','CTYP','PRO','001','Promos','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','CTYP','SPL','001','Splash Screen','N','setup',sysdate,'setup',sysdate);
commit;

Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','MRTY','MR1','001','Merchant 1','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','MRTY','MR2','001','Merchant 2','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','MRTY','MR3','001','Merchant 3','N','setup',sysdate,'setup',sysdate);
commit;

Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','PRI','1','001','1','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','PRI','2','001','2','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','PRI','3','001','3','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','PRI','4','001','4','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','PRI','5','001','5','N','setup',sysdate,'setup',sysdate);
commit;


Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','BUA','ACM','001','Content Management','N','setup',sysdate,'setup',sysdate);
commit;



insert into COCD values (1, '01', 'PCO', 'HLA', '001', 'Product Code', 'N', 'setup', SYSDATE, 'setup', SYSDATE);
insert into COCD values (1, '01', 'PCO', 'HLA1', '001', 'Product Code 1', 'N', 'setup', SYSDATE, 'setup', SYSDATE);
insert into COCD values (1, '01', 'PCO', 'HLA2', '001', 'Product Code 2', 'N', 'setup', SYSDATE, 'setup', SYSDATE);
insert into COCD values (1, '01', 'PCA', 'HLA01', '001', 'Product Category 1', 'N', 'setup', SYSDATE, 'setup', SYSDATE);
insert into COCD values (1, '01', 'PCA', 'HLA02', '001', 'Product Category 2', 'N', 'setup', SYSDATE, 'setup', SYSDATE);
insert into COCD values (1, '01', 'PCA', 'HLA03', '001', 'Product Category 3', 'N', 'setup', SYSDATE, 'setup', SYSDATE);
insert into COCD values (1, '01', 'CFT', 'PRINCIPAL', '001', 'Principal', 'N', 'setup', SYSDATE, 'setup', SYSDATE);
insert into COCD values (1, '01', 'CFT', 'INSTALLMENT', '001', 'Installment', 'N', 'setup', SYSDATE, 'setup', SYSDATE);
insert into COCD values (1, '01', 'CTN', '1', '001', '1', 'N', 'setup', SYSDATE, 'setup', SYSDATE);
insert into COCD values (1, '01', 'CTN', '2', '001', '2', 'N', 'setup', SYSDATE, 'setup', SYSDATE);
insert into COCD values (1, '01', 'CTN', '3', '001', '3', 'N', 'setup', SYSDATE, 'setup', SYSDATE);
insert into COCD values (1, '01', 'MID', 'ALL', '001', 'ALL', 'N', 'setup', SYSDATE, 'setup', SYSDATE);

commit;


--standardTextChange by pratik
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','STXT','TNC','001','Terms and Conditions','N','setup',
sysdate,'setup',sysdate);

--loansimulation
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','LMID','ALL','001','ALL MERCHANTS','N','setup',
sysdate,'setup',sysdate);

Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','LTEN','01','001','1 month','N','setup',
sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 

values (1,'01','LTEN','03','001','3 months','N','setup',
sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','LTEN','06','001','6 months','N','setup',
sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','LTEN','12','001','12 months','N','setup',
sysdate,'setup',sysdate);


Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','ASTG','KTP_SAVED','001','KTP Document Saved','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','ASTG','PER_SAVED','001','Personal Info Saved','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','ASTG','CON_SAVED','001','Contact Info Saved','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','ASTG','EMP_SAVED','001','Employment Info Saved','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','ASTG','CR_SCORE_SUB','001','Credit Score Submitted','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','ASTG','CR_SCORE_APR','001','Credit Score Approved','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','ASTG','CR_SCORE_REJ','001','Credit Score Rejected','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','ASTG','USR_REJECT','001','User Rejected','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','ASTG','ACT_LINK_CONF','001','Activation Link confirmed','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','ASTG','DISB_ACC_CONF','001','Disbursement Account Confirmed','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','ASTG','EKYC_COM','001','EKYC Completed','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','ASTG','DIG_SIGN_COM','001','Digital Signature Created','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','ASTG','DOCUMENT_SIGNED','001','Document Signed','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','ASTG','LOAN_CREATED','001','Loan Created','N','setup',sysdate,'setup',sysdate);

Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','ASVS','KTP_SAVED','001','1','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','ASVS','PER_SAVED','001','2','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','ASVS','CON_SAVED','001','3','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','ASVS','EMP_SAVED','001','4','N','setup',sysdate,'setup',sysdate);
commit;

delete from cocd where code_type='PCA' and bank_id='01';
delete from cocd where code_type='PCO' and bank_id='01';
delete from cocd where code_type='CFT' and bank_id='01';
insert into COCD values (1, '01', 'CRN', 'IDR', '001', 'IDR', 'N', 'setup', SYSDATE, 'setup', SYSDATE);
insert into COCD values (1, '01', 'CFT', 'PRIN', '001', 'Principal', 'N', 'setup', SYSDATE, 'setup', SYSDATE);
insert into COCD values (1, '01', 'CFT', 'INST', '001', 'Installment', 'N', 'setup', SYSDATE, 'setup', SYSDATE);



DELETE FROM COCD WHERE CODE_TYPE='LTEN' AND CM_CODE IN ('01', '03', '06', '12') and BANK_ID='01';
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','LTEN','1','001','1 month','N','setup',
sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','LTEN','3','001','3 months','N','setup',
sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','LTEN','6','001','6 months','N','setup',
sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','LTEN','12','001','12 months','N','setup',
sysdate,'setup',sysdate);

--Vidhi 24Sept Loan balance start
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','LTP','A','001','Active','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','LTP','L','001','Paid','N','setup',sysdate,'setup',sysdate);
commit;
--Vidhi 24Sept Loan balance end


delete from cocd where code_type='CABL' and bank_id='01';
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','CABL','RGLR','001','Y','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','CABL','GOLD','001','Y','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','CABL','PLAT','001','Y','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','CABL','PVRGLR','001','N','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','CABL','PVGOLD','001','N','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','CABL','PVPLAT','001','N','N','setup',sysdate,'setup',sysdate);
commit;

insert into COCD values (1, '01', 'CTN', '4', '001', '4', 'N', 'setup', SYSDATE, 'setup', SYSDATE);
insert into COCD values (1, '01', 'CTN', '5', '001', '5', 'N', 'setup', SYSDATE, 'setup', SYSDATE);
insert into COCD values (1, '01', 'CTN', '6', '001', '6', 'N', 'setup', SYSDATE, 'setup', SYSDATE); 
insert into COCD values (1, '01', 'CTN', '7', '001', '7', 'N', 'setup', SYSDATE, 'setup', SYSDATE);
insert into COCD values (1, '01', 'CTN', '8', '001', '8', 'N', 'setup', SYSDATE, 'setup', SYSDATE);
insert into COCD values (1, '01', 'CTN', '9', '001', '9', 'N', 'setup', SYSDATE, 'setup', SYSDATE); 
insert into COCD values (1, '01', 'CTN', '10', '001', '10', 'N', 'setup', SYSDATE, 'setup', SYSDATE);
insert into COCD values (1, '01', 'CTN', '11', '001', '11', 'N', 'setup', SYSDATE, 'setup', SYSDATE);
insert into COCD values (1, '01', 'CTN', '12', '001', '12', 'N', 'setup', SYSDATE, 'setup', SYSDATE);
commit;

--COCD codes Added for 1.Occuppied Time duration,2. Field for Work, 3. Employment Status, 4.Source of Income, 5. Card Issuer Bank 6. Education Status 
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','OCTM','00','001','0 Tahun','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','OCTM','01','001','1 Tahun','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','OCTM','02','001','2 Tahun','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','OCTM','03','001','3 Tahun','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','OCTM','04','001','4 Tahun','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','OCTM','05','001','> 5 Tahun','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','TYOW','KARY','001','Karyawan','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','TYOW','PROF','001','Profesional','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','TYOW','PENS','001','Pensiunan','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','TYOW','TNIP','001','TNI/POLRI','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','TYOW','WIRA','001','Wiraswasta','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','TYOW','LAIN','001','Lain-Lain ','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','EMPS','TETP','001','Tetap','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','EMPS','KNTR','001','Kontrak/Outsourcing ','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','EMPS','HARN','001','Harian','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','EMPS','LNYA','001','Lainnya','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','INSC','GAJI','001','Gaji','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','INSC','USAH','001','Usaha','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','INSC','LNYA','001','Lainnya','N','setup',sysdate,'setup',sysdate);
Insert into COCD(DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)values(1,'01','CCBK','ANZ','001','BankANZIndonesia','N','setup',sysdate,'setup',sysdate);
Insert into COCD(DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)values(1,'01','CCBK','BUKO','001','BankBukopin','N','setup',sysdate,'setup',sysdate);
Insert into COCD(DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)values(1,'01','CCBK','ICB','001','BankICBBumiputera','N','setup',sysdate,'setup',sysdate);
Insert into COCD(DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)values(1,'01','CCBK','BCA','001','BankCentralAsia','N','setup',sysdate,'setup',sysdate);
Insert into COCD(DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)values(1,'01','CCBK','CIMB','001','BankCIMBNiaga','N','setup',sysdate,'setup',sysdate);
Insert into COCD(DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)values(1,'01','CCBK','BDI','001','BankDanamonIndonesia','N','setup',sysdate,'setup',sysdate);
Insert into COCD(DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)values(1,'01','CCBK','ICBC','001','BankICBCIndonesia','N','setup',sysdate,'setup',sysdate);
Insert into COCD(DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)values(1,'01','CCBK','BII','001','BankInternasionalIndonesia','N','setup',sysdate,'setup',sysdate);
Insert into COCD(DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)values(1,'01','CCBK','MNDR','001','BankMandiri','N','setup',sysdate,'setup',sysdate);
Insert into COCD(DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)values(1,'01','CCBK','MEGA','001','BankMega','N','setup',sysdate,'setup',sysdate);
Insert into COCD(DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)values(1,'01','CCBK','BNI','001','BankNegaraIndonesia1946','N','setup',sysdate,'setup',sysdate);
Insert into COCD(DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)values(1,'01','CCBK','PIB','001','PANIndonesiaBank','N','setup',sysdate,'setup',sysdate);
Insert into COCD(DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)values(1,'01','CCBK','BRI','001','BakRakyatIndonesia','N','setup',sysdate,'setup',sysdate);
Insert into COCD(DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)values(1,'01','CCBK','BP','001','BankPermata','N','setup',sysdate,'setup',sysdate);
Insert into COCD(DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)values(1,'01','CCBK','CITI','001','CITIBANK','N','setup',sysdate,'setup',sysdate);
Insert into COCD(DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)values(1,'01','CCBK','THSB','001','TheHongkong&ShanghaiBank','N','setup',sysdate,'setup',sysdate);
Insert into COCD(DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)values(1,'01','CCBK','OCBC','001','BankOCBCNISP','N','setup',sysdate,'setup',sysdate);
Insert into COCD(DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)values(1,'01','CCBK','SCB','001','StandardCharteredBank','N','setup',sysdate,'setup',sysdate);
Insert into COCD(DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)values(1,'01','CCBK','UOB','001','BankUOBIndonesia','N','setup',sysdate,'setup',sysdate);
Insert into COCD(DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)values(1,'01','CCBK','BNIS','001','BNISyariah','N','setup',sysdate,'setup',sysdate);
Insert into COCD(DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)values(1,'01','CCBK','BSIN','001','BankSinarmas','N','setup',sysdate,'setup',sysdate);
Insert into COCD(DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)values(1,'01','CCBK','AEON','001','AEONCreditServices','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','EDU','SD','001','Sekolah Dasar ','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','EDU','SMP','001','Sekolah Menengah Pertama(SMP)','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','EDU','SMA','001','Sekolah Menengah Atas(SMA)','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','EDU','D1','001','Diploma 1(D1)','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','EDU','D2','001','Diploma 2(D2)','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','EDU','D3','001','Diploma 3(D3)','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','EDU','S1','001','Strata 1(S1)','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','EDU','S2','001','Strata 2(S2)','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','EDU','S3','001','Strata 3(S3)','N','setup',sysdate,'setup',sysdate);
commit;

--Added by Ambili for loan status inquiry
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','ASTG','APP_EXPIRED','001','Application Expired','N','setup',sysdate,'setup',sysdate);
commit;
--Added by Ambili for loan status inquiry



--NOtification related entries by Pratik
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','CNED','1','001','First Day','N','setup',
sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','CNED','2','001','Second Day','N','setup',
sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','CNED','3','001','Third Day','N','setup',
sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','CNED','4','001','Fourth Day','N','setup',
sysdate,'setup',sysdate);

--Added for Loan Purpose
Insert into COCD values (1,'01','LNPS','BSK','001','Biaya Sekolah','N','setup',sysdate,'setup',sysdate);
Insert into COCD values (1,'01','LNPS','BPG','001','Biaya Pengobatan','N','setup',sysdate,'setup',sysdate);
Insert into COCD values (1,'01','LNPS','KRT','001','Keperluan Rumah Tangga','N','setup',sysdate,'setup',sysdate);
Insert into COCD values (1,'01','LNPS','RRM','001','Renovasi Rumah','N','setup',sysdate,'setup',sysdate);
Insert into COCD values (1,'01','LNPS','MOU','001','Modal Usaha','N','setup',sysdate,'setup',sysdate);
Insert into COCD values (1,'01','LNPS','PAE','001','Pembelian Alat Elektronik','N','setup',sysdate,'setup',sysdate);
Insert into COCD values (1,'01','LNPS','LNN','001','Lainnya','N','setup',sysdate,'setup',sysdate);

DELETE FROM ECECUSER.COCD WHERE CODE_TYPE in('RLT','RES','DEPN') AND BANK_ID = '01';
DELETE FROM ECECUSER.COCD WHERE CODE_TYPE in('GEN','MST)' AND BANK_ID = '01';
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'RLT', 'RLT1', '001', 'Kakak/Adik Kandung', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'RLT', 'RLT2', '001', 'Orang Tua', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'RLT', 'RLT3', '001', 'Sepupu', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'RLT', 'RLT4', '001', 'Kemenakan', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'RLT', 'RLT5', '001', 'Ipar', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'RLT', 'RLT6', '001', 'Lainnya', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'RES', 'RES1', '001', 'Milik Sendiri', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'RES', 'RES2', '001', 'Mililk Orang Tua', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'RES', 'RES3', '001', 'Milik Keluarga', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'RES', 'RES4', '001', 'Kontrak / Sewa', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'RES', 'RES5', '001', 'Lainnya', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'DEPN', '1', '001', '1', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'DEPN', '2', '001', '2', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'DEPN', '3', '001', '3', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'DEPN', '4', '001', '4', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'DEPN', '5', '001', '5', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'DEPN', '>5', '001', '>5', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'GEN', 'M', '001', 'Pria', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'GEN', 'F', '001', 'Wanita', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'MST', 'MST1', '001', 'Menikah', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'MST', 'MST2', '001', 'Duda/Janda', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'MST', 'MST3', '001', ' Lajang', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);


REM INSERTING into COCD
SET DEFINE OFF;
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','DTPE','DSCC','001','CustomPermohonanFasilitasCeria|FCERIA','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','DTPE','DBTL','001','CustomSuratPengakuanHutang|SPH','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','DTPE','DAOF','001','CustomPermohonanFasilitasPinang|FPINANG','N','setup',sysdate,'setup',sysdate);

Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','SGMC','1800000|3000000','001','18-30 tahun','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','SGMC','3000000|5000000','001','30-50 tahun','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','SGMC','5000000|','001','50 tahun','N','setup',sysdate,'setup',sysdate);
commit;

INSERT INTO COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','STXT','HTP','001','Help Topics','N','setup',sysdate,'setup',sysdate);


--Modified values as per BPD Setup
DELETE FROM ECECUSER.COCD WHERE CODE_TYPE  in('SGMC','MST','EMPS','TYOW') AND BANK_ID = '01';

INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'SGMC', '1000000|3000000', '001', 'D', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'SGMC', '3000001|5000000', '001', 'C2', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'SGMC', '5000001|7000000', '001', 'C1', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'SGMC', '7000001|10000000', '001', 'B', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'SGMC', '10000001|15000000', '001', 'A2', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'SGMC', '15000001', '001', 'A1', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'MST', 'BELUM KAWIN', '001', 'BELUM KAWIN', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'MST', 'JANDA/DUDA', '001', 'JANDA/DUDA', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'MST', 'MENIKAH', '001', 'MENIKAH', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'MST', 'NS', '001', 'NOT SPECIFIED', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'EMPS', '001', '001', 'KARYAWAN', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'EMPS', '002', '001', 'PROFESIONAL', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'EMPS', '003', '001', 'PENSIUNAN', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'EMPS', '004', '001', 'TNI/POLRI', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'EMPS', '005', '001', 'WIRASWASTA', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'EMPS', '099', '001', 'LAIN-LAIN', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '001', '001', 'ACCOUNTING/FINANCE OFFICER', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '002', '001', 'CUSTOMER SERVICE', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '003', '001', 'ENGINEERING', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '004', '001', 'EKSEKUTIF', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '005', '001', 'ADMINISTRASI UMUM', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '006', '001', 'TEKNOLOGI INFORMASI', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '007', '001', 'KONSULTAN/ANALIS', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '008', '001', 'MARKETING', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '009', '001', 'PENGAJAR (GURU, DOSEN)', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '010', '001', 'MILITER', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '011', '001', 'PENSIUNAN', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '012', '001', 'PELAJAR/MAHASISWA', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '013', '001', 'WIRASWASTA', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '014', '001', 'POLISI', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '015', '001', 'PETANI', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '016', '001', 'NELAYAN', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '017', '001', 'PETERNAK', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '018', '001', 'DOKTER', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '019', '001', 'TENAGA MEDIS (PERAWAT, BIDAN, DAN SEBAGAINYA)', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '020', '001', 'HUKUM (PENGACARA, NOTARIS)', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '021', '001', 'PERHOTELAN AND RESTORAN (KOKI, BARTENDER, DAN SEBAGAINYA)', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '022', '001', 'PENELITI', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '023', '001', 'DESAINER', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '024', '001', 'ARSITEK', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '025', '001', 'PEKERJA SENI (ARTIS, MUSISI, PELUKIS, DAN SEBAGAINYA)', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '026', '001', 'PENGAMANAN', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '027', '001', 'PIALANG/BROKER', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '028', '001', 'DISTRIBUTOR', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '029', '001', 'TRANSPORTASI UDARA (PILOT, PRAMUGARI)', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '030', '001', 'TRANSPORTASI LAUT (NAHKODA, ANAK BUAH KAPAL)', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '031', '001', 'TRANSPORTASI DARAT (MASINIS, SOPIR, KONDEKTUR)', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '032', '001', 'BURUH (BURUH PABRIK, BURUH BANGUNAN, BURUH TANI)', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '033', '001', 'PERTUKANGAN DAN PENGRAJIN (TUKANG KAYU, PENGRAJIN KULIT, DAN LAIN-LAIN)', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '034', '001', 'IBU RUMAH TANGGA', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '035', '001', 'PEKERJA INFORMAL (ASISTEN RUMAH TANGGA, ASONGAN, DLL)', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '036', '001', 'PEJABAT NEGARA/PENYELENGGARA NEGARA', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '037', '001', 'PEGAWAI PEMERINTAHAN/LEMBAGA NEGARA (SELAIN PEJABAT/PENYELENGGARA NEGARA)', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
INSERT INTO ECECUSER.COCD (DB_TS, BANK_ID, CODE_TYPE, CM_CODE, LANG_ID, CD_DESC, DEL_FLG, R_MOD_ID, R_MOD_TIME, R_CRE_ID, R_CRE_TIME) VALUES (1, '01', 'TYOW', '099', '001', 'LAIN-LAIN', 'N', 'SETUP', SYSDATE, 'SETUP', SYSDATE);
COMMIT;

Insert into COCD values (1,'01','STXT','DTS','001','Privy Terms & Conditions','N','setup',sysdate,'setup',sysdate);
commit;


-- Religion common code
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RELI','BUD','001','Buddha','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RELI','HIN','001','Hindu','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RELI','ISL','001','Islam','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RELI','KAT','001','Katolik','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RELI','KRI','001','Kristen','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RELI','ZZZ','001','Lainnya','N','setup',sysdate,'setup',sysdate);

--Added for Loan Origination start---
delete from cocd where code_type='APS' and bank_id='01';
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','KTP_SAVED','001','KTP Document Saved','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','PER_SAVED','001','Personal Info Saved','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','CON_SAVED','001','Contact Info Saved','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','EMP_SAVED','001','Employment Info Saved','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','CR_SCORE_SUB','001','Credit Score Submitted','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','CR_SCORE_APR','001','Credit Score Approved','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','CR_SCORE_REJ','001','Credit Score Rejected','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','USR_REJECT','001','User Rejected','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','ACT_LINK_CONF','001','Activation Link confirmed','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','DISB_ACC_CONF','001','Disbursement Account Confirmed','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','EKYC_COM','001','EKYC Completed','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','DIG_SIGN_COM','001','Digital Signature Created','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','APP_EXPIRED','001','Application Expired','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','DOCUMENT_SIGNED','001','Document Signed ','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','LOAN_CREATED','001','Loan Disbursed','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','ALL','001','All','N','setup',sysdate,'setup',sysdate);

COMMIT;
--Added for Loan Origination end---

--Added for Education 
update COCD set CD_DESC='A. Sekolah Dasar' where  CODE_TYPE='EDU' and CM_CODE='SD';
update COCD set CD_DESC='B. Sekolah Menengah Pertama' where  CODE_TYPE='EDU' and CM_CODE='SMP';
update COCD set CD_DESC='C. Sekolah Menengah Atas' where  CODE_TYPE='EDU' and CM_CODE='SMA';
update COCD set CD_DESC='D. Diploma 1' where  CODE_TYPE='EDU' and CM_CODE='D1';
update COCD set CD_DESC='E. Diploma 2' where  CODE_TYPE='EDU' and CM_CODE='D2';
update COCD set CD_DESC='F. Diploma 3' where  CODE_TYPE='EDU' and CM_CODE='D3';
update COCD set CD_DESC='G. Strata 1' where  CODE_TYPE='EDU' and CM_CODE='S1';
update COCD set CD_DESC='H. Strata 1' where  CODE_TYPE='EDU' and CM_CODE='S1';
update COCD set CD_DESC='I. Strata 1' where  CODE_TYPE='EDU' and CM_CODE='S1';
COMMIT;
--Added for Education 

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','ASTG','LOAN_PAID','001','Loan Paid','N','setup',sysdate,'setup',sysdate);
update COCD set CM_CODE='P' , CD_DESC='Loan Paid' where CODE_TYPE='LTP' and CM_CODE='L' and bank_id='01';
COMMIT;

--Added missing seeds for DOC generation in UAT
SET DEFINE OFF;
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','DTPE','DSCC','001','CustomPermohonanFasilitasCeria|FCERIA','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','DTPE','DBTL','001','CustomSuratPengakuanHutang|SPH','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','DTPE','DAOF','001','CustomPermohonanFasilitasPinang|FPINANG','N','setup',sysdate,'setup',sysdate);



Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','ASTG','OCH_VER_FAIL','001','OCR Verification Failed 3','N','setup',
sysdate,'setup',sysdate);