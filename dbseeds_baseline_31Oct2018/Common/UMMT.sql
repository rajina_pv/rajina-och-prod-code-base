SET DEFINE OFF;
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME)
values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/loanproducts',
'GET','CLOANP',null,'N','SETUP',
sysdate,'SETUP',sysdate);
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','/v1/banks/{bankid}/custom/users','POST','ORONSE',null,'N','setup',sysdate,'setup',sysdate);
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,FIELDVALUE_MENUMAPPING,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/atminquiry','GET','CATMIN',null, EMPTY_CLOB(),'N','SETUP',sysdate,'SETUP',sysdate);

Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/applications','POST','CLACT',null,null,'SETUP',sysdate,'SETUP',sysdate);
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}','GET','CLACT',null,null,'SETUP',sysdate,'SETUP',sysdate);
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}','PUT','CLACT',null,null,'SETUP',sysdate,'SETUP',sysdate);
commit;

--standardText Changes by pratik
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','/v1/banks/{bankid}/custom/standard-text','GET','CSTXT',null,'N','SETUP',to_date('20-AUG-18 10:28:28','DD-MON-RR HH24:MI:SS'),'SETUP',to_date('20-AUG-18 10:28:28','DD-MON-RR HH24:MI:SS'));

--contentManagement 
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME)
values (1,'01','/v1/banks/{bankid}/custom/contents',
'GET','CCONMG',null,'N','SETUP',
sysdate,'SETUP',sysdate);

--PROTIK / Credit Score - 18/09/2018
REM INSERTING into UMMT
SET DEFINE OFF;
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,FIELDVALUE_MENUMAPPING,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','v1/banks/{bankId}/custom/applications/{appId}/creditscoreupdate','GET','CRSCOR',null, EMPTY_CLOB(),'N','SETUP',sysdate,'SETUP',sysdate);


SET DEFINE OFF;
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,FIELDVALUE_MENUMAPPING,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/companydetails','POST','CCMPD',null, EMPTY_CLOB(),null,'SETUP',to_date('10-SEP-18','DD-MON-RR'),'SETUP',to_date('10-SEP-18','DD-MON-RR'));
COMMIT;

--LoanSimulation
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME)
values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/loansimulation',
'GET','CLNSIM',null,'N','SETUP',
sysdate,'SETUP',sysdate);

--Added for loan application list inquiry by Ambili on 20-09-2018 : start
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/applicationInquiry','GET','CLAIT',null,null,'SETUP',sysdate,'SETUP',sysdate);
--Added for loan application list inquiry by Ambili on 20-09-2018 : end

--Added for OTP Generate by Prasad 20 Sep 2018----
insert into UMMT values(1,'01','/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}/generateOtp','GET','COTP',NULL,NULL,NULL,'SETUP',sysdate,'SETUP',sysdate);
--Added for OTP Generate by Prasad 20 Sep 2018----

--Added for OTP Verify by Prasad 20 Sep 2018----
insert into UMMT values(1,'01','/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}/verifyOTP','POST','CVOTP',NULL,NULL,NULL,'SETUP',sysdate,'SETUP',sysdate);
--Added for OTP Verify by Prasad 20 Sep 2018----

--Vidhi 24Sept Loan Bal Inquiry start
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/loans','GET','LONBAL',null,'N','setup',sysdate,'setup',sysdate);

--Vidhi 24Sept Loan Bal Inquiry end


--Vidhi 26Sept Standard text start
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/stmt','GET','STMINQ',null,'N','setup',sysdate,'setup',sysdate);

--Vidhi 26Sept standard text end

--PRATIK_SHAH07 ChangePassword 26-08-2018

Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME)
values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/password',
'PUT','FCPWD',null,'N','SETUP',
sysdate,'SETUP',sysdate);

--Vidhi 27Sept Photo Maintenance
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}/valdiatePhoto','POST','PHOVAL',null,'N','setup',sysdate,'setup',sysdate);

--PRatik for UserProfile changes
DELETE FROM UMMT WHERE BANK_ID='01' and URI='/v1/banks/{bankid}/users/{userid}/custom/userprofile';
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME)
values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/userprofile',
'GET','RCPL',null,'N','SETUP',
sysdate,'SETUP',sysdate);

--Pratik_shah07 for Notification Status update API
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME)
values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/notifications/{notificationId}',
'PUT','CNSUP',null,'N','SETUP',
sysdate,'SETUP',sysdate);
COMMIT;

--Added by Ambili to correct UMMT records
delete from ummt where default_menu_id in ('CLACT','CCMPD','CLAIT','COTP','CVOTP');
commit;
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/applications','POST','CLACT',null,'N','SETUP',sysdate,'SETUP',sysdate);
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}','GET','CLACT',null,'N','SETUP',sysdate,'SETUP',sysdate);
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}','PUT','CLACT',null,'N','SETUP',sysdate,'SETUP',sysdate);
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,FIELDVALUE_MENUMAPPING,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/companydetails','POST','CCMPD',null, EMPTY_CLOB(),'N','SETUP',sysdate,'SETUP',sysdate);
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/applicationInquiry','GET','CLAIT',null,'N','SETUP',sysdate,'SETUP',sysdate);
insert into UMMT values(1,'01','/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}/generateOtp','GET','COTP',NULL,NULL,'N','SETUP',sysdate,'SETUP',sysdate);
insert into UMMT values(1,'01','/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}/verifyOTP','POST','CVOTP',NULL,NULL,'N','SETUP',sysdate,'SETUP',sysdate);
commit;
--Added by Ambili to correct UMMT records

----ADDED BY PRASAD FOR LOAN REPAYMENT SCHEDULE FETCH 04 oct-----
SET DEFINE OFF;
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,FIELDVALUE_MENUMAPPING,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/loans/{accountId}','GET','CFLD',null, EMPTY_CLOB(),'N','SETUP',sysdate,'SETUP',sysdate);
commit;
----ADDED BY PRASAD FOR LOAN REPAYMENT SCHEDULE FETCH-----

----Added by sahana---start 05 Oct
DELETE FROM UMMT WHERE BANK_ID='01' and URI='/v1/banks/{bankid}/users/{userid}/custom/deviceregistration';
Insert into UMMT ("db_ts","bank_id","uri","method","default_menu_id","input_field_name","fieldvalue_menumapping","del_flg","r_cre_id","r_cre_time","r_mod_id","r_mod_time") values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/deviceregistration','PUT','DEVREG',null,null,'N','setup',sysdate,'setup',sysdate);

DELETE FROM UMMT WHERE BANK_ID='01' and URI='/v1/banks/{bankid}/users/{userid}/custom/deviceregistration/updateHpNumber';
Insert into UMMT ("db_ts","bank_id","uri","method","default_menu_id","input_field_name","fieldvalue_menumapping","del_flg","r_cre_id","r_cre_time","r_mod_id","r_mod_time") values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/deviceregistration/updateHpNumber','PUT','DEVUSR',null,null,'N','setup',sysdate,'setup',sysdate);
----Added by sahana---end 05 Oct

---Added by vidhi 05 oct Loan History start
DELETE FROM UMMT WHERE BANK_ID='01' and URI='/v1/banks/{bankid}/users/{userid}/custom/loanHist/{accountId}';
insert into UMMT values(1,'01','/v1/banks/{bankid}/users/{userid}/custom/loanHist/{accountId}','GET','LNHIST',NULL,NULL,'N','SETUP',sysdate,'SETUP',sysdate);
---Added by vidhi 05 oct Loan History end

--Added by Ambili for loan Payment SR:: start
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/servicerequests/loan-payment','POST','CLNPR',null,'N','SETUP',sysdate,'SETUP',sysdate);
--Added by Ambili for loan Payment SR:: end

INSERT INTO UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,FIELDVALUE_MENUMAPPING,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) 
VALUES (1,'01','/v1/banks/{bankid}/users/{userid}/custom/applications/document-list','GET','CLADF',NULL,NULL,'N','SETUP',SYSDATE,'SETUP',SYSDATE);

--Added by Vidhi for loan creation 9 Oct :: start
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}/createloan','PUT','CRELON',null,'N','setup',sysdate,'setup',sysdate);
--Added by Vidhi for loan creation 9 Oct:: end

DELETE FROM UMMT WHERE DEFAULT_MENU_ID='STMINQ' AND METHOD='GET';
INSERT INTO UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) VALUES (1,'01','/v1/banks/{bankid}/users/{userid}/custom/helptopics','GET','STMINQ',NULL,'N','setup',SYSDATE,'setup',SYSDATE);


Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME)
values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/servicerequests/make-purchase',
'POST','CMPAY',null,'N','SETUP',
sysdate,'SETUP',sysdate);
COMMIT;

update UMMT set method='POST',uri='/v1/banks/{bankId}/custom/applications/{appId}/creditscoreupdate' where uri = 'v1/banks/{bankId}/custom/applications/{appId}/creditscoreupdate';

--Added by sahana for forgot password 15 Oct :: start
delete from ummt where default_menu_id='FORPWD' and bank_id='01' and uri='/v1/banks/{bankid}/custom/users/forgot-password/reset';
Insert into UMMT ("db_ts","bank_id","uri","method","default_menu_id","input_field_name","fieldvalue_menumapping","del_flg","r_cre_id","r_cre_time","r_mod_id","r_mod_time") values (1,'01','/v1/banks/{bankid}/custom/users/forgot-password/reset','POST','FORPWD',null,null,'N','setup',sysdate,'setup',sysdate);
--Added by sahana for forgot password 15 Oct :: end
COMMIT;

--Added by Prasad for Loan Balance Details 16 OCt---
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,FIELDVALUE_MENUMAPPING,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/loanaccounts','GET','CALD',null, null,'N','SETUP',sysdate,'SETUP',sysdate);
COMMIT;
--Added by Prasad for Loan Balance Details 16 OCt---

--Added for limit inquiry
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,FIELDVALUE_MENUMAPPING,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/limitInquiry','GET','CLIDT',null, null,'N','SETUP',sysdate,'SETUP',sysdate);
--Added for limit inquiry

--Added for updating method from GET to POST
Delete from UMMT where DEFAULT_MENU_ID = 'CATMIN';
Insert into UMMT (DB_TS,BANK_ID,URI,METHOD,DEFAULT_MENU_ID,INPUT_FIELD_NAME,FIELDVALUE_MENUMAPPING,DEL_FLG,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME) values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/atminquiry','POST','CATMIN',null, EMPTY_CLOB(),'N','SETUP',sysdate,'SETUP',sysdate);
--Added for updating method from GET to POST

--For merchant refund
Insert into UMMT values (1,'01','/v1/banks/{bankid}/custom/servicerequests/make-refund','POST','CMREF',null,null,'N','SETUP',sysdate,'SETUP',sysdate);
--For merchant refund