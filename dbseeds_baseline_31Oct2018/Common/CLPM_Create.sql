create table CUSTOM_LOAN_PROD_MASTER_TABLE
(
    DB_TS number(5,0),
	BANK_ID nvarchar2(11) NOT NULL, 
	MERCHANT_ID  nvarchar2(4),
    PRODUCT_CODE  nvarchar2(4),
    PRODUCT_CATEGORY  nvarchar2(4),
    PRODUCT_SUBCATEGORY  nvarchar2(56),
    CONFIG_TYPE  nvarchar2(4),
    MIN_AMOUNT  number(18,3),
    MAX_AMOUNT  number(18,3),
    LN_REC_ID  nvarchar2(32),
    LN_PURPOSE  nvarchar2(512),
	DEL_FLG CHAR(1), 
	R_MOD_ID NVARCHAR2(65), 
	R_MOD_TIME DATE, 
	R_CRE_ID NVARCHAR2(65), 
	R_CRE_TIME DATE
) 
TABLESPACE MASTER
/

create synonym CLPM for CUSTOM_LOAN_PROD_MASTER_TABLE;
/

create unique index IDX_CLPM
on CUSTOM_LOAN_PROD_MASTER_TABLE(LN_REC_ID)
TABLESPACE IDX_MASTER
/
create sequence NEXT_LN_REC_ID01 increment by 1;