---Added by sahana -- start 05 Oct
delete from vmot where mnu_id='DEVREG' and bank_id='01';
Insert into VMOT ("db_ts","bank_id","del_flg","mnu_id","fg_link_url","reg_link_url","promo_link_url","promotion_flag","user_acceptance_required","ac_type","mnu_txt","additional_mnu_txt","menu_type","type_of_activity","rm_plus_enabled","highlight_flag","highlight_date","highlight_text","product_code","r_cre_id","r_cre_time","r_mod_id","r_mod_time","is_secure_menu_id","auth_reqd_on_entry","adaptive_auth_reqd") values (1,'01','N','DEVREG','FORMSGROUP_ID=CustomDeviceRegistrationRetailResource',null,null,'N','N','ALL|','Device registration',null,'W','NFIN','Y',null,null,null,null,'setup',sysdate,'setup',sysdate,null,null,null);


delete from vmot where mnu_id='DEVUSR' and bank_id='01';
Insert into VMOT ("db_ts","bank_id","del_flg","mnu_id","fg_link_url","reg_link_url","promo_link_url","promotion_flag","user_acceptance_required","ac_type","mnu_txt","additional_mnu_txt","menu_type","type_of_activity","rm_plus_enabled","highlight_flag","highlight_date","highlight_text","product_code","r_cre_id","r_cre_time","r_mod_id","r_mod_time","is_secure_menu_id","auth_reqd_on_entry","adaptive_auth_reqd") values (1,'01','N','DEVUSR','FORMSGROUP_ID=CustomDeviceRegUpdateHPNumberResource',null,null,'N','N','ALL|','Device registration, update HP Number',null,'W','NFIN','Y',null,null,null,null,'setup',sysdate,'setup',sysdate,null,null,null);
---Added by sahana -- end 05 Oct

---Added by vidhi 05 oct Loan History start
delete from vmot where mnu_id='LNHIST' and bank_id='01';
Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,
AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,
R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD)
values (1,'01','N','LNHIST','FORMSGROUP_ID=CustomFetchLoanHistoryResource',null,null,'N','N','ALL|',
'Custom Loan Fetch Loan History',null,'W','NFIN','Y',null,null,null,'SETUP',
sysdate,'SETUP',sysdate,null,null,null);
---Added by vidhi 05 oct Loan History end