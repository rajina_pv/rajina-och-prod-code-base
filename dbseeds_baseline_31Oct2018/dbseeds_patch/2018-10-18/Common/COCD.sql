Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','KTP_SAVED','001','KTP Document Saved','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','PER_SAVED','001','Personal Info Saved','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','CON_SAVED','001','Contact Info Saved','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','EMP_SAVED','001','Employment Info Saved','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','CR_SCORE_SUB','001','Credit Score Submitted','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','CR_SCORE_APR','001','Credit Score Approved','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','CR_SCORE_REJ','001','Credit Score Rejected','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','USR_REJECT','001','User Rejected','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','ACT_LINK_CONF','001','Activation Link confirmed','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','DISB_ACC_CONF','001','Disbursement Account Confirmed','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','EKYC_COM','001','EKYC Completed','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','DIG_SIGN_COM','001','Digital Signature Created','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','APP_EXPIRED','001','Application Expired','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','DOCUMENT_SIGNED','001','Document Signed ','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','LOAN_CREATED','001','Loan Disbursed','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME)
values (1,'01','APS','ALL','001','All','N','setup',sysdate,'setup',sysdate);

COMMIT;