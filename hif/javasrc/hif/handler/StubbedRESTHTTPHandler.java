package com.infosys.fentbase.hif.channel.rest;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import com.infosys.feba.framework.common.ErrorCodes;
import com.infosys.feba.framework.common.FEBAConstants;
import com.infosys.feba.framework.common.FEBAIncidenceCodes;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.DateUtil;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.hif.HIFUtil;
import com.infosys.feba.framework.hif.context.HostContext;
import com.infosys.feba.framework.hif.core.IChannelHandler;
import com.infosys.feba.framework.hif.core.exception.ChannelHandlerException;
import com.infosys.feba.framework.hif.model.IHostMessage;
import com.infosys.feba.framework.hif.model.impl.HostMessageImpl;
import com.infosys.feba.framework.monitor.MonitoringConstants;
import com.infosys.feba.framework.monitor.MonitoringContext;
import com.infosys.feba.framework.types.FEBAHashMap;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.utils.StringByteConverter;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.types.TypesCatalogueConstants;
import com.infosys.fentbase.types.valueobjects.MonitoringVO;

/**
 * This is the channel handler to make stubbed rest calls with host name
 * configured as STUBBEDJSON.
 * 
 * @author Kavipriya_E
 *
 */

public class StubbedRESTHTTPHandler implements IChannelHandler {

	public static final String METHOD_POST = "POST";
	public static final String METHOD_GET = "GET";
	public static final String METHOD_PUT = "PUT";
	public static final String METHOD_DELETE = "DELETE";
	public static final String METHOD_OPTIONS = "OPTIONS";
	public static final String METHOD_HEAD = "HEAD";
	private static final String METHOD_TRACE = "TRACE";
	private static final String EMPTY = "";
	private static ConcurrentHashMap<String, String> responseMap = new ConcurrentHashMap<String, String>();

	private static boolean userSpecificResponse = !FEBAConstants.FLAG_N
			.equalsIgnoreCase(PropertyUtil.getProperty("USER_SPECIFIC_STUB_RESPONSE_REQUIRED", null));
	private Properties properties;

	public void init(Properties pProperties) {
		this.properties = pProperties;
	}

	public IHostMessage transceive(HostContext pContext, IHostMessage pHostMessage) throws ChannelHandlerException {
		String userId = null;
		String corpId = null;
		

		HostMessageImpl responseMessage = new HostMessageImpl();

		// getting the various parameters, message from the framework
		String messageName = pHostMessage.getMessageName();
		// getting the http method configured in HIF_Message.xml
		String httpMethod = pHostMessage.getHostMessageConfig().getHttpMethod(); 
		Map<String,String> reqHeaders = new HashMap<String,String>();
		//reqHeaders = pHostMessage.getRequestHeaders();
		reqHeaders.put("clientTraceId", getReqUUID());
		System.out.println("stubb req header::"+reqHeaders);
		pHostMessage.setRequestHeaders(reqHeaders);

		FBATransactionContext context = (FBATransactionContext) pContext.getFEBATransactionContext();
		MonitoringVO monitoringVo = null;
		String isHostMonitoringRequired = null;
		FEBAHashMap monitoringMap = null;
		MonitoringContext monitoringContext = null;
		String responseString = "";
		//pHostMessage.getRequestHeaders().put("clientTraceId", getReqUUID());

		try {

			isHostMonitoringRequired = PropertyUtil.getProperty(MonitoringConstants.IS_MONITORING_ENABLED, context);

			if (null != isHostMonitoringRequired && "Y".equalsIgnoreCase(isHostMonitoringRequired)) {
				try {
					monitoringContext = (MonitoringContext) ((HostContext) pContext)
							.getFromHostContext(FBAConstants.MONITORING_CONTEXT);
					monitoringMap = monitoringContext.getMonitoringMap();
					monitoringVo = requestMonitoring(context, monitoringMap, messageName);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if ((context.getActionId() != null) && (context.getActionId().toString().contains("DBAuthentication")
					|| context.getActionId().toString().contains("DBRegistration")
					|| (context.getDBApplicationId() != null
							&& context.getDBApplicationId().toString().equalsIgnoreCase("")
							&& context.getActionId().toString().contains("PostalAddressFinderFG")))) {
				userId = "DBUSER";
				corpId = "DBUSER";
			} else {
				userId = context.getUserId().getValue();
				corpId = context.getCorpId().getValue();
			}

			// invokes different methods of JSONRestClient based on the http method
			// configured in HIF_Message.xml(transformer entry)
			if (httpMethod == null || httpMethod.equals(EMPTY)) {
				throw new CriticalException(context, FEBAIncidenceCodes.REST_HTTP_METHOD_NOT_CONFIGURED,
						ErrorCodes.REST_HTTP_METHOD_NOT_CONFIGURED);
			} else {
				String sRequestJSON = (String) pHostMessage.getMessage();
				
				
				BufferedReader br = null;
				FileOutputStream fos = null;
				try {
					if(null != sRequestJSON){
					// write request data to output file
					String requestFileName = getRequestFilename(context, corpId, userId, pHostMessage);
					fos = new FileOutputStream(new File(requestFileName));
					try{
						byte[] bArray = StringByteConverter.convertStringtoByteArray(sRequestJSON,
								FEBAConstants.CHARACTER_SET);
						fos.write(bArray);

						LogManager.logDebug(pContext, "StubbedFICallHandler : Flushing Stream");

						fos.flush();
					}
					catch(NullPointerException e){
						LogManager.logDebug(pContext, "StubbedFICallHandler : Nothing to read");
					}
						
					}

					// Stub Optimization END

					// read response from external file
					String tempStr = "";

					String responseFileName = getResponseFileName(corpId, userId, pHostMessage);

					String response = responseMap.get(pHostMessage.getMessageName());
					if (response == null || userSpecificResponse) {
						FileInputStream fis = new FileInputStream(responseFileName);
						// Get the object of DataInputStream
						DataInputStream in = new DataInputStream(fis);
						br = new BufferedReader(new InputStreamReader(in, "UTF-8"));

						LogManager.logDebug(pContext, "StubbedFICallHandler : Opened input stream");

						// Read File Line By Line
						while ((tempStr = br.readLine()) != null) {
							responseString += tempStr + "\n";
						}

						LogManager.logDebug(pContext, "StubbedFICallHandler : Response data read from external file.");
						responseMap.put(pHostMessage.getMessageName(), responseString);

					}

				} catch (IOException ioe) {
					StringBuilder hostName = new StringBuilder();
					StringBuilder routeName = new StringBuilder();
					HIFUtil.getHostConfig(hostName, routeName, pHostMessage);

					LogManager.logDebug(pContext, "StubbedFICallHandler : Error in Stubbing : " + ioe.getMessage());
					throw new ChannelHandlerException(pContext.getFEBATransactionContext(),
							FEBAIncidenceCodes.HTTP_TRANSPORT_FAILED,
							"Http transport failed for message[" + pHostMessage.getMessageName() + "]. Host[" + hostName
									+ "] , Route[" + routeName + "] , Properties[" + properties
									+ "]. Please check the configuration properties. ",
							ErrorCodes.HTTP_TRANSPORT_FAILED, ioe);
					// Fortify Fix : Unreleased Resource - Streams
				} finally {
					if (fos != null) {
						try {
							fos.close();
						} catch (IOException e) {
							LogManager.logError(context, e);
						}
					}
					if (br != null) {
						try {
							br.close();
						} catch (IOException e) {
							LogManager.logError(context, e);
						}
					}
					// Fortify Fix : END
				}
				// Start Coding specific to Monitoring Tool
				if (null != isHostMonitoringRequired && "Y".equalsIgnoreCase(isHostMonitoringRequired)) {
					if (monitoringVo != null) {
						monitoringVo.setResponseTime(new FEBADate(DateUtil.currentDate(context)));
						monitoringMap.put(messageName, monitoringVo);
					}

				}
				// End Coding specific to Monitoring Tool

				// setting the response string in response message
				responseMessage.setMessage(responseString);
				//responseMessage.setResponseStatus(200);
				//pHostMessage.setMessage(responseString);
				pHostMessage.setMessage(200);
				 Map<String,String> respHeaders = new HashMap<String,String>();
				 respHeaders.put("Access-Control-Allow-Methods", "[GET, POST, OPTIONS]");
				 respHeaders.put("Content-Type", "[application/json;charset=utf-8]");
				 pHostMessage.setResponseHeaders(respHeaders);
				System.out.println("responseMessage=" + responseMessage);
			}

		} catch (CriticalException ce) {
			// Start Coding specific to Monitoring Tool
			if (null != isHostMonitoringRequired && "Y".equalsIgnoreCase(isHostMonitoringRequired)) {
				if (monitoringVo != null) {
					monitoringVo.setResponseTime(new FEBADate(DateUtil.currentDate(context)));
					monitoringVo.setStatus("FAILURE");
					monitoringVo.setEbFailureCode(Integer.toString(FBAErrorCodes.HOST_CALL_TIMEOUT));
					monitoringMap.put(messageName, monitoringVo);
				}
			}
			// End Coding specific to Monitoring Tool
			throw new ChannelHandlerException(pContext.getFEBATransactionContext(),
					FEBAIncidenceCodes.HTTP_TRANSPORT_FAILED,
					"Rest api invocation failed for message[" + pHostMessage.getMessageName() + "]",
					ErrorCodes.HTTP_TRANSPORT_FAILED, ce);
		}

		return responseMessage;
	}

	/**
	 * This method capture user type, corp id and request time.
	 * 
	 * @author Kavipriya_E
	 * @param context
	 * @param monitoringMap
	 * @param sMessageName
	 * @return
	 */
	private MonitoringVO requestMonitoring(FBATransactionContext context, FEBAHashMap monitoringMap,
			String sMessageName) {
		MonitoringVO monitoringVo;
		if (monitoringMap.containsKey(sMessageName)) {
			monitoringVo = (MonitoringVO) monitoringMap.get(sMessageName);
		} else {
			monitoringVo = (MonitoringVO) FEBAAVOFactory.createInstance(TypesCatalogueConstants.MonitoringVO);
		}
		if (monitoringVo != null) {
			monitoringVo.setRequestTime(new FEBADate(DateUtil.currentDate(context)));
			if (null != context.getRecordUserId() && context.getRecordUserId().toString() != "") {
				monitoringVo.setUserID(context.getRecordUserId().toString());
			}
			if (null != context.getUserType()) {
				monitoringVo.setUserType(context.getUserType());
			}
		}
		return monitoringVo;
	}

	public Properties getProperties() {
		return this.properties;
	}

	/**
	 * 
	 * Returns the response file name which is to be read for response <BR>
	 * 
	 * @param userId
	 * @param pHostMessage
	 * @return
	 * @author Kavipriya_E
	 */
	private String getResponseFileName(String corpId, String userId, IHostMessage pHostMessage) {

		String responsePath = properties.getProperty(FBAConstants.PROPERTY_RESPONSE_PATH).concat(File.separator)
				.concat(corpId).concat(File.separator).concat(userId);
		responsePath = responsePath.replace('/', File.separatorChar);

		String responseFileName = pHostMessage.getMessageName().concat(FBAConstants.DOT).concat("json");

		return responsePath.concat(File.separator).concat(responseFileName);
	}

	/**
	 * 
	 * Returns the request file name <BR>
	 * 
	 * @param userId
	 * @param pHostMessage
	 * @return
	 * @author Kavipriya_E
	 */
	private String getRequestFilename(FBATransactionContext context, String corpId, String userId,
			IHostMessage pHostMessage) {

		String requestPath = properties.getProperty(FBAConstants.PROPERTY_REQUEST_PATH).concat(File.separator)
				.concat(corpId).concat(File.separator).concat(userId);
		requestPath = requestPath.replace('/', File.separatorChar);
		File requestPathFile = new File(requestPath);
		if (!(requestPathFile.exists())) {
			requestPathFile.mkdirs();
		}
		String requestFileName = pHostMessage.getMessageName().concat(FBAConstants.UNDERSCORE)
				.concat(DateUtil.currentDate(context).toString()).concat(FBAConstants.DOT).concat("json");
		requestFileName = requestFileName.replace(':', '_');

		return requestPath.concat(File.separator).concat(requestFileName);
	}
	/*
	 * This will generate the unque id like below.
	 */
	public static String getReqUUID(){
		
		 
		   UUID uid = UUID.fromString("4064464E-3256-4D19-9B26-0F40F0152DB7");     
		        
		   // checking the value of random UUID
		   return uid.randomUUID().toString().toUpperCase();
		
	}
	

}
