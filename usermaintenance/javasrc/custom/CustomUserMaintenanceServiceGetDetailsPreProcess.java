package com.infosys.custom.ebanking.usermaintenance.custom;

import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.feba.framework.common.IContext;
import com.infosys.feba.framework.common.ICustomHook;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.valueobjects.FEBATMOCriteria;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.common.util.MaintenanceReqdUtility;
import com.infosys.fentbase.tao.CUSRTAO;
import com.infosys.fentbase.tao.info.CUSRInfo;
import com.infosys.fentbase.types.valueobjects.UserMaintenanceDetailsVO;

public class CustomUserMaintenanceServiceGetDetailsPreProcess implements ICustomHook {

	@Override
	public void execute(IContext objContext, Object objInputOutput)
			throws BusinessException, BusinessConfirmation, CriticalException {

		final UserMaintenanceDetailsVO userMaintenanceDetailsVO = (UserMaintenanceDetailsVO) objInputOutput;
		FEBATransactionContext febaTxnContext = (FEBATransactionContext) objContext;
		FEBATMOCriteria criteria = new FEBATMOCriteria();
		criteria.setMaintenanceRequired(MaintenanceReqdUtility.getMaintenanceReqd(febaTxnContext));
		criteria.setDependencyFlag(FBAConstants.NO);
		CUSRInfo cusrInfo = new CUSRInfo();
		FBATransactionContext ebContext = (FBATransactionContext) objContext;

		try {
			cusrInfo = CUSRTAO.select(febaTxnContext, ebContext.getBankId(),
					userMaintenanceDetailsVO.getOrgIdER().getOrgId(), userMaintenanceDetailsVO.getUserId());
			System.out.println("cusrInfo.getCustId()"+cusrInfo.getCustId());
			if (!FEBATypesUtility.isNotNullOrBlank(cusrInfo.getCustId())) {
				throw new BusinessException(true, objContext, "CUSER00011", null, "Customer Id",
						CustomEBankingErrorCodes.CIF_ID_NOT_AVAILABLE, null, (AdditionalParam) null);
			}

		} catch (FEBATableOperatorException tblOperExp) {
			throw new CriticalException(objContext, FBAIncidenceCodes.TAO_EXP_USER_FETCH_READ, tblOperExp.getMessage(),
					tblOperExp.getErrorCode());
		}

	}
}
