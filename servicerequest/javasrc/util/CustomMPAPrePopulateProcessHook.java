package com.infosys.custom.ebanking.servicerequest.util;

import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.IContext;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.service.LocalServiceUtil;
import com.infosys.feba.framework.types.lists.FEBAHashList;
import com.infosys.feba.framework.types.lists.FEBAListIterator;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.fentbase.common.util.FormManagementVOAccessorUtil;
import com.infosys.fentbase.framework.formmanagement.common.IFMProcessHook;
import com.infosys.fentbase.types.valueobjects.FormManagementVO;
import com.infosys.fentbase.types.valueobjects.NameValuePairVO;
import com.infosys.fentbase.types.valueobjects.SectionDataVO;

public class CustomMPAPrePopulateProcessHook implements IFMProcessHook
{

	@Override
	public void execute(IContext context, Object objInputOutput) throws BusinessException,
	BusinessConfirmation, CriticalException {
		FEBATransactionContext objContext = (FEBATransactionContext)context;
		FormManagementVO formManagementVO=(FormManagementVO) objInputOutput;
		FEBAUnboundInt index = null;
		NameValuePairVO nameValuePairVO = (NameValuePairVO)FEBAAVOFactory.createInstance(NameValuePairVO.class.getName());;
		
		nameValuePairVO = (NameValuePairVO) FormManagementVOAccessorUtil
		.getFormFieldValue(objContext, formManagementVO, "PAYMENT_MODE", index);
		FEBAUnboundString paymentMode = (FEBAUnboundString)nameValuePairVO.getFormFieldValue();
		if(paymentMode.toString()!="REFUND"){
			CustomLoanApplicationEnquiryVO enquiryVO = (CustomLoanApplicationEnquiryVO) FEBAAVOFactory.createInstance(CustomLoanApplicationEnquiryVO.class.getName());
		
			// invoke the service to create transaction entries
			LocalServiceUtil.invokeService(objContext,"CustomLoanApplicationService", new FEBAUnboundString("fetchForInquiry"), enquiryVO);
        
		if(enquiryVO.getResultList().size()==0){
			throw new BusinessException(objContext, CustomEBankingIncidenceCodes.NO_RECORDS_FETCHED, 
					"", EBankingErrorCodes.NO_RECORDS_FOUND);
		}
		
		CustomLoanApplnMasterDetailsVO detailsVO = (CustomLoanApplnMasterDetailsVO)enquiryVO.getResultList().get(0);
		
			
	 		
			nameValuePairVO = (NameValuePairVO) FormManagementVOAccessorUtil
					.getFormFieldValue(objContext, formManagementVO, "LOAN_ACNT_ID", index);
			nameValuePairVO.setFormFieldValue(detailsVO.getLoanAccountId());
		
		}
	}

}
