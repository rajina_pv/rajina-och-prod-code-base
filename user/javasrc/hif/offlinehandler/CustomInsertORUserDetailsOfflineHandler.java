/*

 * InsertAppDetailsOfflineHandler.java

 * @since Aug 31, 2009 - 12:30:57 PM

 *

 * COPYRIGHT NOTICE:

 * Copyright (c) 2007 Infosys Technologies Limited, Electronic City,

 * Hosur Road, Bangalore - 560 100, India.

 * All Rights Reserved.

 * This software is the confidential and proprietary information of

 * Infosys Technologies Ltd. ("Confidential Information"). You shall

 * not disclose such Confidential Information and shall use it only

 * in accordance with the terms of the license agreement you entered

 * into with Infosys.

 */

package com.infosys.custom.ebanking.user.hif.offlinehandler;

import com.infosys.custom.ebanking.user.service.CustomORUserCreationUtility;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.types.TypesCatalogueConstants;
import com.infosys.ebanking.types.valueobjects.OnlineRegDetailsVO;
import com.infosys.ebanking.types.valueobjects.OnlineRegEnquiryVO;
import com.infosys.ebanking.user.EBLoginAltFlowConstants;
import com.infosys.ebanking.user.service.ORUserCreationUtility;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.common.util.SequenceGeneratorUtil;
import com.infosys.feba.framework.common.util.resource.FilePropertyManager;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.transaction.util.ValidationsUtil;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.primitives.CorporateId;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.FreeTextSmall;
import com.infosys.feba.framework.types.primitives.NumberOfAttempts;
import com.infosys.feba.framework.types.primitives.Status;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.primitives.YNFlag;
import com.infosys.feba.framework.types.valueobjects.BusinessInfoVO;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValEngineConstants;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.BlobArrayObjectConversionUtil;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.tao.IUSRTAO;
import com.infosys.fentbase.tao.LAFRTAO;
import com.infosys.fentbase.tao.info.LAFRInfo;
import com.infosys.fentbase.types.primitives.IndividualUserId;
import com.infosys.fentbase.types.valueobjects.DBSectionVO;
import com.infosys.fentbase.types.valueobjects.ILoginAltFlowVO;
import com.infosys.fentbase.types.valueobjects.PasswordVO;
import com.infosys.fentbase.types.valueobjects.UserMaintenanceDetailsVO;
import com.infosys.fentbase.user.LoginAltFlowConstants;
import com.infosys.fentbase.user.validators.PasswordChangeVal;
import com.infosys.fentbase.user.validators.PasswordPropertyVal;
import com.infosys.fentbase.usermaintenance.util.UserMaintenanceHostInvoker;

/**
 *
 * This class has methods to insert the details of the applicant in ORRT and
 * LGIN table
 *
 * UserID is created only if RMApproval is not required and the record is
 * inserted in CUSR and BRCM table
 *
 * @author Renita_Pinto
 *
 * @version 1.0
 *
 * @since FEBA 2.0
 *
 */

public class CustomInsertORUserDetailsOfflineHandler extends AbstractLocalUpdateTran {

	private static final String NXT_IUSR_SRL_NO = "NXT_IUSR_SRL_NO";
	private static final String DDMMYYYY= "dd-MM-yyyy";
	@Override
	public FEBAValItem[] prepareValidationsList(
			FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException,
			BusinessConfirmation, CriticalException {
		  return new FEBAValItem[0];
	}

	/**
	 *
	 * This method has calls to appropriate methods of ORUserIDPwdUtility class
	 * to insert the applicant details into ORRT,CUSR,BRCM and LGIN table
	 *
	 * @param objContext
	 * @param objInputOutput
	 * @param objTxnWM
	 * @throws BusinessException
	 * @throws BusinessConfirmation
	 * @throws CriticalException
	 *
	 * @author Renita_Pinto
	 *
	 * @see com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran#process(com.infosys.feba.framework.commontran.context.FEBATransactionContext,
	 *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject,
	 *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject)
	 *
	 * @since Sep 8, 2009 - 2:27:13 PM
	 *
	 */
	@Override
	public void process(FEBATransactionContext objContext,
			IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
			throws BusinessException, BusinessConfirmation, CriticalException {

		EBTransactionContext ebContext = (EBTransactionContext) objContext;
		final ILoginAltFlowVO loginAltFlowVO = (ILoginAltFlowVO) objInputOutput;
		


	 LAFRInfo lafrInfo=null;

	     try {

	    	  lafrInfo=LAFRTAO.select(ebContext, ebContext.getBankId(), ebContext.getRequestId());

		} catch (FEBATableOperatorException e) {
				throw new BusinessException(objContext,
				                    EBIncidenceCodes.NO_RECORDS_FOUND_FPDT,"LAFR fetch failed",
                    EBankingErrorCodes.NO_RECORDS_FOUND);
		}

		byte[] bytes = lafrInfo.getRequestDetails().getValue();
		Object object = BlobArrayObjectConversionUtil.toObject(bytes);
		DBSectionVO  loginAltFlowSectionVO = (DBSectionVO) object;

		loginAltFlowVO.getForgotPasswordOfflineVO().setFpSectionVO(loginAltFlowSectionVO);



		insertApplicantDetails(objContext, loginAltFlowVO,lafrInfo);


	}



	/**
	 *
	 * 
	 *
	 * @param objContext
	 * @param rmApprovalReq
	 * @param hashList
	 * @param requestID
	 * @param lafrInfo
	 * @throws BusinessException
	 * @throws BusinessConfirmation
	 * @throws CriticalException
	 * @author Renita_Pinto
	 * @since Aug 01, 2012 - 4:10:21 PM
	 */
	private void insertApplicantDetails(FEBATransactionContext objContext,
			ILoginAltFlowVO loginAltFlowVO,
			LAFRInfo lafrInfo) throws BusinessException, BusinessConfirmation,
			CriticalException {

		EBTransactionContext ebContext = (EBTransactionContext) objContext;


		String strUserId = ebContext.getUserId().toString();



		final LAFRTAO lafrTAO = new LAFRTAO(ebContext);

		/* Inserting into CUSR,LGIN,BRCM table if no RMApproval is required */

	        //Added for unified login
			//Added for Authentication Scheme from IUSR tabele. 
			CommonCode authenticationScheme = ORUserCreationUtility.populateAuthenticationScheme(objContext);
			FEBAUnboundString segmentName = null;

			OnlineRegDetailsVO detVO = null;
			OnlineRegEnquiryVO onlineRegEnqVO = (OnlineRegEnquiryVO) FEBAAVOFactory.createInstance(TypesCatalogueConstants.OnlineRegEnquiryVO);
			onlineRegEnqVO.getCriteria().setUserId(strUserId);
			detVO = onlineRegEnqVO.getSummary();
			if ((FEBATypesUtility.isNotNullOrBlank(ebContext.getSegmentName()))){
				detVO.setSegmentName(ebContext.getSegmentName());
				}

			if ((FEBATypesUtility.isNotNullOrBlank(detVO.getSegmentName()))){
				segmentName= new FEBAUnboundString(detVO.getSegmentName().toString());
				}
			IndividualUserId individualID = insertIUSRForOR(objContext,strUserId,segmentName,authenticationScheme);
			ebContext.setIndividualId(individualID);
			CustomORUserCreationUtility.insertCUSRTAO(objContext,strUserId,individualID,loginAltFlowVO.getForgotPasswordOfflineVO().getFpSectionVO(),segmentName);
			PasswordVO passwordVO=loginAltFlowVO
					.getChangePwdVO();
			UserMaintenanceDetailsVO userMaintenanceDetailsVO = (UserMaintenanceDetailsVO)FEBAAVOFactory.createInstance("com.infosys.fentbase.types.valueobjects.UserMaintenanceDetailsVO");
			populateMaintenanceVO(objContext,strUserId,passwordVO,userMaintenanceDetailsVO);
			
			ORUserCreationUtility.insertCSIP(objContext, strUserId,individualID,segmentName);

			CustomORUserCreationUtility.insertBRCMTAO(objContext,strUserId);

			CustomORUserCreationUtility.insertCULNTAO(objContext,strUserId);

			ORUserCreationUtility.insertPWDHTAO(objContext,strUserId);

			/*Host call to create User in UCDT table*/
			UserMaintenanceHostInvoker.createRequest(ebContext, userMaintenanceDetailsVO);
		try {
			lafrTAO.associateBankId(ebContext.getBankId());
			lafrTAO.associateRequestId(ebContext.getRequestId());
			lafrTAO.associateRequestStatus(new Status(LoginAltFlowConstants.REQUEST_STATUS_CLOSE));
			lafrTAO.associateCookie(lafrInfo.getCookie());
			lafrTAO.update(ebContext);
		} catch (FEBATableOperatorException e) {
			throw new FatalException(
                    ebContext,
                    "User Record could not be updated",
                    EBIncidenceCodes.USER_RECORD_COULD_NOT_BE_UPDATED);
		}


		objContext.addBusinessInfo(getSuccessMessage(strUserId));


	}
	/**
	 *
	 * This method populates the user maintenance object which will be passed
	 * to Authentication MS for user creation
	 *
	 * @param objContext
	 * @param strUserId
	 * @param individualID
	 * @param passwordVO
	 * @param userMaintenanceDetailsVO
	 * @throws CriticalException
	 * @author Indrajit_Banerjee
	 * @since Nov 22, 2017
	 */
	private void populateMaintenanceVO(FEBATransactionContext objContext, String strUserId,
			 PasswordVO passwordVO, UserMaintenanceDetailsVO userMaintenanceDetailsVO)
			 {

		final FilePropertyManager filePropertyManager = new FilePropertyManager();
		userMaintenanceDetailsVO.setUserId(new UserId(strUserId));
		userMaintenanceDetailsVO.getOrgIdER().setOrgId(new CorporateId(strUserId));
		userMaintenanceDetailsVO.getUserDetailsVO().setUserType(EBankingConstants.USER_TYPE_RETAIL_USER);
		userMaintenanceDetailsVO.setFreeText1(new FreeTextSmall("OR"));
		userMaintenanceDetailsVO.setSignOnFlg(passwordVO.getSignOnFlg());
		userMaintenanceDetailsVO.setTxnFlg(passwordVO.getTxnFlg());
		userMaintenanceDetailsVO.setTxnNewPwd(passwordVO.getTxnNewPwd());
		userMaintenanceDetailsVO.setTxnDupNewPwd(passwordVO.getTxnDupNewPwd());
		userMaintenanceDetailsVO.setSignOnNewPwd(passwordVO.getSignOnNewPwd());
		userMaintenanceDetailsVO.setSignOnDupNewPwd(passwordVO.getSignOnDupNewPwd());
		String pwdExpDate= filePropertyManager.getProperty("OR_PWD_EXP_DATE", objContext);
		pwdExpDate= changeDateFormat(pwdExpDate);
		String txnPwdExpDate= filePropertyManager.getProperty("OR_TXN_PWD_EXP_DATE", objContext);
		txnPwdExpDate= changeDateFormat(txnPwdExpDate);
		String txnPwdResetDate= filePropertyManager.getProperty("OR_TXN_PWD_RESET_DATE", objContext);
		txnPwdResetDate=txnPwdResetDate.replaceAll("/", "-");
		
		userMaintenanceDetailsVO.setTxnPwdEnabledFlag(passwordVO.getTxnFlg());
		userMaintenanceDetailsVO.setNoOfAtmpts(new NumberOfAttempts(
						filePropertyManager.getProperty(LoginAltFlowConstants.OR_NO_OF_ATMPTS, objContext)));
		userMaintenanceDetailsVO.setTxnPwdExpDate(new FEBADate(DDMMYYYY, txnPwdExpDate));
		userMaintenanceDetailsVO.setPwdExpDate(new FEBADate(DDMMYYYY, pwdExpDate));
		userMaintenanceDetailsVO.setNoOfTxnAtmpts(new NumberOfAttempts(
				filePropertyManager.getProperty(LoginAltFlowConstants.OR_NO_OF_TXN_ATMPTS, objContext)));
		userMaintenanceDetailsVO.setTxnPwdResetDate(new FEBADate(DDMMYYYY, txnPwdResetDate));
		userMaintenanceDetailsVO.setPwdResetdate(new FEBADate(DDMMYYYY, "01-01-1900"));
		
		if (passwordVO.getSignOnFlg().equals(new YNFlag(FBAConstants.CHAR_Y))) {
			userMaintenanceDetailsVO.setFreeText2(new FreeTextSmall(passwordVO.getSignOnNewPwd().getValue()));
		}
		userMaintenanceDetailsVO.setForcePwdChangeFlag(new YNFlag(filePropertyManager
				.getProperty(LoginAltFlowConstants.OR_FORCE_PWD_CHANGE_FLG,
						objContext)));
		userMaintenanceDetailsVO.setForceTxnPwdChangeFlag(new YNFlag(filePropertyManager
				.getProperty(LoginAltFlowConstants.OR_FORCE_TXN_PASSWORD_CHANGE_FLG,
						objContext)));
		userMaintenanceDetailsVO.setDigSignEnabledFlag(new YNFlag(filePropertyManager
				.getProperty(LoginAltFlowConstants.OR_DIG_SIGN_ENABLED_FLG,
						objContext)));
		userMaintenanceDetailsVO.setForcePwdChangeFlag(
				new YNFlag(filePropertyManager.getProperty(LoginAltFlowConstants.OR_FORCE_PWD_CHANGE_FLG, objContext)));
		userMaintenanceDetailsVO.setForceQnaChangeFlag(new YNFlag(
				filePropertyManager.getProperty(EBLoginAltFlowConstants.OR_FORCE_QNA_CHANGE_FLG, objContext)));
		userMaintenanceDetailsVO.setPwdEnabledFlag(passwordVO.getSignOnFlg());
		/* Authentication MS call for inserting data in UCDT end */

	}

	private static String changeDateFormat(String dateExp) {
		dateExp=dateExp.replaceAll("/", "-");
		return dateExp;
	}

/**
	 *
	 * This method returns BusinessInfo with proper messages to be displayed
	 * based on RMApproval required or not
	 *
	 *
	 * @param rmApprovalReq
	 * @return infoDetails
	 *
	 * @author Renita_Pinto
	 *
	 * @since Aug 01, 2012 - 4:10:21 PM
	 *
	 */

	private BusinessInfoVO getSuccessMessage(
			String strUserId) {
		/* Instantiating BusinessinfoVO */
		final BusinessInfoVO infoDetails = (BusinessInfoVO) FEBAAVOFactory
				.createInstance("com.infosys.feba.framework.types.valueobjects.BusinessInfoVO");
		/*
		 * Setting the success code, displayMessage and logMessage based on RM
		 * Approval required or not
		 */
		FEBAUnboundInt errorCode = null;
		FEBAUnboundString displayMessage = null;
		FEBAUnboundString logMessage = null;

			errorCode = new FEBAUnboundInt(EBankingErrorCodes.OR_USER_CREATED_SUCCESSFULLY);
			displayMessage = new FEBAUnboundString(
					"The password is set and the User is created for Internet Banking");
			logMessage = new FEBAUnboundString(
					"The password is set and the User is created for Internet Banking");



		/*
		 * Setting the success code, displaymessage and logmessage in
		 * BusinessInfoVO
		 */
		infoDetails.setCode(errorCode);
		infoDetails.setLogMessage(logMessage);
		infoDetails.setDispMessage(displayMessage);
		infoDetails.setParam(new AdditionalParam("user_id",	new FEBAUnboundString(strUserId)));

		return infoDetails;
	}
	/**
	 * Method to inserts into IUSR table record for the Online Registration Enhancement, for 11.0.10
	 *
	 * @author ponvel_pk
	 * @param objContext
	 * @param strUserId
	 * @return
	 * @return IndividualUserId
	 */
	public IndividualUserId insertIUSRForOR(FEBATransactionContext objContext, String strUserId, FEBAUnboundString segmentName,CommonCode authScheme) {
		FBATransactionContext ebContext = (FBATransactionContext) objContext;

		// Generate sequence number for IUSR table.
		final IndividualUserId individualUserId = new IndividualUserId(String
				.valueOf(SequenceGeneratorUtil.getNextSequenceNumber(
						NXT_IUSR_SRL_NO, objContext)));
		final FilePropertyManager filePropertyManager = new FilePropertyManager();
		String twoFactorAuthRegReqd = null;
		IUSRTAO iusrTAO = new IUSRTAO(ebContext);
		iusrTAO.associateBankId(ebContext.getBankId());
		iusrTAO.associateOrgId(new CorporateId(strUserId));
		iusrTAO.associateUserId(new UserId(strUserId));
		iusrTAO.associateIndividualId(individualUserId);
		iusrTAO.associateUnifiedLoginUser(FBAConstants.YNFLAG_N);
		iusrTAO.associateAuthenticationScheme(authScheme);
		iusrTAO.associateForceChangePam(FBAConstants.YNFLAG_N);
		if ((FEBATypesUtility.isNotNullOrBlank(segmentName))){
			
			twoFactorAuthRegReqd = filePropertyManager.getProperty((LoginAltFlowConstants.OR_TWO_FACTOR_AUTH_REG_REQD+ "_" + segmentName), objContext);
			if (twoFactorAuthRegReqd.isEmpty()){
				twoFactorAuthRegReqd = null;				
			}
			iusrTAO.associateTwoFactorAuthRegReqd(new YNFlag(twoFactorAuthRegReqd));
		}
		else{
			iusrTAO.associateTwoFactorAuthRegReqd(FBAConstants.YNFLAG_N);

		}
		try {
			iusrTAO.insert(ebContext);
		} catch (FEBATableOperatorException e) {
			throw new FatalException(
					ebContext,
					"Insert into IUSR failed, While creating user via Online Registration",
					e.getMessage(), e.getCause());
		}

		return individualUserId;
	}

	}

