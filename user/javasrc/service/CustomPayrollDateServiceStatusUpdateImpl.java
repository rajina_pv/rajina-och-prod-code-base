package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.tao.CPCTTAO;
import com.infosys.custom.ebanking.types.valueobjects.CustomPayrollDateChangeVO;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.SequenceGeneratorUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.primitives.FEBAUnboundLong;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomPayrollDateServiceStatusUpdateImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void process(FEBATransactionContext objTrnCtx, IFEBAValueObject objInOutVo, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		CustomPayrollDateChangeVO inOutVO = (CustomPayrollDateChangeVO) objInOutVo;
		CPCTTAO cpcttao = new CPCTTAO(objTrnCtx);
		cpcttao.associateApplicationId(inOutVO.getApplicationId());
		cpcttao.associateBankId(objTrnCtx.getBankId());
		cpcttao.associateOldpayrolldate(inOutVO.getOldPayrollDate());
		cpcttao.associateNewpayrolldate(inOutVO.getNewPayrollDate());
		cpcttao.associateUserid(inOutVO.getUserId());
		cpcttao.associateStatusDesc(inOutVO.getStatusDesc());
		cpcttao.associateBatchreferenceid(
				new FEBAUnboundLong(SequenceGeneratorUtil.getNextSequenceNumber("NXT_PAY_DATE_CHANGE_SEQ", objTrnCtx)));
		cpcttao.associateStatus(inOutVO.getStatus());
		try {
			cpcttao.insert(objTrnCtx);
		} catch (FEBATableOperatorException e) {
			LogManager.logError(objTrnCtx, e);
//			throw new FatalException(objTrnCtx, "Record could not be updated", "USERTX0016");
		}

	}

}
