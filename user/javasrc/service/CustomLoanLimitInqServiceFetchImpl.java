package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.tao.CLATTAO;
import com.infosys.custom.ebanking.tao.info.CLATInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnLimitInqVO;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.feba.tools.common.util.Logger;

public class CustomLoanLimitInqServiceFetchImpl  extends
AbstractHostUpdateTran {

	   
public FEBAValItem[] prepareValidationsList(

FEBATransactionContext objContext,

IFEBAValueObject objInputOutput,

IFEBAValueObject objTxnWM)

throws BusinessException,

BusinessConfirmation,

CriticalException {

return new FEBAValItem[0];

}

/**
*
* This method is used to process local data
*
*/

protected void processLocalData(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
	IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

}

/**
* This method is used to make the HOST calls
*
*/

public void processHostData(

FEBATransactionContext objContext,

IFEBAValueObject objInputOutput,

IFEBAValueObject objTxnWM)

throws BusinessException,

BusinessConfirmation,

CriticalException {    	    	
	CustomLoanApplnLimitInqVO loanLimitInqVO = (CustomLoanApplnLimitInqVO) objInputOutput;
	try {
		CLATInfo clatinfo  =CLATTAO.select(objContext, objContext.getBankId(),loanLimitInqVO.getApplicationId());
		loanLimitInqVO.setLoanAcntId(clatinfo.getLoanAccountId().toString());
    EBHostInvoker.processRequest(
            objContext,
            CustomEBRequestConstants.CUSTOM_LOAN_LIMIT_INQUIRY,
            loanLimitInqVO);

} catch (CriticalException ce) {
	Logger.logError("Exception e" + ce);
}catch (FEBATableOperatorException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

}
}
