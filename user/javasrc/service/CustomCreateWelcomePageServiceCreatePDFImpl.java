package com.infosys.custom.ebanking.user.service;

import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomApplicationDocBinaryListVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomApplicationDocEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO;
import com.infosys.custom.ebanking.user.util.CustomDocumentSigningReportUtility;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.service.LocalServiceUtil;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.FEBABinary;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomCreateWelcomePageServiceCreatePDFImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {};
	}

	@Override
	public void process(FEBATransactionContext txnContext, IFEBAValueObject objInputOuput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		FEBABinary pdfBinary = null;
		
		CustomApplicationDocEnquiryVO enquiryVO = (CustomApplicationDocEnquiryVO) objInputOuput;
		CustomApplicationDocBinaryListVO binaryListVO = (CustomApplicationDocBinaryListVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomApplicationDocBinaryListVO);
		FEBAArrayList<CustomApplicationDocBinaryListVO> arrayList = new FEBAArrayList<>();
		CustomLoanApplicationDetailsVO applicationDetailsVO = (CustomLoanApplicationDetailsVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomLoanApplicationDetailsVO);

		CustomLoanApplnMasterDetailsVO applnMasterDetailsVO = (CustomLoanApplnMasterDetailsVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomLoanApplnMasterDetailsVO);

		applnMasterDetailsVO.setApplicationId(enquiryVO.getCriteria().getApplicationId());
		applicationDetailsVO.setLoanApplnMasterDetails(applnMasterDetailsVO);
		final FEBAUnboundString docTextMaintService = new FEBAUnboundString("CustomLoanApplicationService");
		final FEBAUnboundString docTextMaintMethod = new FEBAUnboundString("fetchLoanDetails");

		txnContext.setServiceName(docTextMaintService);
		txnContext.setMethodName(docTextMaintMethod);

		try {

			// Invoke the service to Mark the LM Structure.
			LocalServiceUtil.invokeService(txnContext, docTextMaintService.getValue(), docTextMaintMethod,
					applicationDetailsVO);
		} catch (FEBAException e) {
			// intentionally left blank
		}
		Date currentDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String formattedDate = formatter.format(currentDate);
		String docTitle = formattedDate + "-" + applicationDetailsVO.getLoanApplicantDetails().getNationalId() + "-" + "WelcomePack";
		

		FEBABinary binaryCode = createPDF(enquiryVO, applicationDetailsVO, txnContext);
		binaryListVO.setBinaryCode(binaryCode);
		binaryListVO.setDocString(getBase64Format(binaryCode));
		binaryListVO.setDocTitle(new FEBAUnboundString(docTitle));
		arrayList.addObject(binaryListVO);
		enquiryVO.setBinaryList(arrayList);

	}

	public FEBABinary createPDF(CustomApplicationDocEnquiryVO enquiryVO,
			CustomLoanApplicationDetailsVO applicationDetailsVO, FEBATransactionContext txnContext) {

		FEBAArrayList<CustomLoanApplicationDetailsVO> jrxmlContentList = new FEBAArrayList<>();
		jrxmlContentList.addObject(applicationDetailsVO);

		return generateReport(txnContext, jrxmlContentList);
	}

	private FEBABinary generateReport(FEBATransactionContext objContext,
			FEBAArrayList<CustomLoanApplicationDetailsVO> jrxmlContentList) {
		CustomDocumentSigningReportUtility reportUtility = new CustomDocumentSigningReportUtility();
		reportUtility.setSaveReport(false);
		reportUtility.addFieldName("loanApplicantDetails.name");
		reportUtility.addFieldName("loanApplicantDetails.addressLine1");
		reportUtility.addFieldName("loanApplicantDetails.addressLine2");
		reportUtility.addFieldName("loanApplicantDetails.addressLine3");
		reportUtility.addFieldName("loanApplicantDetails.addressLine4");
		reportUtility.addFieldName("loanApplicantDetails.addressLine5");
		reportUtility.addFieldName("loanApplicantDetails.state");
		reportUtility.addFieldName("loanApplicantDetails.city");
		reportUtility.addFieldName("loanApplicantDetails.postalCode");
		reportUtility.addFieldName("loanApplnMasterDetails.approvedAmount");
		reportUtility.addFieldName("loanApplnMasterDetails.loanAccountId");

		FEBABinary outputStream = null;
		try {
			outputStream = reportUtility.generateReport(objContext, "CustomWelcomePageCeria", jrxmlContentList, null,
					null, null, null);
		} catch (CriticalException | BusinessException | BusinessConfirmation e) {
			// intentionally left blank
		}
		return outputStream;
	}

	public String getBase64Format(FEBABinary pdfBinary) {
		String base64Format = Base64.getEncoder().encodeToString(pdfBinary.getValue());
		Base64.getDecoder().decode(base64Format);
		return base64Format;
	}

}
