package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomPrivyUpdateMobileNumVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractHostInquiryTran;
import com.infosys.feba.framework.types.primitives.CorporateId;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.tao.CUSRTAO;
import com.infosys.fentbase.tao.info.CUSRInfo;

public class CustomPrivyMobileNumServiceUpdateImpl extends AbstractHostInquiryTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void processHostData(FEBATransactionContext txnContext, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {

		EBTransactionContext context = (EBTransactionContext) txnContext;
		CustomPrivyUpdateMobileNumVO privyUpdadeVO = (CustomPrivyUpdateMobileNumVO) arg1;
		UserId userId = txnContext.getUserId();
		CorporateId orgId = new CorporateId(txnContext.getUserId().toString());
		CUSRInfo cusrInfoObj = null;
		try {
			cusrInfoObj = CUSRTAO.select(txnContext, context.getBankId(), orgId, userId);
			if (cusrInfoObj != null) {
				privyUpdadeVO.setOldMobileNum(cusrInfoObj.getCMPhoneNo());

			}
		} catch (FEBATableOperatorException e) {
			LogManager.logError(null, e);

		}
		if (privyUpdadeVO.getOldMobileNum().equals(privyUpdadeVO.getNewMobileNum())) {
			privyUpdadeVO.setCode("200");
			return;
		}
		try {
			privyUpdadeVO.setMerchantKey(PropertyUtil.getProperty("PRIVY_MERCHANT_KEY", txnContext));
			EBHostInvoker.processRequest(context, CustomEBRequestConstants.CUSTOM_PRIVY_MOBILE_NUMBER_UPDATE_REQUEST,
					privyUpdadeVO);
		} catch (BusinessException be) {
			throw new BusinessException(context, CustomEBankingIncidenceCodes.PRIVY_MOBILE_UPDATE,
					"An unexpected exception occurred during privy updation",
					CustomEBankingErrorCodes.PRIVY_MOBILE_UPDATE, be);
		}
	}

	@Override
	protected void processLocalData(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		// TODO Auto-generated method stub

	}

}
