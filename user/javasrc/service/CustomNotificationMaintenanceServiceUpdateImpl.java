package com.infosys.custom.ebanking.user.service;

import com.infosys.ci.common.LogManager;
import com.infosys.custom.ebanking.tao.CNEDTAO;
import com.infosys.custom.ebanking.tao.info.CNEDInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomNotificationInOutVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.tmo.FEBATMOException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.FEBAUnboundLong;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.primitives.YNFlag;
import com.infosys.feba.framework.types.valueobjects.FEBATMOCriteria;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.common.util.MaintenanceReqdUtility;

public class CustomNotificationMaintenanceServiceUpdateImpl extends AbstractLocalUpdateTran{

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {};
	}

	@Override
	public void process(FEBATransactionContext context, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		CustomNotificationInOutVO inOutVO = (CustomNotificationInOutVO)arg1;

		try {
			FEBATMOCriteria criteria = new FEBATMOCriteria();
			
			criteria.setMaintenanceRequired(MaintenanceReqdUtility
					.getMaintenanceReqd(context));
			UserId userId = context.getUserId();
			BankId bankId = context.getBankId();
			FEBAUnboundLong notificationId = inOutVO.getNotificationId();
			CNEDInfo  cnedInfo= null;
			CNEDTAO cnedTAO = new CNEDTAO(context);
			cnedTAO.associateBankId(bankId);
			cnedTAO.associateUserId(userId);
			cnedTAO.associateNotificationId(notificationId);
			cnedTAO.associateIsNew(new YNFlag("N"));
			cnedInfo = CNEDTAO.select(context, bankId, userId, notificationId);
			
			cnedTAO.associateCookie(cnedInfo.getCookie());
			
			cnedTAO.update(context);
		} catch (FEBATableOperatorException e) {
			
			throw new BusinessException(context,
					"Record does not exist.",
					e.getMessage(),
					e.getErrorCode());
		
		} 
		
	} 




}


