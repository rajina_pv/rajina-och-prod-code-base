package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.tao.CLADTAO;
import com.infosys.custom.ebanking.tao.CLDDTAO;
import com.infosys.custom.ebanking.tao.info.CLADInfo;
import com.infosys.custom.ebanking.tao.info.CLDDInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsDetailsVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.types.primitives.DocumentType;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.DALException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.tao.FEBATableOperator;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.CorporateId;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.feba.tools.common.util.Logger;
import com.infosys.fentbase.tao.CUSRTAO;
import com.infosys.fentbase.tao.info.CUSRInfo;
import com.infosys.fentbase.types.primitives.PanOrNationalID;

public class CustomLoanApplicationDocDetailsServiceCreateOrUpdateImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {};
	}

	@Override
	public void process(FEBATransactionContext txnContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		
		CustomLoanApplnDocumentsDetailsVO loanAppDocDetVO = (CustomLoanApplnDocumentsDetailsVO) objInputOutput;

		// insertion into LAMT using TAO call
		insertOrUpdateCLDD(txnContext, loanAppDocDetVO);
		//updateCUSR(txnContext,loanAppDocDetVO);
	}

	private void insertOrUpdateCLDD(FEBATransactionContext objContext,
			CustomLoanApplnDocumentsDetailsVO loanAppDocsDetVO) throws BusinessException {

		EBTransactionContext ebContext = (EBTransactionContext) objContext;

		// Declare a TAO object
    	CLDDTAO clddTAO = new CLDDTAO(objContext);    
    	CLDDInfo clddInfo = null;
    	
    	 FEBAArrayList<CustomLoanApplnDocumentsDetailsVO> docList = null;
		 QueryOperator queryOperator = QueryOperator.openHandle(objContext, CustomEBQueryIdentifiers.CUSTOM_FETCH_LOAN_DOCUMENT_LIST);

		 queryOperator.associate("bankId", ((EBTransactionContext) objContext).getBankId());
			queryOperator.associate("applicationId", loanAppDocsDetVO.getApplicationId());
			queryOperator.associate("docType", new DocumentType("KTP"));
			
			clddTAO.associateBankId(ebContext.getBankId());			
			clddTAO.associateApplicationId(loanAppDocsDetVO.getApplicationId());
			clddTAO.associateDocType(loanAppDocsDetVO.getDocType());
			clddTAO.associateDocCode(loanAppDocsDetVO.getDocCode());
			clddTAO.associateDocKey(loanAppDocsDetVO.getDocKey());
			clddTAO.associateDocStorePath(loanAppDocsDetVO.getDocStorePath());
			clddTAO.associateFileSequenceNum(loanAppDocsDetVO.getFileSeqNo());

			try {
				docList = queryOperator.fetchList(objContext);
				if(docList.size()>0 && loanAppDocsDetVO.getDocType().toString().equals("KTP")){
					CustomLoanApplnDocumentsDetailsVO docDetVO = docList.get(0);
					clddTAO.associateFileSequenceNum(docDetVO.getFileSeqNo());
					clddInfo = CLDDTAO.select(ebContext, ebContext.getBankId(),loanAppDocsDetVO.getApplicationId(), docDetVO.getFileSeqNo());					
					clddTAO.associateCookie(clddInfo.getCookie());
				
					clddTAO.physicalDelete(ebContext);
					
					clddTAO.associateFileSequenceNum(loanAppDocsDetVO.getFileSeqNo());
					clddTAO.insert(ebContext);
				}else{
				
					clddTAO.insert(objContext);
				}
				
			} catch (DALException e) {
				Logger.logError("Exception e" + e);
			
				try {
					clddTAO.insert(objContext);
				} catch (FEBATableOperatorException e1) {
					throw new BusinessException(objContext,
							EBIncidenceCodes.RETAIL_FIM_RECORD_INSERTION_FAILED_IN_EAUSR,
							"Record Insertion failed in CLDD", EBankingErrorCodes.UNABLE_TO_PROCESS_REQUEST);
				}
			} catch (FEBATableOperatorException e) {
				Logger.logError("Exception e" + e);
			} finally {
				// Call the closeHandle method which Closes the handle
				queryOperator.closeHandle((EBTransactionContext) objContext);
			}
	}
	private void updateCUSR (FEBATransactionContext ebContext,CustomLoanApplnDocumentsDetailsVO applicantDetailsVO) {

		System.out.println("update CUSR from CustomLoanApplicationDocDetailsServiceCreateOrUpdateImpl- "+applicantDetailsVO);
		
		try {
			
			CUSRInfo cusrInfo = null;
			CUSRTAO cusrTao = new CUSRTAO(ebContext);
			CorporateId corpId = new CorporateId(ebContext.getUserId().toString());
			UserId userId = ebContext.getUserId();
			BankId bankId = ebContext.getBankId();
			cusrInfo = CUSRTAO.select(ebContext, bankId, corpId, userId);
			cusrTao.associateBankId(bankId);
			cusrTao.associateUserId(userId);
			cusrTao.associateOrgId(corpId);
			cusrTao.associatePanNationalId(new PanOrNationalID(applicantDetailsVO.getDocCode().getValue()));
			cusrTao.associateCookie(cusrInfo.getCookie());
			cusrTao.update(ebContext);
			FEBATableOperator.commit(ebContext);
			System.out.println("Update in CUSR successfull for KTP Number");
		}   catch (FEBATableOperatorException e) {
			LogManager.logError(ebContext, e, "ERROR");
		} 
	}

   

}
