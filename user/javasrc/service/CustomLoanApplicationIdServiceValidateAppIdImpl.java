package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.types.valueobjects.CustomLoanAppIdCriteriaVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanAppIdEnquiryVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomLoanApplicationIdServiceValidateAppIdImpl extends AbstractLocalListInquiryTran
{

    public CustomLoanApplicationIdServiceValidateAppIdImpl()
    {
    }

    protected void associateQueryParameters(FEBATransactionContext txnContext, IFEBAValueObject objQueryCrit, IFEBAValueObject arg2, QueryOperator queryOperator)
        throws CriticalException
    {
        EBTransactionContext ebcontext = (EBTransactionContext)txnContext;
        CustomLoanAppIdCriteriaVO criteriaVO = (CustomLoanAppIdCriteriaVO)objQueryCrit;
        queryOperator.associate("bankId", ebcontext.getBankId());
        queryOperator.associate("userId", criteriaVO.getUserId());
        queryOperator.associate("applicationId", criteriaVO.getApplicationId());

        
    }

    public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
        throws BusinessException, BusinessConfirmation, CriticalException
    {
        return new FEBAValItem[0];
    }

    public String getQueryIdentifier(FEBATransactionContext pObjContext, IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
    {
        String queryIdentifier = "CustomValidateLoanAppIdDAL";
        return queryIdentifier;
    }

    protected void executeQuery(FEBATransactionContext objContext, IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
        throws BusinessException, BusinessConfirmation, CriticalException
    {
    	CustomLoanAppIdEnquiryVO enquiryVO = (CustomLoanAppIdEnquiryVO)objInputOutput;
        try
        {
            super.executeQuery(objContext, enquiryVO, objTxnWM);
            
        	if(enquiryVO.getResultList().size() == 0){
        		//throw business exception in the resource file.
        		enquiryVO.getDetails().setIsLoanAppIdMatch("N");
        	}else{
        		enquiryVO.getDetails().setIsLoanAppIdMatch("Y");
        	}
        }
        catch(BusinessException be)
        {
        	be.printStackTrace();
        	enquiryVO.getDetails().setIsLoanAppIdMatch("N");
            //throw new BusinessException(objContext, "LOANAPPLN001", "Loan Application ID Details have not found.", 8504);
        }
    }
}

