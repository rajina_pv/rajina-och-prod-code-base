package com.infosys.custom.ebanking.user.service;

import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomApplicationServiceDeDupCheckImpl extends AbstractLocalListInquiryTran {

	@Override
	protected void associateQueryParameters(FEBATransactionContext objContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2,
			QueryOperator queryOperator) throws CriticalException {
		queryOperator.associate("bankId", ((EBTransactionContext) objContext).getBankId());
		queryOperator.associate("userId", objContext.getUserId());
	}

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		return null;
	}

	@Override
	protected final void executeQuery(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		try {
			super.executeQuery(objContext, objInputOutput, objTxnWM);
		} catch (BusinessException be) {
			LogManager.logError(objContext, be);

		}
	}

	@Override
	public String getQueryIdentifier(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) {

		return "CustomLoanApplicationDeDupDAL";

	}

	@Override
	public void postProcess(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		// Do nothing .

	}
}
