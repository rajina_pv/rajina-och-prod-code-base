package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomAccountListDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomAccountListEnquiryVO;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.feba.tools.common.util.Logger;

public class CustomAccountListInquiryServiceFetchImpl extends AbstractHostUpdateTran{
	

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		return null;
	}

	@Override
	protected void processHostData(FEBATransactionContext ebcontext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {

		 
		CustomAccountListEnquiryVO enquiryVO = (CustomAccountListEnquiryVO) objInputOutput;
		
		FEBAArrayList<CustomAccountListDetailsVO> resultlist = enquiryVO.getResultList(); 
		
		UserId userid = enquiryVO.getUserId();
		FEBAUnboundString userIdStr=new FEBAUnboundString(userid.getValue());
		
		
		try {
			
			EBHostInvoker.processRequest(ebcontext, CustomEBRequestConstants.CUSTOM_ACCOUNT_LIST_INQUIRY_REQUEST,
					enquiryVO);
			
		} catch (BusinessException be) {
			throw new BusinessException(ebcontext, "LNINQ003",
					"An unexpected exception occurred during loan history retrieval",
					211083, be);
		    }

	}

	@Override
	protected void processLocalData(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		
		
	}

}
