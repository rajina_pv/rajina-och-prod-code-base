package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.tao.CLADTAO;

import com.infosys.custom.ebanking.tao.info.CLADInfo;

import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnApplicantDetailsVO;

import com.infosys.ebanking.common.EBTransactionContext;

import com.infosys.ebanking.common.validators.MobileNumberCheckVal;
import com.infosys.ebanking.common.validators.PhoneNumberPatternVal;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;

import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValEngineConstants;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.common.validators.EmailValidator;

public class CustomLoanApplicationApplicantDetailsServiceModifyImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		CustomLoanApplnApplicantDetailsVO applicantDetVO = (CustomLoanApplnApplicantDetailsVO) objInputOutput;

		return new FEBAValItem[] {

				new FEBAValItem("emailId", applicantDetVO.getEmailId(), new EmailValidator(),
						FEBAValEngineConstants.NON_MANDATORY, FEBAValEngineConstants.INDEPENDENT),

				new FEBAValItem("mobileNum", applicantDetVO.getMobileNum(), new MobileNumberCheckVal(),
						FEBAValEngineConstants.NON_MANDATORY, FEBAValEngineConstants.INDEPENDENT),

				new FEBAValItem("homePhoneNum", applicantDetVO.getHomePhoneNum(), new PhoneNumberPatternVal(),
						FEBAValEngineConstants.NON_MANDATORY, FEBAValEngineConstants.INDEPENDENT), };

	}

	@Override
	public void process(FEBATransactionContext txnContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		EBTransactionContext ebContext = (EBTransactionContext) txnContext;
		CustomLoanApplnApplicantDetailsVO loanApplicantDetlsVO = (CustomLoanApplnApplicantDetailsVO) objInputOutput;
		final CLADTAO cladTAO = new CLADTAO(ebContext);
		CLADInfo cladInfo = null;
		try {
			cladTAO.associateBankId(ebContext.getBankId());
			cladTAO.associateApplicationId(loanApplicantDetlsVO.getApplicationId());
			cladTAO.associateMobileNo(loanApplicantDetlsVO.getMobileNum());
			cladTAO.associateEmailId(loanApplicantDetlsVO.getEmailId());
			cladTAO.associateName(loanApplicantDetlsVO.getName());
			cladTAO.associateNationalId(loanApplicantDetlsVO.getNationalId());
			cladTAO.associateGender(loanApplicantDetlsVO.getGender());
			cladTAO.associatePlaceOfBirth(loanApplicantDetlsVO.getPlaceOfBirth());
			cladTAO.associateReligion(loanApplicantDetlsVO.getReligion());
			cladTAO.associateLastEducation(loanApplicantDetlsVO.getLastEducation());
			cladTAO.associateHomeOwnershipStatus(loanApplicantDetlsVO.getHomeOwnershipStatus());
			cladTAO.associateHomeOwnershipDuration(loanApplicantDetlsVO.getHomeOwnershipDuration());
			cladTAO.associateHomePhoneNum(loanApplicantDetlsVO.getHomePhoneNum());
			cladTAO.associateAddrLine1(loanApplicantDetlsVO.getAddressLine1());
			cladTAO.associateAddrLine2(loanApplicantDetlsVO.getAddressLine2());
			cladTAO.associateAddrLine3(loanApplicantDetlsVO.getAddressLine3());
			cladTAO.associateAddrLine4(loanApplicantDetlsVO.getAddressLine4());
			cladTAO.associateAddrLine5(loanApplicantDetlsVO.getAddressLine5());
			cladTAO.associateState(loanApplicantDetlsVO.getState());
			cladTAO.associateCity(loanApplicantDetlsVO.getCity());
			cladTAO.associatePostalCode(loanApplicantDetlsVO.getPostalCode());
			cladTAO.associateCardIssuerBank(loanApplicantDetlsVO.getCardIssuerBank());
			cladTAO.associateDateOfBirth(loanApplicantDetlsVO.getDateOfBirth());
			cladTAO.associateIsAddrSameAsKtp(loanApplicantDetlsVO.getIsAddressSameAsKTP());
			cladInfo = CLADTAO.select(ebContext, ebContext.getBankId(), loanApplicantDetlsVO.getApplicationId());			
			cladTAO.associateCookie(cladInfo.getCookie());

			cladTAO.update(ebContext);

		} catch (FEBATableOperatorException e) {

			throw new FatalException(ebContext, "Record could not be updated",
					FBAIncidenceCodes.USER_RECORD_COULD_NOT_BE_UPDATED);
		}

	}

}
