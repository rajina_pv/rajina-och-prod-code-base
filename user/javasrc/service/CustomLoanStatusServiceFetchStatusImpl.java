package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanCallDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanCallEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanStatusVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractHostInquiryTran;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.CorporateId;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.Flag;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.tao.CUSRTAO;
import com.infosys.fentbase.tao.info.CUSRInfo;

public class CustomLoanStatusServiceFetchStatusImpl extends AbstractHostInquiryTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext context, IFEBAValueObject inOutVO,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {					
		return new FEBAValItem[] {};
	}

	@Override
	protected void processHostData(FEBATransactionContext febaContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		EBTransactionContext context = (EBTransactionContext) febaContext;
		CustomLoanStatusVO statusVO = (CustomLoanStatusVO) objInputOutput;
		CustomLoanCallEnquiryVO enqVO = (CustomLoanCallEnquiryVO) FEBAAVOFactory
				.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanCallEnquiryVO");	


		String userId=context.getUserId().toString();
		CUSRInfo cusrInfo =null;
		try {
			cusrInfo = CUSRTAO.select(context, context.getBankId(),new CorporateId(userId),new UserId(userId));
			if (cusrInfo == null) {
				throw new BusinessException(true, context, "LNINQ002",
						"User Id is invalid", null, 211082, null);								
			} 
		} catch (FEBATypeSystemException | FEBATableOperatorException e) {
			throw new BusinessException(true, context, "LNINQ002",
					"User Id is invalid", null, 211082, null);
		}
		try {
			if(null!=cusrInfo){
				enqVO.getCriteria().setCifId(new FEBAUnboundString(cusrInfo.getCustId().toString()));
				EBHostInvoker.processRequest(context, "CustomLoanListInqRequest", enqVO);
			}			
		}catch (BusinessException be) {
			throw new BusinessException(context, "LNINQ003",
					"An unexpected exception occurred during loan list retrieval",
					211082, be);
		}
		catch (Exception e) {
			throw new BusinessException(context, EBIncidenceCodes.HIF_HOST_NOT_AVAILABLE, "The host does not exist.",
					EBankingErrorCodes.HOST_NOT_AVAILABLE);
		}
		FEBAUnboundString activeLoanAccountID = statusVO.getAccountId();
		boolean isLoanPaid=false;
		if(enqVO.getResultList().size()!=0)
		{
			FEBAArrayList<CustomLoanCallDetailsVO> detailsListVO = enqVO.getResultList();

			for (int i=0;i<detailsListVO.size();i++)
			{  
				CustomLoanCallDetailsVO detVO= detailsListVO.get(i);

				String bal = detVO.getAccountBalance().toString();	    		
				String sign=bal.substring(0,1);
				if(sign.equalsIgnoreCase("-") || detVO.getAcctClsFlg().equals(new Flag('N')))  			 
				{
					statusVO.setLoanStatus("Active");
					statusVO.setAccountId(detVO.getAccountId());
					break;
				}
				if(activeLoanAccountID.equals(detVO.getAccountId()) && detVO.getAcctClsFlg().getValue()=='Y'){
					isLoanPaid=true;
				}
			}	         
		}else{
			throw new BusinessException(context, CustomEBankingIncidenceCodes.NO_RECORDS_FETCHED, 
					"No records Found", EBankingErrorCodes.NO_RECORDS_FOUND);
		}

		if(isLoanPaid)
		{
			statusVO.setLoanStatus("LOAN_PAID");
		}


	}


	@Override
	protected void processLocalData(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		// To Do Code
	}

	@Override
	public void postProcess(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject bjTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {	
		// To Do Code
	}

}
