package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.tao.CPITTAO;
import com.infosys.custom.ebanking.tao.info.CPITInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnPayrollAccntDetailsVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAIncidenceCodes;

public class CustomCaptureApplnPayrollAccntServiceModifyImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {};
	}

	@Override
	public void process(FEBATransactionContext txnContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		
		CustomLoanApplnPayrollAccntDetailsVO payrollAccntDetVO = (CustomLoanApplnPayrollAccntDetailsVO) objInputOutput;

		// updation into CLAPA using TAO call
		updateCPIT(txnContext, payrollAccntDetVO);

	}

	private void updateCPIT(FEBATransactionContext objContext, CustomLoanApplnPayrollAccntDetailsVO payrollAcctDetVO)
			throws BusinessException {

		EBTransactionContext ebContext = (EBTransactionContext) objContext;

		// Declare a TAO object
		CPITTAO cpitTAO = new CPITTAO(objContext);
		CPITInfo cpitInfo = null;

		try {
			cpitTAO.associateBankId(ebContext.getBankId());
			cpitTAO.associateApplicationId(payrollAcctDetVO.getApplicationId());
			cpitTAO.associatePayrollAccountNum(payrollAcctDetVO.getPayrollAccountId());
			cpitTAO.associatePayrollAccBranch(payrollAcctDetVO.getPayrollAccountBranch());
			cpitTAO.associateSavingAmount(payrollAcctDetVO.getSavingAmount());
			
			cpitTAO.associateNameWl(payrollAcctDetVO.getNameWL());
			cpitTAO.associateBirthDateWl(payrollAcctDetVO.getBirthDateWL());
			cpitTAO.associateGenderWl(payrollAcctDetVO.getGenderWL());
			cpitTAO.associateAddressWl(payrollAcctDetVO.getAddressWL());
			cpitTAO.associatePlaceOfBirthWl(payrollAcctDetVO.getPlaceOfBirthWL());
			
			cpitInfo = CPITTAO.select(ebContext, ebContext.getBankId(), payrollAcctDetVO.getApplicationId());	
			
			cpitTAO.associateCookie(cpitInfo.getCookie());

			cpitTAO.update(ebContext);

		} catch (FEBATableOperatorException e) {

			throw new FatalException(ebContext, "Record could not be updated",
					FBAIncidenceCodes.USER_RECORD_COULD_NOT_BE_UPDATED);
		}
	}

}
