package com.infosys.custom.ebanking.user.service;

import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.hif.EBRequestConstants;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.types.primitives.FEBARecordUserId;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.types.primitives.LAFRequestType;
import com.infosys.fentbase.types.valueobjects.LoginAltFlowVO;
import com.infosys.fentbase.user.LoginAltFlowConstants;

public class CustomLoginAltFlowServiceInsertOfflineRequestDetailsImpl extends AbstractHostUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {};
	}

	/**
	 * This method is used to process host data.
	 *
	 */
	@Override
	protected void processHostData(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

		LoginAltFlowVO loginAltFlowVO = (LoginAltFlowVO) objInputOutput;
		final EBTransactionContext ebContext = (EBTransactionContext) objContext;

		if (ebContext.getRequestType().equals(new LAFRequestType(LoginAltFlowConstants.FORGOT_PWD_OFFLINE_REQUEST))
				|| ebContext.getRequestType()
						.equals(new LAFRequestType(LoginAltFlowConstants.FORGOT_PWD_UX3_OFFLINE_REQUEST))) {
			// Setting SYSTEM as record user id since it is required to insert
			// value for r_cre_id and r_mod_id in table
			ebContext.setRecordUserId(new FEBARecordUserId("SYSTEM"));
		}

		EBHostInvoker.processRequest(objContext, EBRequestConstants.LAF_INSERT_REQ_DETAILS_REQUEST, loginAltFlowVO);

		loginAltFlowVO.getForgotPasswordOfflineVO().setRequestID(ebContext.getRequestId());
		/* Added for fp offline alert request title -start */
		if (ebContext.getRequestType().equals(new LAFRequestType((LoginAltFlowConstants.FORGOT_PWD_OFFLINE_REQUEST)))
				|| ebContext.getRequestType()
						.equals(new LAFRequestType((LoginAltFlowConstants.FORGOT_PWD_UX3_OFFLINE_REQUEST)))) {
			loginAltFlowVO.getForgotPasswordOfflineVO().setRequestTitle("Forgot Password");
		}
		if (ebContext.getRequestType().equals(new LAFRequestType((LoginAltFlowConstants.ONLINE_REG_OFFLINE_REQUEST)))) {
			loginAltFlowVO.getForgotPasswordOfflineVO().setRequestTitle("Online Registration");
		}

		/* Added for fp offline alert request title -end */
	}

	@Override
	protected void processLocalData(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		// Do nothing as we are calling host.

	}
}
