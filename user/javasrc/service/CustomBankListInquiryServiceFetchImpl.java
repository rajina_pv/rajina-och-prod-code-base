package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomBankListEnquiryVO;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.feba.tools.common.util.Logger;

public class CustomBankListInquiryServiceFetchImpl extends AbstractHostUpdateTran{
	

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		return null;
	}

	@Override
	protected void processHostData(FEBATransactionContext ebcontext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {

		 
		CustomBankListEnquiryVO enquiryVO = (CustomBankListEnquiryVO) objInputOutput;
		
		UserId userid = enquiryVO.getUserId();
		FEBAUnboundString userIdStr=new FEBAUnboundString(userid.getValue());
		
		try {
			
			EBHostInvoker.processRequest(ebcontext, CustomEBRequestConstants.CUSTOM_BANK_LIST_INQUIRY_REQUEST,
					enquiryVO);
			
		} catch (CriticalException ce) {
			Logger.logError("Exception e" + ce);
		}
		
		
		/*FEBAArrayList<CustomBankListDetailsVO> resultlist = enquiryVO.getResultList(); 
		
		if(null != resultlist){
			
			for(int i=0;i<resultlist.size();i++){
				
				enquiryVO.getDetails().setBankCode(resultlist.get(i).getBankCode());
				enquiryVO.getDetails().setBankName(resultlist.get(i).getBankName());

			}
		}*/
		
		
	}

	@Override
	protected void processLocalData(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		
		
	}

}
