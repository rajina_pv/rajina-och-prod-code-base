package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.types.valueobjects.CustomLoanCallDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanCallEnquiryVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractHostInquiryTran;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.primitives.CorporateId;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.tao.CUSRTAO;
import com.infosys.fentbase.tao.info.CUSRInfo;

public class CustomLoanListServiceFetchBalanceImpl extends AbstractHostInquiryTran {
		
	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext context, IFEBAValueObject inOutVO,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {					
		return new FEBAValItem[] {};
	}

	@Override
	protected void processHostData(FEBATransactionContext febaContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		EBTransactionContext context = (EBTransactionContext) febaContext;
		CustomLoanCallEnquiryVO enquiryVO = (CustomLoanCallEnquiryVO) objInputOutput;
		CustomLoanCallDetailsVO detailsVO = enquiryVO.getDetails();
		
		try {
			if (!FEBATypesUtility.isNotNullOrBlank(detailsVO.getAccountId()))
			 {
				throw new BusinessException(true, context, "LNINQ004",
						"Account Id is invalid", null, 211083, null);
			 }				
			EBHostInvoker.processRequest(context, "CustomLoanBalInqRequest", enquiryVO);
			
		}
	      catch (BusinessException be) {
		throw new BusinessException(context, "LNINQ003",
				"An unexpected exception occurred during loan list retrieval",
				211083, be);
	    }
		
		if(!enquiryVO.getDetails().getStatus().toString().equalsIgnoreCase("SUCCESS"))
		{
			 throw new BusinessException(true, context, "LNINQ005",
						"Error in Fetching in Balance details.", null, 211304, null);
		}
	}
	

	@Override
	protected void processLocalData(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		// To Do Code
	}

	@Override
	public void postProcess(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject bjTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {	
		// To Do Code
	}

}
