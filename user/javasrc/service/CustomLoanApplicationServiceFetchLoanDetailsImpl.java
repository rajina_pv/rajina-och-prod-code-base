package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.tao.CLATTAO;

import com.infosys.custom.ebanking.tao.info.CLATInfo;

import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationDetailsVO;
import com.infosys.custom.ebanking.user.util.CustomPopulateLoanApplicationDetailsUtility;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;

import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalInquiryTran;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;

import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomLoanApplicationServiceFetchLoanDetailsImpl extends AbstractLocalInquiryTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {};

	}

	@Override
	protected void process(FEBATransactionContext objContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {

		CustomLoanApplicationDetailsVO detailsVO = (CustomLoanApplicationDetailsVO) objInputOutput;
		EBTransactionContext ebContext = (EBTransactionContext) objContext;
		CLATInfo clatInfo = null;

		try {
			// Fetch the TRQH record for modify/update

			clatInfo = CLATTAO.select(objContext, ebContext.getBankId(),
					detailsVO.getLoanApplnMasterDetails().getApplicationId());

			CustomPopulateLoanApplicationDetailsUtility.populateLoanDetails(detailsVO, clatInfo);

			CustomPopulateLoanApplicationDetailsUtility.populatePayrollDetails(objContext, detailsVO);

			CustomPopulateLoanApplicationDetailsUtility.populateDocumentDetails(objContext, detailsVO);

			CustomPopulateLoanApplicationDetailsUtility.populateApplicantDetails(objContext, detailsVO);
			CustomPopulateLoanApplicationDetailsUtility.populateContactDetails(objContext, detailsVO);
			CustomPopulateLoanApplicationDetailsUtility.populateEmploymentDetails(objContext, detailsVO);

		} catch (FEBATableOperatorException e) {
			throw new CriticalException(objContext, "NO RECORDS FETCHED", "No Records fetched ",
					EBankingErrorCodes.NO_RECORDS_FOUND, e);
		}

	}

}
