package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.rest.common.v1.CustomResourceConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomEmailDetailsVO;
import com.infosys.custom.ebanking.user.custom.CustomSendEmailUtil;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.primitives.CorporateId;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.tao.CUSRTAO;
import com.infosys.fentbase.tao.info.CUSRInfo;

public class CustomSendEmailServiceSendMailImpl extends AbstractLocalUpdateTran {
		
	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {		
		return new FEBAValItem[] {};
	}

	@Override
	public void process(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		
		EBTransactionContext context = (EBTransactionContext) arg0;
		CustomEmailDetailsVO detVO = (CustomEmailDetailsVO) arg1;
		
		try {
			CUSRInfo cusrInfo = CUSRTAO.select(context, context.getBankId(),new CorporateId(context.getUserId().toString()),new UserId(context.getUserId().toString()));
			detVO.setToAddress(cusrInfo.getCEmailId().toString());
			detVO.setMessageContent("Some Content");
			detVO.setSubject(CustomResourceConstants.SUBJECT);
			CustomSendEmailUtil.sendEmail(arg0,detVO);
		} 
		catch (FEBATypeSystemException | FEBATableOperatorException e) 
		{
			throw new BusinessException(true, context, "LNINQ002",
					"User Id is invalid", null, 211082, null);
		}
		
		
	}

}
