
package com.infosys.custom.ebanking.user.service;


import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.tao.CUPRTAO;
import com.infosys.custom.ebanking.tao.info.CUPRInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomPrivyRegistrationVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.BusinessExceptionVO;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.feba.tools.common.util.Logger;
import com.infosys.feba.utils.insulate.ArrayList;

/**
 * This service is used for token privy token creation..
 * 
 */

public class CustomPrivyServiceTokenCreateImpl extends AbstractHostUpdateTran {

	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[0];
	}

	/**
	 *
	 * This method is used to process local data
	 *
	 */

	protected void processLocalData(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

	}

	/**
	 * This method is used to make the HOST calls
	 *
	 */

	protected void processHostData(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		final EBTransactionContext ebContext = (EBTransactionContext) objContext;	
		CustomPrivyRegistrationVO privyRegVO = (CustomPrivyRegistrationVO) objInputOutput;
		validateMandatory(objContext, privyRegVO);	
		FEBAUnboundString userId = privyRegVO.getUserId();		
		String userIdStr = userId.toString();

		if (userIdStr != "" && !(userIdStr.isEmpty())) {
			try {
				populatePrivyRegVO(ebContext, privyRegVO);
			} catch (FEBATableOperatorException e) {
				throw new FatalException(ebContext, "Unable to select the record for Privy Token Creation",
						CustomEBankingIncidenceCodes.UNABLE_TO_SELECT_RECORD_FOR_PRIVY_TOKEN_CREATION, e);
			}
		}

		try {
			EBHostInvoker.processRequest(objContext, CustomEBRequestConstants.CUSTOM_PRIVY_TOKEN_CREATION_REQUEST,
					privyRegVO);

		} catch (CriticalException ce) {
			Logger.logError("Exception e" + ce);
		}

	}

	
	private void validateMandatory(FEBATransactionContext ebContext, CustomPrivyRegistrationVO privyRegVO)
			throws BusinessException {
		ArrayList<BusinessExceptionVO> valError = new ArrayList<>();
		
		if (!FEBATypesUtility.isNotNullOrBlank(privyRegVO.getUserId())) {
			valError.add(getBusinessExceptionVO(ebContext,"User Id is mandatory"));
		}
		
		if (!valError.isEmpty()) {
			throw new BusinessException(ebContext, valError);
		}
	}

	private void populatePrivyRegVO(EBTransactionContext ebContext, CustomPrivyRegistrationVO privyRegVO)
			throws FEBATableOperatorException {
		CUPRInfo cuprInfo=CUPRTAO.select(ebContext, ebContext.getBankId(), privyRegVO.getUserId());
		privyRegVO.setPrivyId(cuprInfo.getPrivyId());
		privyRegVO.setMerchantKey(PropertyUtil.getProperty("PRIVY_MERCHANT_KEY", ebContext));
	}

	private BusinessExceptionVO getBusinessExceptionVO(FEBATransactionContext context, String msg) {
		return new BusinessExceptionVO(false, context, EBIncidenceCodes.HOST_FAILURE_RESPONSE, msg,
				EBankingErrorCodes.HOST_CP_FI_ERROR_CODE, null,
				new AdditionalParam("message", msg));
	}

	private BusinessException getBusinessException(FEBATransactionContext context, String errorField, String msg) {
		return new BusinessException(false, context, EBIncidenceCodes.HOST_FAILURE_RESPONSE, msg, errorField,
				EBankingErrorCodes.HOST_CP_FI_ERROR_CODE, null,
				new AdditionalParam("message", msg));
	}

	
}
