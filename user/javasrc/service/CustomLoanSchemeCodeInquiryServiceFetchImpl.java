package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanSchmCodeListEnquiryVO;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomLoanSchemeCodeInquiryServiceFetchImpl extends AbstractHostUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {		
		return new FEBAValItem[] {};
	}

	@Override
	protected void processHostData(FEBATransactionContext objContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		CustomLoanSchmCodeListEnquiryVO enquiryVO = (CustomLoanSchmCodeListEnquiryVO) objInputOutput;	    
	    	try {
	    		EBHostInvoker.processRequest(objContext, CustomEBRequestConstants.CUSTOM_LOAN_SCHEME_CODES_FETCH_REQUEST,
						enquiryVO);
	    	}
	    		  catch (BusinessException be) {
	    				throw new BusinessException(objContext, "LNINQ003",
	    						"An unexpected exception occurred during loan scheme codes retrieval",
	    						211083, be);
	    			    }
	    	if(enquiryVO.getResultList().size()==0)
	    	{
	    		throw new BusinessException(objContext, CustomEBankingIncidenceCodes.NO_RECORDS_FETCHED, 
						"No History Found for account", EBankingErrorCodes.NO_RECORDS_FOUND);	    		
	    	}
	    
		
	}

	@Override
	protected void processLocalData(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		// To write some code		
	}

}
