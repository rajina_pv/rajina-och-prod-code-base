
package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.tao.CLATTAO;
import com.infosys.custom.ebanking.tao.CUPRTAO;
import com.infosys.custom.ebanking.tao.info.CLATInfo;
import com.infosys.custom.ebanking.tao.info.CUPRInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomPrivyRegistrationVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperator;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalInquiryTran;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

/**
 * This service is used for privy registration..
 * 
 */

public class CustomPrivyServiceFetchByUserIdImpl extends AbstractLocalInquiryTran {

	@SuppressWarnings("rawtypes")
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};

	}

	@SuppressWarnings("rawtypes")
	@Override
	protected void process(FEBATransactionContext objContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		final EBTransactionContext ebContext = (EBTransactionContext) objContext;
		CustomPrivyRegistrationVO privyRegistrationVO = (CustomPrivyRegistrationVO) objInputOutput;
		try {
			CUPRInfo info = CUPRTAO.select(objContext, objContext.getBankId(), privyRegistrationVO.getUserId());
			if(info.getStage().getValue().equalsIgnoreCase(CustomEBConstants.PRIVY_COM)){
				CustomLoanApplicationDetailsVO detailsVO = (CustomLoanApplicationDetailsVO) 
						FEBAAVOFactory.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationDetailsVO");
				detailsVO.getLoanApplicantDetails().setApplicationId(privyRegistrationVO.getApplicationId());

				updateCLAT(objContext,detailsVO);
				
				privyRegistrationVO.setKTP(info.getKtpNum());
				privyRegistrationVO.setPrivyId(info.getPrivyId());
				privyRegistrationVO.setName(info.getPrivyUserName());
				privyRegistrationVO.setStage(info.getStage());
			}else{
				throw new BusinessException(
						objContext,
						EBIncidenceCodes.NO_RECORDS_FETCHED_FROM_TSTM,
						"Privy record not found",
						EBankingErrorCodes.NO_RECORDS_FOUND);
			}			
		} catch (FEBATableOperatorException e) {
			throw new BusinessException(
					objContext,
					EBIncidenceCodes.NO_RECORDS_FETCHED_FROM_TSTM,
					"Privy record not found",
					EBankingErrorCodes.NO_RECORDS_FOUND);
		}

	}
	private void updateCLAT (FEBATransactionContext ebContext,CustomLoanApplicationDetailsVO detailsVO) throws BusinessException {
		ApplicationNumber applicationId = detailsVO.getLoanApplicantDetails().getApplicationId();
		CLATTAO clatTao = new CLATTAO(ebContext);
		CLATInfo clatInfo = null;
		try {
			clatInfo = CLATTAO.select(ebContext, ebContext.getBankId(), applicationId);
			if(!clatInfo.getApplicationStatus().getValue().equalsIgnoreCase("DISB_ACC_CONF")){
				throw new BusinessException(true, ebContext, CustomEBankingIncidenceCodes.LN_APP_ID_MANDATORY,
						"Loan Application Id is null or invalid", null, CustomEBankingErrorCodes.LN_APP_ID_MANDATORY, null);
			}
			clatTao.associateBankId(clatInfo.getBankId());
			clatTao.associateUserId(clatInfo.getUserId());
			clatTao.associateApplicationId(clatInfo.getApplicationId());
			
			clatTao.associateApplicationStatus(new CommonCode(CustomEBConstants.PRIVY_COM));
			
			clatTao.associateCookie(clatInfo.getCookie());
			clatTao.update(ebContext);					
			FEBATableOperator.commit(ebContext);
		} catch (FEBATableOperatorException e) {
			throw new BusinessException(true, ebContext, CustomEBankingIncidenceCodes.LN_APP_ID_MANDATORY,
					"Loan Application Id is null or invalid", null, CustomEBankingErrorCodes.LN_APP_ID_MANDATORY, null);
		}
	}

}
