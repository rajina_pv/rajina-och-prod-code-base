package com.infosys.custom.ebanking.user.service;



import com.infosys.custom.ebanking.tao.CLHTTAO;
import com.infosys.custom.ebanking.tao.info.CLHTInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanOriginationDetailsVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;


public class CustomLoanOriginationServiceViewHistoryImpl extends AbstractLocalUpdateTran{

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {};
	}

	@Override
	public void process(FEBATransactionContext context, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		final EBTransactionContext ebContext = (EBTransactionContext) context;
		CustomLoanOriginationDetailsVO detailsVO = (CustomLoanOriginationDetailsVO)arg1;		
		ApplicationNumber applicationId=detailsVO.getApplicationId();
		String applicationIdStr=applicationId.toString();
		
    	
		if (applicationIdStr != "" && !(applicationIdStr.isEmpty())) {
			try {
				CommonCode appStatus=detailsVO.getApplicationStatus();
				CLHTInfo clhtInfo=CLHTTAO.select(ebContext, ebContext.getBankId(), detailsVO.getApplicationId(),appStatus);
				detailsVO.setHistoryCreationTime(clhtInfo.getCookie().getRCreTime());
				detailsVO.setHistoryRemarks(clhtInfo.getRemarks());		
				if(appStatus==null){
					detailsVO.setApplicationStatus(clhtInfo.getApplicationStatus());
				}
			}  catch (FEBATableOperatorException e) {
				throw new CriticalException(context, "NO RECORDS FETCHED", "No Records fetched ",
						EBankingErrorCodes.NO_RECORDS_FOUND, e);
			}
		}
		
	} 


}


