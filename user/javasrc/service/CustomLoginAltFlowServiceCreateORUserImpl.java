/**

 * PersonalProfileServiceChangePasswordImpl.java

 *

 * COPYRIGHT NOTICE:

 * Copyright (c) 2007 Infosys Technologies Limited

 * All Rights Reserved.

 *

 * This software is the confidential and proprietary information of

 * Infosys Technologies Ltd. ("Confidential Information"). You shall

 * not disclose such Confidential Information and shall use it only

 * in accordance with the terms of the license agreement you entered

 * into with Infosys.

 */
package com.infosys.custom.ebanking.user.service;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.hif.EBRequestConstants;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.primitives.YNFlag;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.types.primitives.Password;
import com.infosys.fentbase.types.primitives.UserSignOnStatusFlag;
import com.infosys.fentbase.types.valueobjects.LoginAltFlowVO;
import com.infosys.fentbase.types.valueobjects.PasswordVO;
import com.infosys.fentbase.user.service.LoginAltFlowNavigationUtility;

/**
 * This class creates user
 *
 * @author Renita_Pinto
 * @extends AbstractHostUpdateTran
 */
public final class CustomLoginAltFlowServiceCreateORUserImpl extends
AbstractHostUpdateTran {

    /**
     * Validates the password
     *
     * @see com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran#prepareValidationsList(com.infosys.feba.framework.commontran.context.FEBATransactionContext,
     *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject,
     *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject)
     * @throws BusinessException
     */
    public FEBAValItem[] prepareValidationsList(
            FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
            IFEBAValueObject objTxnWM) throws BusinessException,
            BusinessConfirmation, CriticalException {


		return new FEBAValItem[] {};
    }
    /**
     * Method to create user
     *
     * @see com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran#processHostData(com.infosys.feba.framework.commontran.context.FEBATransactionContext,
     *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject,
     *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject)
     * @throws BusinessException
     * @throws CriticalException
     * @throws BusinessConfirmation
     */

	@Override
	protected void processHostData(FEBATransactionContext objContext,
			IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
			throws BusinessException, BusinessConfirmation, CriticalException {

    	final LoginAltFlowVO loginAltFlowVO = (LoginAltFlowVO) objInputOutput;
    	final EBTransactionContext ebContext = (EBTransactionContext) objContext;

    	final UserSignOnStatusFlag status;

    	if (ebContext.isUnifiedLoginSession()) {
			status = new UserSignOnStatusFlag(
					EBankingConstants.LOGIN_STATUS_UNIFIED_LOGIN);
		} else {
			status = new UserSignOnStatusFlag(
					EBankingConstants.LOGIN_STATUS_COMPLETELY_AUTHENTICATED_CHAR);
		}
    	
    	PasswordVO passwordVO = loginAltFlowVO.getChangePwdVO();
    	
    	YNFlag signonFlag = passwordVO.getSignOnFlg();
        
        boolean signOnBeingChanged = (signonFlag.getValue() == (EBankingConstants.CHAR_Y));
    	
    	Password newSignOnPwd = loginAltFlowVO.getChangePwdVO().getSignOnNewPwd();
    	Password retypedSignOnPwd = loginAltFlowVO.getChangePwdVO().getSignOnDupNewPwd();    	
    	
    	if (signOnBeingChanged && !newSignOnPwd.toString().trim().equals(retypedSignOnPwd.toString().trim())) 
    	{

                throw new BusinessException(
                ebContext,
                EBIncidenceCodes.NEW_AND_RETYPE_SIGNON_PWDS_DONOT_MATCH,"error","changePwdVO.signOnNewPwd",
                EBankingErrorCodes.CRP_ERR_SIGN_ON_PASSWORD_DO_NOT_MATCH,null);
    	}
    	
    	if(ebContext.getFromContextData("IS_REST_REQUEST") !=null && ebContext.getFromContextData("IS_REST_REQUEST").toString().equals(EBankingConstants.YES) ){
    		if(FEBATypesUtility.isNotNullOrBlank(ebContext.getRequestId())){
    			LoginAltFlowNavigationUtility.getInstance().vaidateLGINSessionEntry(
	    				ebContext, status);
	    		EBHostInvoker.processRequest(objContext,EBRequestConstants.OR_INSERT_USER_DETAILS_REQUEST,loginAltFlowVO);
    		}
    	}else{
			LoginAltFlowNavigationUtility.getInstance().vaidateLGINSessionEntry(
				ebContext, status);
			EBHostInvoker.processRequest(objContext,EBRequestConstants.OR_INSERT_USER_DETAILS_REQUEST,loginAltFlowVO);
    	}

	}

	@Override
	protected void processLocalData(FEBATransactionContext arg0,
			IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		// Do nothing because of X and Y.
	}
}
