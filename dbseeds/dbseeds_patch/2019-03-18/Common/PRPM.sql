Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','OCR_VERIFICATION_FAIL_ALLOWED_COUNTER','3','Number of times OCR being allowed to Fail before marking it success for offline validation','N','setup',
sysdate,'setup',sysdate);

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','OCR_VERIFICATION_FAIL_STATUS_PERIOD','1','Application Status form expiry period','N','setup',
sysdate,'setup',sysdate);