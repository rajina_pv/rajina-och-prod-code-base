-- Added entry for re upload KTP
SET DEFINE OFF;
Insert into VMNP ("db_ts","bank_id","del_flg","mnu_id","mnu_prf_cd","parent_mnu_id","mnu_id_type","mnu_id_order","is_part_of_menu_tree","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','N','CREUPD','RUSER','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP ("db_ts","bank_id","del_flg","mnu_id","mnu_prf_cd","parent_mnu_id","mnu_id_type","mnu_id_order","is_part_of_menu_tree","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','N','CREUPD','DEF_RET','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);

-- Added entry for fetch KTP information
SET DEFINE OFF;
Insert into VMNP ("db_ts","bank_id","del_flg","mnu_id","mnu_prf_cd","parent_mnu_id","mnu_id_type","mnu_id_order","is_part_of_menu_tree","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','N','COKTPF','RRMGR','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP ("db_ts","bank_id","del_flg","mnu_id","mnu_prf_cd","parent_mnu_id","mnu_id_type","mnu_id_order","is_part_of_menu_tree","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','N','COKTPF','VTUSR','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);

-- Added entry for verify Payroll for second apply
SET DEFINE OFF;
Insert into VMNP ("db_ts","bank_id","del_flg","mnu_id","mnu_prf_cd","parent_mnu_id","mnu_id_type","mnu_id_order","is_part_of_menu_tree","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','N','CVPAYL','RUSER','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP ("db_ts","bank_id","del_flg","mnu_id","mnu_prf_cd","parent_mnu_id","mnu_id_type","mnu_id_order","is_part_of_menu_tree","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','N','CVPAYL','DEF_RET','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);

