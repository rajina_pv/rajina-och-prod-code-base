-- Added entry for re upload KTP
SET DEFINE OFF;
Insert into UMMT ("db_ts","bank_id","uri","method","default_menu_id","input_field_name","fieldvalue_menumapping","del_flg","r_cre_id","r_cre_time","r_mod_id","r_mod_time") values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}/reuploadktp','PUT','CREUPD',null,null,'N','SETUP',sysdate,'SETUP',sysdate);

-- Added entry for fetch KTP information
SET DEFINE OFF;
Insert into UMMT ("db_ts","bank_id","uri","method","default_menu_id","input_field_name","fieldvalue_menumapping","del_flg","r_cre_id","r_cre_time","r_mod_id","r_mod_time") values (1,'01','/v1/banks/{bankId}/custom/applications/{appId}/fetchKTP','GET','COKTPF',null,null,'N','SETUP',sysdate,'SETUP',sysdate);

-- Added entry for verify Payroll for second apply
Insert into UMMT ("db_ts","bank_id","uri","method","default_menu_id","input_field_name","fieldvalue_menumapping","del_flg","r_cre_id","r_cre_time","r_mod_id","r_mod_time") values (1,'01','/v1/banks/{bankid}/users/{userid}/custom/verifyPayroll','GET','CVPAYL',null,null,'N','SETUP',sysdate,'SETUP',sysdate);

