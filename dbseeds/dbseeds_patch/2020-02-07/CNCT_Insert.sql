Insert into  CNCT ("db_ts","bank_id","app_notif_status","app_id","num_days_notif_type","message_header","short_message","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','KTP_SAVED','PINANG','D1|P','Hi $$CUSTOMER_NAME$$, yuk lanjutkan pengajuan Pinangmu!','Nasabah Yth,
Tinggal sedikit lagi  untuk mendapatkan fasilitas Pinang. Ayo segera lanjutkan pengajuan pinjamanmu sebelum $$EXPDATE$$.','N','BPD',sysdate,'BPD',sysdate);

Insert into  CNCT ("db_ts","bank_id","app_notif_status","app_id","num_days_notif_type","message_header","short_message","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PER_SAVED','PINANG','D1|P,D3|S|E','Hi $$CUSTOMER_NAME$$, yuk lanjutkan pengajuan Pinangmu!','Nasabah Yth,
Tinggal sedikit lagi  untuk mendapatkan fasilitas Pinang. Ayo segera lanjutkan pengajuan pinjamanmu sebelum $$EXPDATE$$.','N','BPD',sysdate,'BPD',sysdate);

Insert into  CNCT ("db_ts","bank_id","app_notif_status","app_id","num_days_notif_type","message_header","short_message","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PAY_SAVED','PINANG','D1|P,D3|S','Hi $$CUSTOMER_NAME$$, yuk lanjutkan pengajuan Pinangmu!','Nasabah Yth,
Tinggal sedikit lagi  untuk mendapatkan fasilitas Pinang. Ayo segera lanjutkan pengajuan pinjamanmu sebelum $$EXPDATE$$.','N','BPD',sysdate,'BPD',sysdate);

Insert into  CNCT ("db_ts","bank_id","app_notif_status","app_id","num_days_notif_type","message_header","short_message","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','CON_SAVED','PINANG','D1|P,D3|S','Hi $$CUSTOMER_NAME$$, yuk lanjutkan pengajuan Pinangmu!','Nasabah Yth,
Tinggal sedikit lagi  untuk mendapatkan fasilitas Pinang. Ayo segera lanjutkan pengajuan pinjamanmu sebelum $$EXPDATE$$.','N','BPD',sysdate,'BPD',sysdate);

Insert into  CNCT ("db_ts","bank_id","app_notif_status","app_id","num_days_notif_type","message_header","short_message","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','CR_SCORE_APR','PINANG','D1|P,D7|P,D14|S,D20|S|E','Hi $$CUSTOMER_NAME$$, Pinjamanmu hampir cair nih!','Nasabah Yth,
Selamat! Pinjaman telah disetujui!. Segera lanjutkan proses pencairan untuk mendapatkan pinjamanmu. Penawaran ini berlaku sampai $$EXPDATE$$','N','BPD',sysdate,'BPD',sysdate);
