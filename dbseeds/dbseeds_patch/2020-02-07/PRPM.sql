-- Added for PINANG notifications - To be configured for production
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','PINANG_FIREBASE_PUSH_NOTIF_ENDPOINT_URL','/fcm/v1/projects/pinang-bri-dev/messages:send','The firebase end point URL for Pinang notifications batch setup','N','setup',
sysdate,'setup',sysdate);

-- Added for AGRO PINANG SMS end point URL
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','PINANG_AGRO_SMS_ENDPOINT_URL','/agro/sendmessagepinang','The pinang agro sms end point URL.','N','setup',
sysdate,'setup',sysdate);

-- AGRO email server user name
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','PINANG_EMAIL_SERVER_USERNAME','pinang','The Agro Email server user name','N','setup',
sysdate,'setup',sysdate);

---- AGRO email server password
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','PINANG_EMAIL_SERVER_PASSWORD','P1n4ng','The Agro Email server password.','N','setup',
sysdate,'setup',sysdate);

-- AGRO sms server user name
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','PINANG_SMS_SERVER_USERNAME','pinang','The Agro SMS server user name','N','setup',
sysdate,'setup',sysdate);

---- AGRO sms server password
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','PINANG_SMS_SERVER_PASSWORD','P1n4ng','The Agro SMS server password.','N','setup',
sysdate,'setup',sysdate);

---- AGRO Email Server End point URL
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','PINANG_AGRO_EMAIL_ENDPOINT_URL','/api/agro/sendemail','The Agro Email server End point URL.','N','setup',
sysdate,'setup',sysdate);


