Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") 
values (1,'01','CSHD','PNANG','001','Pinjaman Pinang','N','SETUP',sysdate,'SETUP',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") 
values (1,'01','CSHD','INFIN','001','Pinang Infinite','N','SETUP',sysdate,'SETUP',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") 
values (1,'01','CSHD','PYLTR','001','Pinang Paylater','N','SETUP',sysdate,'SETUP',sysdate);


-- For PINANG Pay Later SPH preview document changes 
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','DBTL','DTB6','001','97|TABLE|NULL|DHD4|LOAN_SANCTION_AMOUNT;LOAN_SCHEDULE_START_DATE;LOAN_SCHEDULE_END_DATE;MONTHLY_DUE_DATE;LOAN_INTEREST_RATE;LOAN_EMI_AMOUNT;LOAN_EMI_PENALTY;AUTO_INSTALLMENT_PAYMENT;LOAN_TENURE;PROCESSING_FEE','N','SETUP',to_timestamp('0001-01-01 00:00:00.0','null'),'SETUP',to_timestamp('0001-01-01 00:00:00.0','null'));

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','DBTL','DTX14','001','82|TEXT|3.4|DHD3|STATIC','N','setup',to_timestamp('2020-12-28 12:47:24.0','null'),'setup',to_timestamp('2020-12-28 12:47:24.0','null'));

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','DBTL','DTX15','001','83|TEXT|3.4|DHD3|STATIC','N','setup',to_timestamp('2020-12-28 12:47:24.0','null'),'setup',to_timestamp('2020-12-28 12:47:24.0','null'));

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','DBTL','DT36','001','217|TEXT|NULL|DHD11|STATIC','N','setup',to_timestamp('0001-01-01 00:00:00.0','null'),'setup',to_timestamp('0001-01-01 00:00:00.0','null'));
