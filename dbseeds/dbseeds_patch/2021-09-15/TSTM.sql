-- For Biaya layanan and Angsuran Pokok changes in SPH preview
Insert into TSTM ("db_ts","bank_id","corp_id","user_type","text_type","text_name","standard_text","del_flg","r_cre_id","r_cre_time","r_mod_id","r_mod_time") values (1,'01','01','4','TXM','DTB6','Jumlah Pinjaman (IDR) : @#@#@LOAN_SANCTION_AMOUNT@#@#@ | Tenor Pinjaman : @#@#@LOAN_TENURE@#@#@ Bulan | Jatuh Tempo Angsuran : Tanggal @#@#@MONTHLY_DUE_DATE@#@#@ Setiap Bulan | Biaya layanan : Rp @#@#@PROCESSING_FEE@#@#@ | Angsuran Pokok : @#@#@LOAN_SANCTION_AMOUNT@#@#@ | Penalti Keterlambatan : 0.50% per hari dari Bunga Harian | Fasilitas Otomatis Pembayaran Angsuran : Tagihan | (* automasi system) :','N','01.SUPADMIN',sysdate,'01.SUPADMIN',sysdate);

-- Added entry for PINANG Pay later
Insert into TSTM ("db_ts","bank_id","corp_id","user_type","text_type","text_name","standard_text","del_flg","r_cre_id","r_cre_time","r_mod_id","r_mod_time") values (1,'01','01','4','TXM','DTX14','Fasilitas PINANG tersebut akan dibayarkan kembali oleh DEBITUR kepada BANK dengan cara melakukan pembayaran dalam tenggang waktu 1 bulan terhitung sejak tanggal disetujuinya fasilitas PINANG dan ditandatanganinya SPH ini dan akan dilakukan dengan cara pendebetan secara otomatis oleh Bank dari rekening Debitur yang telah didaftarkan dan dikuasakan oleh Debitur kepada Bank','N','01.SUPADMIN',sysdate,'01.SUPADMIN',sysdate);

-- Added entry for PINANG Pay later
Insert into TSTM ("db_ts","bank_id","corp_id","user_type","text_type","text_name","standard_text","del_flg","r_cre_id","r_cre_time","r_mod_id","r_mod_time") values (1,'01','01','4','TXM','DTX15','Pendebetan otomatis oleh Bank sebagaimana dimaksud pada butir 2.2 akan dilakukan oleh sistem pada tanggal jatuh tempo pinjaman debitur dengan jumlah pendebetan sebesar tagihan kewajiban pada saat jatuh tempo','N','01.SUPADMIN',sysdate,'01.SUPADMIN',sysdate);

-- Added entry for PINANG Pay later
Insert into TSTM ("db_ts","bank_id","corp_id","user_type","text_type","text_name","standard_text","del_flg","r_cre_id","r_cre_time","r_mod_id","r_mod_time") values (1,'01','01','4','TXM','DT36','Debitur wajib untuk membayar kewajibannya atas Fasilitas PINANG yang diterima berupa angsuran pokok +biaya layanan paling lambat sebelum tanggal jatuh tempo sebagaimana diuraikan pada butir 4.1 diatas','N','01.SUPADMIN',sysdate,'01.SUPADMIN',sysdate);


-- Added for SPH doc Number 4 TNC
update TSTM set standard_text ='Fasilitas PINANG tersebut akan dibayarkan kembali oleh DEBITUR kepada BANK dengan cara melakukan angsuran secara bulanan dalam tenggang waktu bulan terhitung sejak tanggal disetujuinya fasilitas PINANG dan ditandatanganinya SPH ini dan akan dilakukan dengan cara pendebetan secara otomatis oleh Bank dari rekening Debitur yang telah didaftarkan dan dikuasakan oleh Debitur kepada Bank'
where user_type = '4' and text_type = 'TXM' and text_name = 'DTX7';


 